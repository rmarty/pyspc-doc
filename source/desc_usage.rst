
.. _descusage:

Utilisation des outils pySPC
----------------------------

Utilitaires et Scripts
++++++++++++++++++++++

.. index:: Utilitaire
.. index:: Script
.. index:: Binaire

Le paquet *pySPC* comporte des utilitaires dédiés aux données, modèles et outils utilisés au sein du réseau **Vigicrues**. Les fichiers *.py*, situés dans *pySPC\\bin\\*, sont des programmes où le(s) paramètre(s) est/sont à renseigner en tant qu'argument(s). Si l'installation est réalisée correctement, alors ces "binaires" peuvent être exécutées depuis n'importe quel répertoire.

.. note:: Le terme *utilitaire* est équivalent au terme *binaire* dans cette documentation.

Afin de réaliser des traitements *en masse*, le paquet *pySPC* comprend des scripts. Ceux-ci, placés dans *pySPC\\script\\*, ont le caractère *\_* comme préfixe. Le(s) paramètre(s) y est/sont écrit(s) 'en dur'. Certains d'entre eux utilisent des programmes de ce module *pySPC*. Dans ce cas, ceux-ci sont mentionnés dans les pages documentaires des scripts.

.. seealso:: Voir la section relative aux :ref:`varenv`.

.. warning:: Les scripts dédiés au projet Prévision 2015 du Schapi sont désormais dans `pyOTAMIN <https://pyotamin.readthedocs.io/fr/latest>`_.

Interface graphique et ligne de commande
++++++++++++++++++++++++++++++++++++++++

.. index:: Interface graphique
.. index:: Ligne de commande


Le module *pySPC* est fourni avec une interface graphique permettant de lancer les utilitaires de façon didactique. Ceux-ci peuvent aussi être lancés en mode *ligne de commande*. C'est d'ailleurs le moyen le plus simple de les utiliser. L'interface graphique permet de construire la ligne de commande à exécuter à partir de l'utilitaire et des options choisis par l'utilisateur. 

.. seealso:: Voir les sections relatives à :ref:`usage` et à :ref:`gui`.

