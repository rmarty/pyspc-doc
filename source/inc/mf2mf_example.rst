
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Re-écriture du fichier de données Météo-France ETPMON.data de type 'MF_Data' situé dans le répertoire data/data/mf vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf -O data/_bin/mf2mf/out -d ETPMON.data -t MF_Data


:blue:`Re-écriture du fichier de données Météo-France RR6.data de type 'MF_Data' situé dans le répertoire data/data/mf vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf -O data/_bin/mf2mf/out -d RR6.data -t MF_Data


:blue:`Re-écriture du fichier de données Météo-France RR1.data de type 'MF_Data' situé dans le répertoire data/data/mf vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf -O data/_bin/mf2mf/out -d RR1.data -t MF_Data


:blue:`Re-écriture du fichier de données Météo-France RR.data de type 'MF_Data' situé dans le répertoire data/data/mf vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf -O data/_bin/mf2mf/out -d RR.data -t MF_Data


:blue:`Re-écriture du fichier de données Météo-France T.data de type 'MF_Data' situé dans le répertoire data/data/mf vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf -O data/_bin/mf2mf/out -d T.data -t MF_Data


:blue:`Re-écriture du fichier de données Météo-France TNTXM.data de type 'MF_Data' situé dans le répertoire data/data/mf vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf -O data/_bin/mf2mf/out -d TNTXM.data -t MF_Data


:blue:`Re-écriture du fichier OpenAPI de Météo-France 43091005_H_202410170000_202410171200.csv de type 'MF_OpenAPI' situé dans le répertoire data/data/mf/open_api vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf/open_api -O data/_bin/mf2mf/out -d 43091005_H_202410170000_202410171200.csv -t MF_OpenAPI -v


:blue:`Re-écriture du fichier OpenAPI de Météo-France 43091005_MN_202410170500_202410170700.csv de type 'MF_OpenAPI' situé dans le répertoire data/data/mf/open_api vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf/open_api -O data/_bin/mf2mf/out -d 43091005_MN_202410170500_202410170700.csv -t MF_OpenAPI -v


:blue:`Re-écriture du fichier OpenAPI de Météo-France 43091005_Q_202410150000_202410190000.csv de type 'MF_OpenAPI' situé dans le répertoire data/data/mf/open_api vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf/open_api -O data/_bin/mf2mf/out -d 43091005_Q_202410150000_202410190000.csv -t MF_OpenAPI -v


:blue:`Re-écriture du fichier OpenData de Météo-France H_43_2010-2019.csv.gz de type 'MF_OpenData' situé dans le répertoire data/data/mf/open_data vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf/open_data -O data/_bin/mf2mf/out -d H_43_2010-2019.csv.gz -t MF_OpenData -v


:blue:`Re-écriture du fichier OpenData de Météo-France MN_43_previous-2020-2022.csv.gz de type 'MF_OpenData' situé dans le répertoire data/data/mf/open_data vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf/open_data -O data/_bin/mf2mf/out -d MN_43_previous-2020-2022.csv.gz -t MF_OpenData -v


:blue:`Re-écriture du fichier OpenData de Météo-France Q_07_latest-2023-2024_RR-T-Vent.csv.gz de type 'MF_OpenData' situé dans le répertoire data/data/mf/open_data vers le répertoire data/_bin/mf2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      mf2mf.py -I data/data/mf/open_data -O data/_bin/mf2mf/out -d Q_07_latest-2023-2024_RR-T-Vent.csv.gz -t MF_OpenData -v

