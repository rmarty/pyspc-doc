
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Conversion du fichier d'export Hydro2 crucal1.txt situé dans le répertoire data/metadata/hydro2 vers le fichier data/_bin/hydroStat/out/test_crucal.csv`

.. container:: cmdimg

   .. container:: cmdline

      hydroStat.py -d crucal1.txt -I data/metadata/hydro2 -c data/_bin/hydroStat/out/test_crucal.csv


:blue:`Conversion du fichier d'export Hydro2 debcla1.txt situé dans le répertoire data/metadata/hydro2 vers le fichier data/_bin/hydroStat/out/test_debcla.csv`

.. container:: cmdimg

   .. container:: cmdline

      hydroStat.py -d debcla1.txt -I data/metadata/hydro2 -c data/_bin/hydroStat/out/test_debcla.csv


:blue:`Conversion du fichier d'export Hydro2 synth1.txt situé dans le répertoire data/metadata/hydro2 vers le fichier data/_bin/hydroStat/out/test_synth.csv`

.. container:: cmdimg

   .. container:: cmdline

      hydroStat.py -d synth1.txt -I data/metadata/hydro2 -c data/_bin/hydroStat/out/test_synth.csv

