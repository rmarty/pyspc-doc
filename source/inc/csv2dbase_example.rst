
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Intégration des prévisions de débit horaire (['QH']) dont les identifiants sont listés dans data/_bin/csv2dbase/in/list_previ19.txt. Les séries sont lues depuis le répertoire data/core/csv et sont ajoutées à la base de type 'previ19' nommée prevision_2019.mdb dans le répertoire data.`

.. container:: cmdimg

   .. container:: cmdline

      csv2dbase.py -I data/core/csv -d prevision_2019.mdb -O data -n QH -l data/_bin/csv2dbase/in/list_previ19.txt -t previ19


Fichier de stations : data/_bin/csv2dbase/in/list_previ19.txt

.. code-block:: text

   K1251810_2018010412_2001_brut_10
   K1251810_2018010412_2001_brut_50
   K1251810_2018010412_2001_brut_90
   K1251810_2018010412_2001_brut


