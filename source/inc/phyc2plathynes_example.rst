
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les données nécessaires à PLATHYNES depuis la PHyC (cv) et enregistrer les fichiers xml dans le répertoire data/_bin/phyc2plathynes/ctl/xml. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/phyc2plathynes/plathynes: la station K0030020 est une station d'injection. Tous les événements sont considérés.`

.. container:: cmdimg

   .. container:: cmdline

      phyc2plathynes.py -I data/_bin/phyc2plathynes/ctl/xml -O data/_bin/phyc2plathynes/plathynes -c plathynes_project.prj -s K0030020 -M cv


:blue:`Extraire les données nécessaires à PLATHYNES depuis la PHyC (cv) et enregistrer les fichiers xml dans le répertoire data/_bin/phyc2plathynes/ctl/xml. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/phyc2plathynes/plathynes: la station K0030020 est une station d'injection. Seul l'événement 201911 est considéré.`

.. container:: cmdimg

   .. container:: cmdline

      phyc2plathynes.py -I data/_bin/phyc2plathynes/ctl/xml -O data/_bin/phyc2plathynes/plathynes -c plathynes_project.prj -s K0030020 -S 201911 -M cv


:blue:`Extraire les données nécessaires à PLATHYNES depuis la PHyC (cv) et enregistrer les fichiers xml dans le répertoire data/_bin/phyc2plathynes/ctl/xml. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/phyc2plathynes/plathynes: la station K0030020 est une station d'injection. Tous les événements sont considérés. Seules les données 'P' sont exportées.`

.. container:: cmdimg

   .. container:: cmdline

      phyc2plathynes.py -I data/_bin/phyc2plathynes/ctl/xml -O data/_bin/phyc2plathynes/plathynes -c plathynes_project.prj -s K0030020 -M cv -n P


:blue:`Extraire les données nécessaires à PLATHYNES depuis la PHyC (dl) et enregistrer les fichiers xmls dans le répertoire data/_bin/phyc2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/phyc2plathynes/plathynes. Tous les événements sont considérés. La configuration de la connexion à la PHyC est spécifiée dans le fichier ../bin/phyc2xml_RM.txt.`

.. container:: cmdimg

   .. container:: cmdline

      phyc2plathynes.py -I data/_bin/phyc2plathynes/in -D ../bin/phyc2xml_RM.txt -O data/_bin/phyc2plathynes/plathynes -c plathynes_project.prj -M dl


:blue:`Extraire les données nécessaires à PLATHYNES depuis la PHyC (dl) et enregistrer les fichiers xml dans le répertoire data/_bin/phyc2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/phyc2plathynes/plathynes. Seul l'événement 201911 est considéré. La configuration de la connexion à la PHyC est spécifiée dans le fichier ../bin/phyc2xml_RM.txt.`

.. container:: cmdimg

   .. container:: cmdline

      phyc2plathynes.py -I data/_bin/phyc2plathynes/in -D ../bin/phyc2xml_RM.txt -O data/_bin/phyc2plathynes/plathynes -c plathynes_project.prj -S 201911 -M dl

