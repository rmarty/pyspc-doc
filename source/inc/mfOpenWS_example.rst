
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Récupérer depuis l'API des données météorologiques (MF_OpenAPI) pour la grandeur PJ pour les lieux listés dans data/_bin/mfOpenWS/cfg/sitesmeteo_openapi.txt entre les dates 2016-05-29 00:00:00 et 2016-06-03 00:00:00 et enregistrer les fichiers csv dans le répertoire data/_bin/mfOpenWS/out. Les éléments de connexion à l'API sont renseignés dans ../bin/mfOpenWS_RM.txt.`

.. container:: cmdimg

   .. container:: cmdline

      mfOpenWS.py -l data/_bin/mfOpenWS/cfg/sitesmeteo_openapi.txt -c ../bin/mfOpenWS_RM.txt -t MF_OpenAPI -n PJ -F 20160529 -L 20160603 -O data/_bin/mfOpenWS/out -v


.. code-block:: cfg

   [data_obs_meteo]
   appid = CHAINE DE CARACTERES
   apikey = CHAINE DE CARACTERES


Fichier de stations : data/_bin/mfOpenWS/cfg/sitesmeteo_openapi.txt

.. code-block:: text

   45004001
   45069001
   45187001
   45340002



:blue:`Récupérer des données météorologiques (MF_OpenData) pour la grandeur PJ pour les lieux listés dans data/_bin/mfOpenWS/cfg/sitesmeteo.txt entre les dates 2019-09-01 00:00:00 et 2025-02-11 00:00:00 et enregistrer les fichiers csv.gz dans le répertoire data/_bin/mfOpenWS/out.`

.. container:: cmdimg

   .. container:: cmdline

      mfOpenWS.py -l data/_bin/mfOpenWS/cfg/sitesmeteo.txt -t MF_OpenData -n PJ -F 20190901 -L 20250211 -O data/_bin/mfOpenWS/out -v


Fichier de stations : data/_bin/mfOpenWS/cfg/sitesmeteo.txt

.. code-block:: text

   07075001
   07105001
   07154005
   43111002


