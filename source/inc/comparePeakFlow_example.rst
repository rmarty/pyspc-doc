
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Comparaison des pointes de crues, de la grandeur QH, amont lues dans le fichier K1524010.csv et aval dans le fichier K1533010.csv, tous deux placés dans le répertoire data/_bin/comparePeakFlow/in. Le fichier csv associant les pointes de crues et le fichier png de l'ajustement sont écrits dans data/_bin/comparePeakFlow/out`

.. container:: cmdimg

   .. container:: cmdline

      comparePeakFlow.py -I data/_bin/comparePeakFlow/in -n QH -c K1524010.csv -d K1533010.csv -O data/_bin/comparePeakFlow/out

