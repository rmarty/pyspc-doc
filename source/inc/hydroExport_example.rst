
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Création du fichier d'export Hydro2 correspondant à la procédure CRUCAL pour les stations listées dans le fichier data/_bin/hydroExport/cfg/Arroux.txt sur la période 194809010000 - 202008310000 dans le répertoire data/_bin/hydroExport/out. La procédure est appliquée une seule fois grâce à l'usage de ['onefile', 'crucal1.txt'].`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -l data/_bin/hydroExport/cfg/Arroux.txt -O data/_bin/hydroExport/out -t CRUCAL -F 19480901 -L 20200831 -U onefile crucal1.txt


Fichier de stations : data/_bin/hydroExport/cfg/Arroux.txt

.. code-block:: text

   K1251810
   K1321810
   K1341810



:blue:`Création du fichier d'export Hydro2 correspondant à la procédure DEBCLA pour les stations listées dans le fichier data/_bin/hydroExport/cfg/Arroux.txt sur la période 194809010000 - 202008310000 dans le répertoire data/_bin/hydroExport/out. La procédure est appliquée une seule fois grâce à l'usage de ['onefile', 'debcla1.txt'].`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -l data/_bin/hydroExport/cfg/Arroux.txt -O data/_bin/hydroExport/out -t DEBCLA -F 19480901 -L 20200831 -U onefile debcla1.txt


Fichier de stations : data/_bin/hydroExport/cfg/Arroux.txt

.. code-block:: text

   K1251810
   K1321810
   K1341810



:blue:`Création du fichier d'export Hydro2 correspondant à la procédure H-TEMPS pour la station K1321810 sur la période 202003050600 - 202003070600 dans le répertoire data/_bin/hydroExport/out`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -s K1321810 -O data/_bin/hydroExport/out -t H-TEMPS -F 2020030506 -L 2020030706


:blue:`Création du fichier d'export Hydro2 correspondant à la procédure QJM pour la station K1321810 sur la période 202003050600 - 202003070600 dans le répertoire data/_bin/hydroExport/out`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -s K1321810 -O data/_bin/hydroExport/out -t QJM -F 2020030506 -L 2020030706


:blue:`Création du fichier d'export Hydro2 correspondant à la procédure QTFIX pour la station K1321810 sur la période 202003050600 - 202003070600 dans le répertoire data/_bin/hydroExport/out`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -s K1321810 -O data/_bin/hydroExport/out -t QTFIX -F 2020030506 -L 2020030706


:blue:`Création du fichier d'export Hydro2 correspondant à la procédure QTVAR pour la station K1321810 sur la période 202003050600 - 202003070600 dans le répertoire data/_bin/hydroExport/out`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -s K1321810 -O data/_bin/hydroExport/out -t QTVAR -F 2020030506 -L 2020030706


:blue:`Création du fichier d'export Hydro2 correspondant à la procédure SYNTHESE pour les stations listées dans le fichier data/_bin/hydroExport/cfg/Arroux.txt sur la période 194809010000 - 202008310000 dans le répertoire data/_bin/hydroExport/out. La procédure est appliquée une seule fois grâce à l'usage de ['onefile', 'synth1.txt'].`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -l data/_bin/hydroExport/cfg/Arroux.txt -O data/_bin/hydroExport/out -t SYNTHESE -F 19480901 -L 20200831 -U onefile synth1.txt


Fichier de stations : data/_bin/hydroExport/cfg/Arroux.txt

.. code-block:: text

   K1251810
   K1321810
   K1341810



:blue:`Création du fichier d'export Hydro2 correspondant à la procédure TOUSMOIS pour les stations listées dans le fichier data/_bin/hydroExport/cfg/Arroux.txt sur la période 201801010000 - 202012310000 dans le répertoire data/_bin/hydroExport/out. La procédure est appliquée une seule fois grâce à l'usage de ['onefile', 'tm1prec1.txt']. La précision de l'export est définie par ['precision', '1'].`

.. container:: cmdimg

   .. container:: cmdline

      hydroExport.py -l data/_bin/hydroExport/cfg/Arroux.txt -O data/_bin/hydroExport/out -t TOUSMOIS -F 20180101 -L 20201231 -U onefile tm1prec1.txt -U precision 1


Fichier de stations : data/_bin/hydroExport/cfg/Arroux.txt

.. code-block:: text

   K1251810
   K1321810
   K1341810


