
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Dupliquer l'événement TEST du projet PLATHYNES ARNON_MAREUIL.prj situé dans le répertoire data/_bin/duplicatePlathynesEvent/plathynes pour créer un autre projet PLATHYNES dans le répertoire data/_bin/duplicatePlathynesEvent/out en ignorant les événements existant (option -o: True). Les événements à créer sont listés dans le fichier data/_bin/duplicatePlathynesEvent/in/events.csv.`

.. container:: cmdimg

   .. container:: cmdline

      duplicatePlathynesEvent.py -I data/_bin/duplicatePlathynesEvent/plathynes -d ARNON_MAREUIL.prj -O data/_bin/duplicatePlathynesEvent/out -c data/_bin/duplicatePlathynesEvent/in/events.csv -S TEST -o

