
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Construction d'une série de valeurs > 100 à la station K0000000. Application de la méthode ath avec les éléments utilisateurs ['100'] à partir de données au format 'pyspc' dans le répertoire data/io/pyspcfile de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/io/pyspcfile -O data/_bin/csv2csv/out -n QH -s K0000000 -M ath 100


:blue:`Construction d'une série de valeurs < 100 à la station K0000000. Application de la méthode bth avec les éléments utilisateurs ['100'] à partir de données au format 'pyspc' dans le répertoire data/io/pyspcfile de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/io/pyspcfile -O data/_bin/csv2csv/out -n QH -s K0000000 -M bth 100


:blue:`Construction d'une série entre 2 dates à la station K0000000. Application de la méthode bwd avec les éléments utilisateurs ['2014110406', '2014110410'] à partir de données au format 'pyspc' dans le répertoire data/io/pyspcfile de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/io/pyspcfile -O data/_bin/csv2csv/out -n QH -s K0000000 -M bwd 2014110406 2014110410


:blue:`Concaténer les séries listées dans data/_bin/csv2csv/in/cat/K000000_concat.txt en une seule. Application de la méthode cat avec les éléments utilisateurs [] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/cat de la grandeur ('QH') aux stations listées dans data/_bin/csv2csv/in/cat/K000000_concat.txt vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/cat -O data/_bin/csv2csv/out -n QH -l data/_bin/csv2csv/in/cat/K000000_concat.txt -M cat


Fichier de stations : data/_bin/csv2csv/in/cat/K000000_concat.txt

.. code-block:: text

   K000000a
   K000000b



:blue:`Changement de code d'une série de données. Application de la méthode ccd avec les éléments utilisateurs ['K1234567'] à partir de données au format 'pyspc' dans le répertoire data/io/pyspcfile de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/io/pyspcfile -O data/_bin/csv2csv/out -n QH -s K0000000 -M ccd K1234567


:blue:`Compléter des données par un bloc annuel d'une autre série. Application de la méthode cpy avec les éléments utilisateurs ['43130002', '2014', '20151103', '20151110'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/dsc de la grandeur ('PJ') à la station 43130002 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/dsc -O data/_bin/csv2csv/out -n PJ -s 43130002 -M cpy 43130002 2014 20151103 20151110


:blue:`Construction d'une série horaire à partir de données journalières à la station 43130002. Application de la méthode dsc avec les éléments utilisateurs ['PH', '66'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/dsc de la grandeur ('PJ') à la station 43130002 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/dsc -O data/_bin/csv2csv/out -n PJ -s 43130002 -M dsc PH 66


:blue:`Construction d'une série d'ETP horaire à la station 43130002. Application de la méthode etp avec les éléments utilisateurs ['hourly', '44.9'] à partir de données au format 'grp16' dans le répertoire data/_bin/csv2csv/in/usc de la grandeur ('TH') à la station 43130002 vers le format 'grp16' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/usc -O data/_bin/csv2csv/out -n TH -s 43130002 -M etp hourly 44.9 -C grp16 grp16


:blue:`Création de simulations à partir de prévisions listées dans data/_bin/csv2csv/in/f2s/grp.txt. Application de la méthode f2s avec les éléments utilisateurs ['6', '24', '48'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/f2s de la grandeur ('QH') des séries listées dans data/_bin/csv2csv/in/f2s/grp.txt vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/f2s -O data/_bin/csv2csv/out -n QH -l data/_bin/csv2csv/in/f2s/grp.txt -M f2s 6 24 48 -1


Fichier de stations : data/_bin/csv2csv/in/f2s/grp.txt

.. code-block:: text

   K1251810_2018010312_1007
   K1251810_2018010312_2007
   K1251810_2018010312_2011
   K1251810_2018010412_1007
   K1251810_2018010412_2007
   K1251810_2018010412_2011



:blue:`Remplacement des valeurs manquantes par une constante à la station K0000000. Application de la méthode fct avec les éléments utilisateurs ['0'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/ctl/ath de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/ctl/ath -O data/_bin/csv2csv/out -n QH -s K0000000 -M fct 0


:blue:`Remplacement des valeurs manquantes par interpolation à la station K0000000. Application de la méthode fli avec les éléments utilisateurs [] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/ctl/bth de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/ctl/bth -O data/_bin/csv2csv/out -n QH -s K0000000 -M fli


:blue:`Construction d'une série de valeurs > 100 à la station K0000000. Application de la méthode lrg avec les éléments utilisateurs ['2', '10', '6'] à partir de données au format 'pyspc' dans le répertoire data/io/pyspcfile de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/io/pyspcfile -O data/_bin/csv2csv/out -n QH -s K0000000 -M lrg 2 10 6


:blue:`Construction d'une série 6-min à partir de données 5-min à la station K0109910. Application de la méthode nsc avec les éléments utilisateurs ['P6m', 'o'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/nsc de la grandeur ('P5m') à la station K0109910 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/nsc -O data/_bin/csv2csv/out -n P5m -s K0109910 -M nsc P6m o


:blue:`Calcul des percentiles (fréquences: ['10-25-50-75-90']) à partir des séries listées dans data/_bin/csv2csv/in/pct/pearp.txt. Application de la méthode pct avec les éléments utilisateurs ['10-25-50-75-90'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/pct de la grandeur ('QH') aux stations listées dans data/_bin/csv2csv/in/pct/pearp.txt vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out. Un seul fichier est créé`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/pct -O data/_bin/csv2csv/out -n QH -l data/_bin/csv2csv/in/pct/pearp.txt -M pct 10-25-50-75-90 -1


Fichier de stations : data/_bin/csv2csv/in/pct/pearp.txt

.. code-block:: text

   K6373020_2016053006_2101
   K6373020_2016053006_2102
   K6373020_2016053006_2103
   K6373020_2016053006_2104
   K6373020_2016053006_2105
   K6373020_2016053006_2106
   K6373020_2016053006_2107
   K6373020_2016053006_2108
   K6373020_2016053006_2109
   K6373020_2016053006_2110
   K6373020_2016053006_2111
   K6373020_2016053006_2112
   K6373020_2016053006_2113
   K6373020_2016053006_2114
   K6373020_2016053006_2115
   K6373020_2016053006_2116
   K6373020_2016053006_2117
   K6373020_2016053006_2118
   K6373020_2016053006_2119
   K6373020_2016053006_2120
   K6373020_2016053006_2121
   K6373020_2016053006_2122
   K6373020_2016053006_2123
   K6373020_2016053006_2124
   K6373020_2016053006_2125
   K6373020_2016053006_2126
   K6373020_2016053006_2127
   K6373020_2016053006_2128
   K6373020_2016053006_2129
   K6373020_2016053006_2130
   K6373020_2016053006_2131
   K6373020_2016053006_2132
   K6373020_2016053006_2133
   K6373020_2016053006_2134
   K6373020_2016053006_2135



:blue:`Construction de séries à la station K001002010 à partir d'un barème de réservoir. Application de la méthode res avec les éléments utilisateurs ['data//core//reservoir//reservoir.txt', 'llp', 'V:V,Q:Qd'] à partir de données au format 'pyspc' dans le répertoire data/core/reservoir de la grandeur ('HH') à la station K001002010 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/core/reservoir -O data/_bin/csv2csv/out -n HH -s K001002010 -M res data/core/reservoir/reservoir.txt llp V:V,Q:Qd


:blue:`Construction d'une série horaire à partir de données instantanées à la station RH10585x. Application de la méthode rsc avec les éléments utilisateurs [] à partir de données au format 'grp18' dans le répertoire data/model/grp18/cal de la grandeur ('QI') à la station RH10585x vers le format 'grp16' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/model/grp18/cal -O data/_bin/csv2csv/out -n QI -s RH10585x -M rsc -C grp18 grp16


:blue:`Construction de séries à la station K0100020 à partir d'un barème d'une base Bareme. Application de la méthode rtc avec les éléments utilisateurs ['data//io//dbase//bareme.mdb'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/rtc de la grandeur ('QH') à la station K0100020 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/rtc -O data/_bin/csv2csv/out -n QH -s K0100020 -M rtc data/io/dbase/bareme.mdb


:blue:`Construction de séries à la station K0100020 à partir d'un bareme particulier d'une base Bareme. Application de la méthode rtc avec les éléments utilisateurs ['data//io//dbase//bareme.mdb', 'H200809'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/rtc de la grandeur ('QH') à la station K0100020 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/rtc -O data/_bin/csv2csv/out -n QH -s K0100020 -M rtc data/io/dbase/bareme.mdb H200809


:blue:`Construction de séries à la station K0550010 à partir d'un barème d'un fichier XML Sandre. Application de la méthode rtc avec les éléments utilisateurs ['data//_bin//csv2csv//in//rtc//RatingCurves.xml'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/rtc de la grandeur ('QH') à la station K0550010 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/rtc -O data/_bin/csv2csv/out -n QH -s K0550010 -M rtc data/_bin/csv2csv/in/rtc/RatingCurves.xml


:blue:`Construction de séries à la station K0550010 à partir d'un barème et d'une courbe de correction de fichiers. XML Sandre. Application de la méthode rtc avec les éléments utilisateurs ['data//_bin//csv2csv//in//rtc//RatingCurves.xml', 'data//_bin//csv2csv//in//rtc//levelcor.xml'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/rtc de la grandeur ('QH') à la station K0550010 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/rtc -O data/_bin/csv2csv/out -n QH -s K0550010 -M rtc data/_bin/csv2csv/in/rtc/RatingCurves.xml data/_bin/csv2csv/in/rtc/levelcor.xml


:blue:`Création de prévisions à partir de simulations et observations listées dans data/_bin/csv2csv/in/s2f/mohys.txt. Application de la méthode s2f avec les éléments utilisateurs ['36', '48', '2018010412', '2018010412'] à partir de données au format 'pyspc' dans le répertoire data/_bin/csv2csv/in/s2f de la grandeur ('QH') des séries listées dans data/_bin/csv2csv/in/s2f/mohys.txt vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/s2f -O data/_bin/csv2csv/out -n QH -l data/_bin/csv2csv/in/s2f/mohys.txt -M s2f 36 48 2018010412 2018010412


Fichier de stations : data/_bin/csv2csv/in/s2f/mohys.txt

.. code-block:: text

   K1321810
   K1321810_mohys



:blue:`Construction d'une série infra-horaire à partir de données horaires selon l'approche interpolate à la station K040301i. Application de la méthode shs avec les éléments utilisateurs ['900', 'interpolate'] à partir de données au format 'grp16' dans le répertoire data/_bin/csv2csv/in/shs de la grandeur ('QH') à la station K040301i vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/shs -O data/_bin/csv2csv/out -n QH -s K040301i -M shs 900 interpolate -C grp16 pyspc


:blue:`Décalage temporel (-2h) d'une série à la station K0000000. Application de la méthode tlg avec les éléments utilisateurs ['" -2"'] à partir de données au format 'pyspc' dans le répertoire data/io/pyspcfile de la grandeur ('QH') à la station K0000000 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/io/pyspcfile -O data/_bin/csv2csv/out -n QH -s K0000000 -M tlg " -2"


:blue:`Construction d'une série journalière à partir de données horaires à la station 43130002. Application de la méthode usc avec les éléments utilisateurs ['TJ', 'o', '00'] à partir de données au format 'grp16' dans le répertoire data/_bin/csv2csv/in/usc de la grandeur ('TH') à la station 43130002 vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/usc -O data/_bin/csv2csv/out -n TH -s 43130002 -M usc TJ o 00 -C grp16 pyspc


:blue:`Construction d'une série à partir de la somme pondérée de plusieurs séries listées dans data/_bin/csv2csv/in/wav/HteLoireN.txt. Application de la méthode wav avec les éléments utilisateurs ['data//_bin//csv2csv//in//wav//ponderation.txt'] à partir de données au format 'grp16' dans le répertoire data/_bin/csv2csv/in/wav de la grandeur ('PH') aux stations listées dans data/_bin/csv2csv/in/wav/HteLoireN.txt vers le format 'pyspc' dans le répertoire data/_bin/csv2csv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2csv.py -I data/_bin/csv2csv/in/wav -O data/_bin/csv2csv/out -n PH -l data/_bin/csv2csv/in/wav/HteLoireN.txt -M wav data/_bin/csv2csv/in/wav/ponderation.txt -C grp16 pyspc


Fichier de stations : data/_bin/csv2csv/in/wav/HteLoireN.txt

.. code-block:: text

   43091005N
   43130002N
   43051003N


