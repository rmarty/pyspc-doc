
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Conversion des données depuis le format csv de GRP v2016 (grp16) des séries listées dans data/_bin/csv2grpRT/in/grp16.txt, les grandeurs ['PH', 'QH'], dont les fichiers sont dans le répertoire data/model/grp16/cal vers les fichiers d'entrée pour GRP RT v2016 (grp16_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/model/grp16/cal -l data/_bin/csv2grpRT/in/grp16.txt -O data/_bin/csv2grpRT/out -n PH QH -t grp16_rt_data -C grp16 -w


Fichier de stations : data/_bin/csv2grpRT/in/grp16.txt

.. code-block:: text

   43091005
   K0114030



:blue:`Conversion des données depuis le format csv de GRP v2018 (grp18) des séries listées dans data/_bin/csv2grpRT/in/grp18.txt, les grandeurs ['PH', 'QI'], dont les fichiers sont dans le répertoire data/model/grp18/cal vers les fichiers d'entrée pour GRP RT v2018 (grp18_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/model/grp18/cal -l data/_bin/csv2grpRT/in/grp18.txt -O data/_bin/csv2grpRT/out -n PH QI -t grp18_rt_data -C grp18 -w


Fichier de stations : data/_bin/csv2grpRT/in/grp18.txt

.. code-block:: text

   90065003
   RH10585x



:blue:`Conversion des données depuis le format csv de GRP v2020 (grp20) des séries listées dans data/_bin/csv2grpRT/in/grp20.txt, les grandeurs ['PH', 'QI'], dont les fichiers sont dans le répertoire data/model/grp20/cal vers les fichiers d'entrée pour GRP RT v2020 (grp20_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/model/grp20/cal -l data/_bin/csv2grpRT/in/grp20.txt -O data/_bin/csv2grpRT/out -n PH QI -t grp20_rt_data -C grp20 -w


Fichier de stations : data/_bin/csv2grpRT/in/grp20.txt

.. code-block:: text

   90065003
   RH10585x



:blue:`Conversion des données depuis le format csv de GRP v2022 (grp22) des séries listées dans data/_bin/csv2grpRT/in/grp22.txt, les grandeurs ['PH', 'QI'], dont les fichiers sont dans le répertoire data/model/grp22/cal vers les fichiers d'entrée pour GRP RT v2022 (grp22_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/model/grp22/cal -l data/_bin/csv2grpRT/in/grp22.txt -O data/_bin/csv2grpRT/out -n PH QI -t grp22_rt_data -C grp22 -w


Fichier de stations : data/_bin/csv2grpRT/in/grp22.txt

.. code-block:: text

   90065003
   RH10585x



:blue:`Conversion des données depuis le format csv de type grp20 des séries liées à None, aux grandeurs ['PH', 'QI'], dont les fichiers sont dans le répertoire data/model/grp20/cal vers les fichiers d'entrée pour GRP RT v2020 (grp20_rt_archive) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/model/grp20/cal -l data/_bin/csv2grpRT/in/grp20.txt -O data/_bin/csv2grpRT/out -n PH QI -t grp20_rt_archive -C grp20 -w -v


Fichier de stations : data/_bin/csv2grpRT/in/grp20.txt

.. code-block:: text

   90065003
   RH10585x



:blue:`Conversion des données depuis le format csv de type grp22 des séries liées à None, aux grandeurs ['PH', 'QI'], dont les fichiers sont dans le répertoire data/model/grp22/cal vers les fichiers d'entrée pour GRP RT v2022 (grp22_rt_archive) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/model/grp22/cal -l data/_bin/csv2grpRT/in/grp22.txt -O data/_bin/csv2grpRT/out -n PH QI -t grp22_rt_archive -C grp22 -w -v


Fichier de stations : data/_bin/csv2grpRT/in/grp22.txt

.. code-block:: text

   90065003
   RH10585x



:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à LaLoireChadrac, aux grandeurs ['PH', 'QH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers d'entrée pour GRP RT v2016 (grp16_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s LaLoireChadrac -O data/_bin/csv2grpRT/out -n PH QH -t grp16_rt_data -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à LaLoireChadrac, aux grandeurs ['PH', 'QH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers d'entrée pour GRP RT v2018 (grp18_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s LaLoireChadrac -O data/_bin/csv2grpRT/out -n PH QH -t grp18_rt_data -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à LaLoireChadrac, aux grandeurs ['PH', 'QH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers d'entrée pour GRP RT v2020 (grp20_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s LaLoireChadrac -O data/_bin/csv2grpRT/out -n PH QH -t grp20_rt_data -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à LaLoireChadrac, aux grandeurs ['PH', 'QH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers d'entrée pour GRP RT v2022 (grp22_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s LaLoireChadrac -O data/_bin/csv2grpRT/out -n PH QH -t grp22_rt_data -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à Gazeille_2017061312, aux grandeurs ['PH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers de scénarios météo pour GRP RT v2016 (grp16_rt_data) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s Gazeille_2017061312 -O data/_bin/csv2grpRT/out -n PH -t grp16_rt_data -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à Gazeille_2017061312, aux grandeurs ['PH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers de scénarios météo pour GRP RT v2018 (grp18_rt_metscen) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s Gazeille_2017061312 -O data/_bin/csv2grpRT/out -n PH -t grp18_rt_metscen -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à Gazeille_2017061312, aux grandeurs ['PH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers de scénarios météo pour GRP RT v2020 (grp20_rt_metscen) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s Gazeille_2017061312 -O data/_bin/csv2grpRT/out -n PH -t grp20_rt_metscen -C pyspc -w


:blue:`Conversion des données depuis le format csv de type pyspc des séries liées à Gazeille_2017061312, aux grandeurs ['PH'], dont les fichiers sont dans le répertoire data/core/csv vers les fichiers de scénarios météo pour GRP RT v2022 (grp22_rt_metscen) dans le répertoire data/_bin/csv2grpRT/out.`

.. container:: cmdimg

   .. container:: cmdline

      csv2grpRT.py -I data/core/csv -s Gazeille_2017061312 -O data/_bin/csv2grpRT/out -n PH -t grp22_rt_metscen -C pyspc -w

