
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Mise à jour de la configuration Scores config_0.txt dans le répertoire data/_bin/duplicateScoresCfg/out à partir du fichier 'source' config_defaut.cfg situé dans le répertoire data/verification/scores. Puisque l'option U n'est pas défini, cela revient à copier le fichier source`

.. container:: cmdimg

   .. container:: cmdline

      duplicateScoresCfg.py -I data/verification/scores -D config_defaut.cfg -O data/_bin/duplicateScoresCfg/out -c config_0.txt


:blue:`Mise à jour de la configuration Scores config_1.txt dans le répertoire data/_bin/duplicateScoresCfg/out à partir du fichier 'source' config_0.txt situé dans le répertoire data/_bin/duplicateScoresCfg/ctl. Celle-ci concerne les sections/options suivantes : (GENERAL, ENTITE) -> K5383020, (GENERAL, GRANDEUR) -> Q, (OBSERVATION, DONNEES_OBSERVATION) -> "obs/fichier_1.csv, obs/fichier_2.csv", (PREVISION, DONNEES_PREVISION) -> "fcst/fichier_1.csv, fcst/fichier_2.csv", (PREVISION, LISTE_MODELE) -> 45gGRPd000, (PREVISION, LISTE_ECHEANCE) -> "0, 6, 12, 24, 48, 72", (EXPORT, DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE) -> output/fcst_ltime, (CALCUL, FICHIER_RESULTAT) -> output/results.xml, (CALCUL, LISTE_GROUPES_SCORES) -> "1, 3, 7"`

.. container:: cmdimg

   .. container:: cmdline

      duplicateScoresCfg.py -I data/_bin/duplicateScoresCfg/ctl -D config_0.txt -O data/_bin/duplicateScoresCfg/out -c config_1.txt -U GENERAL ENTITE K5383020 -U GENERAL GRANDEUR Q -U OBSERVATION DONNEES_OBSERVATION "obs/fichier_1.csv, obs/fichier_2.csv" -U PREVISION DONNEES_PREVISION "fcst/fichier_1.csv, fcst/fichier_2.csv" -U PREVISION LISTE_MODELE 45gGRPd000 -U PREVISION LISTE_ECHEANCE "0, 6, 12, 24, 48, 72" -U EXPORT DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE output/fcst_ltime -U CALCUL FICHIER_RESULTAT output/results.xml -U CALCUL LISTE_GROUPES_SCORES "1, 3, 7"

