
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir le fichier bp_ic_201605310503.xml d'archive BP ('bp') situé dans le répertoire data/data/mf au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la zone 71005 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d bp_ic_201605310503.xml -t bp -s 71005 -C pyspc -1


:blue:`Convertir le fichier bp_ly_200811010718.xml d'archive BP ('bp') situé dans le répertoire data/data/mf au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la zone 403 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d bp_ly_200811010718.xml -t bp -s 403 -C pyspc -1


:blue:`Convertir le fichier RR1.data de la Publithèque ('data') situé dans le répertoire data/data/mf au format csv de type 'grp16' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 42039003 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d RR1.data -t data -s 42039003 -C grp16


:blue:`Convertir le fichier T.data de la Publithèque ('data') situé dans le répertoire data/data/mf au format csv de type 'grp16' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 21567001 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d T.data -t data -s 21567001 -C grp16


:blue:`Convertir le fichier ETPMON.data de la Publithèque ('data') situé dans le répertoire data/data/mf au format csv de type 'grp18' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 63113001 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d ETPMON.data -t data -s 63113001 -C grp18


:blue:`Convertir le fichier RR6.data de la Publithèque ('data') situé dans le répertoire data/data/mf au format csv de type 'grp18' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 43111002 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d RR6.data -t data -s 43111002 -C grp18


:blue:`Convertir le fichier RR.data de la Publithèque ('data') situé dans le répertoire data/data/mf au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 07075001 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d RR.data -t data -s 07075001 -C pyspc


:blue:`Convertir le fichier TNTXM.data de la Publithèque ('data') situé dans le répertoire data/data/mf au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 03060001 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d TNTXM.data -t data -s 03060001 -C pyspc


:blue:`Convertir le fichier 43091005_H_202410170000_202410171200.csv de l'api de Météo-France ('MF_OpenAPI') situé dans le répertoire data/data/mf/open_api au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_api -O data/_bin/mf2csv/out -d 43091005_H_202410170000_202410171200.csv -t MF_OpenAPI -C pyspc -v


:blue:`Convertir le fichier 43091005_MN_202410170500_202410170700.csv de l'api de Météo-France ('MF_OpenAPI') situé dans le répertoire data/data/mf/open_api au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_api -O data/_bin/mf2csv/out -d 43091005_MN_202410170500_202410170700.csv -t MF_OpenAPI -C pyspc -v


:blue:`Convertir le fichier 43091005_Q_202410150000_202410190000.csv de l'api de Météo-France ('MF_OpenAPI') situé dans le répertoire data/data/mf/open_api au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_api -O data/_bin/mf2csv/out -d 43091005_Q_202410150000_202410190000.csv -t MF_OpenAPI -C pyspc -v


:blue:`Convertir le fichier H_43_2010-2019.csv.gz du site meteo.data.gouv.fr ('MF_OpenData') situé dans le répertoire data/data/mf/open_data au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_data -O data/_bin/mf2csv/out -d H_43_2010-2019.csv.gz -t MF_OpenData -C pyspc


:blue:`Convertir le fichier MN_43_previous-2020-2022.csv.gz du site meteo.data.gouv.fr ('MF_OpenData') situé dans le répertoire data/data/mf/open_data au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_data -O data/_bin/mf2csv/out -d MN_43_previous-2020-2022.csv.gz -t MF_OpenData -C pyspc


:blue:`Convertir le fichier Q_07_latest-2023-2024_RR-T-Vent.csv.gz du site meteo.data.gouv.fr ('MF_OpenData') situé dans le répertoire data/data/mf/open_data au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_data -O data/_bin/mf2csv/out -d Q_07_latest-2023-2024_RR-T-Vent.csv.gz -t MF_OpenData -C pyspc


:blue:`Convertir le fichier Q_07_latest-2023-2024_RR-T-Vent.csv.gz du site meteo.data.gouv.fr ('MF_OpenData') situé dans le répertoire data/data/mf/open_data au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Un seul fichier est créé et seule la station 07154005 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf/open_data -O data/_bin/mf2csv/out -d Q_07_latest-2023-2024_RR-T-Vent.csv.gz -s 07154005 -t MF_OpenData -C pyspc


:blue:`Convertir le fichier sympo_201611200658 d'archive SYMPO ('sympo') situé dans le répertoire data/data/mf au format csv de type 'pyspc' dans le répertoire data/_bin/mf2csv/out. Seule la zone 0708 est extraite.`

.. container:: cmdimg

   .. container:: cmdline

      mf2csv.py -I data/data/mf -O data/_bin/mf2csv/out -d sympo_201611200658 -t sympo -s 0708 -C pyspc

