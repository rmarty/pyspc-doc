
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Téléchargement du document de type 'inrae_explore2070' vers le répertoire data/_bin/onlineReport/out pour l'entité 652`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -s 652 -O data/_bin/onlineReport/out -t inrae_explore2070


:blue:`Téléchargement du document de type 'inrae_explore2070' vers le répertoire data/_bin/onlineReport/out pour les entités listé dans data/_bin/onlineReport/in/liste_explore2070.txt`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -l data/_bin/onlineReport/in/liste_explore2070.txt -O data/_bin/onlineReport/out -t inrae_explore2070 -v


Fichier de stations : data/_bin/onlineReport/in/liste_explore2070.txt

.. code-block:: text

   Carto2 - Explore 2070
   
   Explore ID	Lien Fiche	Nom commun	SourceGeo	Num fiche
   653	Lien	COUBON	Sim	653  (COUBON )
   654	Lien	BRIVES-CHARENSAC	Gr	654  (BRIVES-CHARENSAC )
   655	Lien	LE MONTEIL	Gr	655  (LE MONTEIL )



:blue:`Téléchargement du document de type 'inrae_hydroclim' vers le répertoire data/_bin/onlineReport/out pour l'entité K153301001`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -s K153301001 -O data/_bin/onlineReport/out -t inrae_hydroclim


:blue:`Téléchargement du document de type 'inrae_shyreg' vers le répertoire data/_bin/onlineReport/out pour l'entité LO2228`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t inrae_shyreg -s LO2228


:blue:`Téléchargement du document de type 'inrae_shyreg_bnbv' vers le répertoire data/_bin/onlineReport/out pour l'entité LO2228`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t inrae_shyreg_bnbv -s LO2228


:blue:`Téléchargement du document de type 'inrae_shyreg_hydro' vers le répertoire data/_bin/onlineReport/out pour l'entité K0403010`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t inrae_shyreg_hydro -s K0403010


:blue:`Téléchargement du document de type 'mf_clim' vers le répertoire data/_bin/onlineReport/out pour l'entité 03155003`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t mf_clim -s 03155003


:blue:`Téléchargement du document de type 'mf_climdata' vers le répertoire data/_bin/onlineReport/out pour l'entité 03155003`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t mf_climdata -s 03155003


:blue:`Téléchargement du document de type 'mf_dailyreport' vers le répertoire data/_bin/onlineReport/out pour l'entité FR (forcé par pyspc) à la date 2017-06-13 00:00:00`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t mf_dailyreport -r 20170613


:blue:`Téléchargement du document de type 'mf_monthlyreport' vers le répertoire data/_bin/onlineReport/out pour l'entité 01 à la date 2020-06-12 00:00:00`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -s 01 -O data/_bin/onlineReport/out -t mf_monthlyreport -r 20200612


:blue:`Téléchargement du document de type 'mf_station' vers le répertoire data/_bin/onlineReport/out pour l'entité 07154005 à la date None`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t mf_station -s 07154005


:blue:`Téléchargement du document de type 'mf_warning' vers le répertoire data/_bin/onlineReport/out à la date 2019-11-23 00:00:00`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t mf_warning -r 20191123


:blue:`Téléchargement des informations Vigicrues (service 1.1) de type 'vigicrues-1_geoinfo' vers le répertoire data/_bin/onlineReport/out au format geoJSON.`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t vigicrues-1_geoinfo


:blue:`Téléchargement des informations Vigicrues de type 'vigicrues_fcst' vers le répertoire data/_bin/onlineReport/out pour l'entité K118001010 et la grandeur Q`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t vigicrues_fcst -s K118001010 -n Q


:blue:`Téléchargement des informations Vigicrues de type 'vigicrues_loc' vers le répertoire data/_bin/onlineReport/out pour l'entité K118001010.`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t vigicrues_loc -s K118001010


:blue:`Téléchargement des informations Vigicrues de type 'vigicrues_obs' vers le répertoire data/_bin/onlineReport/out pour l'entité K118001010 et la grandeur Q`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t vigicrues_obs -s K118001010 -n Q


:blue:`Téléchargement des informations Vigicrues de type 'vigicrues_sandre' vers le répertoire data/_bin/onlineReport/out pour l'entité K118001010 et la grandeur Q`

.. container:: cmdimg

   .. container:: cmdline

      onlineReport.py -O data/_bin/onlineReport/out -t vigicrues_sandre -s K118001010 -n Q

