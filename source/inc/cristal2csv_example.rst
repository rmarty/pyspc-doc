
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les données depuis les archives CRISTAL situé dans le répertoire data/data/cristal au format csv de type 'pyspc' dans le répertoire data/_bin/cristal2csv/out. L'extraction concerne la période du 2008103118 au 2008110200.`

.. container:: cmdimg

   .. container:: cmdline

      cristal2csv.py -I data/data/cristal -O data/_bin/cristal2csv/out -F 2008103118 -L 2008110200 -s K0600010 -C pyspc


:blue:`Extraire les données depuis les archives CRISTAL situé dans le répertoire data/data/cristal au format csv de type 'grp18' dans le répertoire data/_bin/cristal2csv/out. L'extraction concerne la période du 2008103118 au 2008110200.`

.. container:: cmdimg

   .. container:: cmdline

      cristal2csv.py -I data/data/cristal -O data/_bin/cristal2csv/out -F 2008103118 -L 2008110200 -s K0559910 -C grp18

