
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraction des valeurs d'évaporation de l'image radar 'sim evap rr' entre les instants 20180101 et 20180102 sur le domaine 656368,6565385 en ciblant la grandeur EH, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S 656368,6565385 -F 20180101 -L 20180102 -t sim evap rr -n EH


:blue:`Extraction des lames d'eau de l'image entre les instants 202308310915 et 202308311015 sur le domaine LO808 issue de la prévision immédiate 'arome-pi rr total' émise à 2023083109 en ciblant la grandeur P15m, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out. Les lames extraites correspondent aux 'stats standard'.`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO808 -F 202308310915 -L 202308311015 -r 202308310900 -t arome-pi rr total -n P15m -U stats standard


:blue:`Extraction des statistiques de prévision de précipitations émises le 2018110506 de l'image prévue 'sympo rr rr' entre les instants 2018110506 et 2018110806 sur le domaine LO8087 en ciblant la grandeur P3H, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO8087 -F 2018110506 -r 2018110506 -L 2018110806 -t sympo rr rr -n P3H -U stats standard -U precision standard


:blue:`Extraction des valeurs de précipitations de l'image radar 'panthere france rr' entre les instants 201706131730 et 201706131930 sur le domaine Bx772000,6422000,773000,6421000 en ciblant la grandeur P5m, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S Bx772000,6422000,773000,6421000 -F 201706131730 -L 201706131930 -t panthere france rr -n P5m


:blue:`Extraction des statistiques de prévision de précipitations émises le 2022033106 de l'image prévue 'arome rr total' entre les instants 2022033107 et 20220402 sur le domaine LO8087 en ciblant la grandeur PH, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO8087 -F 2022033107 -r 2022033106 -L 2022040200 -t arome rr total -n PH -U stats standard -U precision standard


:blue:`Extraction des statistiques de prévision de précipitations émises le 2022033112 de l'image prévue 'arome-ifs rr total' entre les instants 2022033113 et 2022040212 sur le domaine LO8087 en ciblant la grandeur PH, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO8087 -F 2022033113 -r 2022033112 -L 2022040212 -t arome-ifs rr total -n PH -U stats standard -U precision standard


:blue:`Extraction des lames d'eau de l'image radar 'antilope temps-reel rr' entre les instants 2008103106 et 2008110206 sur le domaine LO8087+LO12844 en ciblant la grandeur PH, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out. Les lames extraites correspondent aux 'stats complete'.`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO8087+LO12844 -F 2008103106 -L 2008110206 -t antilope temps-reel rr -n PH -U stats complete


:blue:`Extraction des valeurs de températures de l'image radar 'sim t t' entre les instants 20180101 et 20180102 sur le domaine LO6498 en ciblant la grandeur TI, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO6498 -F 20180101 -L 20180102 -t sim t t -n TI


:blue:`Extraction des statistiques de prévision de températures émises le 2022033106 de l'image prévue 'arpege t t' entre les instants 2022033106 et 2022040306 sur le domaine LO8087 en ciblant la grandeur TI, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO8087 -F 2022033106 -r 2022033106 -L 2022040306 -t arpege t t -n TI -U stats standard -U precision standard


:blue:`Extraction des statistiques de prévision de températures émises le 2018110506 de l'image prévue 'sympo t t' entre les instants 2018110506 et 2018110806 sur le domaine LO8087 en ciblant la grandeur TI, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S LO8087 -F 2018110506 -r 2018110506 -L 2018110806 -t sympo t t -n TI -U stats standard -U precision standard


:blue:`Extraction des statistiques de prévision de températures émises le 2018110506 de l'image prévue 'sympo t t' entre les instants 2018110506 et 2018110806 sur le domaine 784507,6402274+794057,6413270 en ciblant la grandeur TI, dans un fichier placé dans le répertoire local data/_bin/bdimage2xml/out`

.. container:: cmdimg

   .. container:: cmdline

      bdimage2xml.py -O data/_bin/bdimage2xml/out -S 784507,6402274+794057,6413270 -F 2018110506 -r 2018110506 -L 2018110806 -t sympo t t -n TI

