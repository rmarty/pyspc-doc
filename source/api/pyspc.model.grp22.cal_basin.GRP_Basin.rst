pyspc.model.grp22.cal\_basin.GRP\_Basin
=======================================

.. currentmodule:: pyspc.model.grp22.cal_basin

.. autoclass:: GRP_Basin

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Basin.__init__
      ~GRP_Basin.clear
      ~GRP_Basin.convert
      ~GRP_Basin.copy
      ~GRP_Basin.from_bool
      ~GRP_Basin.from_csv
      ~GRP_Basin.from_datetime
      ~GRP_Basin.from_dictoffloat
      ~GRP_Basin.from_dictofint
      ~GRP_Basin.from_dictofstr
      ~GRP_Basin.from_float
      ~GRP_Basin.from_int
      ~GRP_Basin.from_listoffloat
      ~GRP_Basin.from_listofint
      ~GRP_Basin.from_listofintorstr
      ~GRP_Basin.from_listofstr
      ~GRP_Basin.from_multitxt
      ~GRP_Basin.fromkeys
      ~GRP_Basin.get
      ~GRP_Basin.items
      ~GRP_Basin.join_basename
      ~GRP_Basin.keys
      ~GRP_Basin.list_ordered_options
      ~GRP_Basin.list_sections_options
      ~GRP_Basin.list_unique_options
      ~GRP_Basin.move_to_end
      ~GRP_Basin.pop
      ~GRP_Basin.popitem
      ~GRP_Basin.read
      ~GRP_Basin.setdefault
      ~GRP_Basin.split_basename
      ~GRP_Basin.to_bool
      ~GRP_Basin.to_csv
      ~GRP_Basin.to_datetime
      ~GRP_Basin.to_datetimeformat
      ~GRP_Basin.to_dictoffloat
      ~GRP_Basin.to_dictofint
      ~GRP_Basin.to_dictofstr
      ~GRP_Basin.to_float
      ~GRP_Basin.to_int
      ~GRP_Basin.to_listofdatetime
      ~GRP_Basin.to_listoffloat
      ~GRP_Basin.to_listofint
      ~GRP_Basin.to_listofintorstr
      ~GRP_Basin.to_listofstr
      ~GRP_Basin.to_multitxt
      ~GRP_Basin.to_path
      ~GRP_Basin.update
      ~GRP_Basin.update_config
      ~GRP_Basin.values
      ~GRP_Basin.write
   
   

   
   
   