pyspc.model.plathynes.observation.Data
======================================

.. currentmodule:: pyspc.model.plathynes.observation

.. autoclass:: Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Data.__init__
      ~Data.define_file_metadata
      ~Data.read
      ~Data.read_mgr
      ~Data.read_mqoi
      ~Data.write
      ~Data.write_mgr
      ~Data.write_mhoi
      ~Data.write_mqoi
   
   

   
   
   