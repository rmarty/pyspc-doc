pyspc.data.meteofrance.MF\_Data
===============================

.. currentmodule:: pyspc.data.meteofrance

.. autoclass:: MF_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MF_Data.__init__
      ~MF_Data.read
      ~MF_Data.write
   
   

   
   
   