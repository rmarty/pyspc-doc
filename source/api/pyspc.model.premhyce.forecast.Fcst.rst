pyspc.model.premhyce.forecast.Fcst
==================================

.. currentmodule:: pyspc.model.premhyce.forecast

.. autoclass:: Fcst

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Fcst.__init__
      ~Fcst.join_basename
      ~Fcst.read
      ~Fcst.split_basename
      ~Fcst.write
   
   

   
   
   