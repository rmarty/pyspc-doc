pyspc.metadata.shyreg.precipitation.Precipitation
=================================================

.. currentmodule:: pyspc.metadata.shyreg.precipitation

.. autoclass:: Precipitation

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Precipitation.__init__
      ~Precipitation.read
   
   

   
   
   