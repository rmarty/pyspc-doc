pyspc.data.vigicrues.fcst.Vigicrues\_Fcst
=========================================

.. currentmodule:: pyspc.data.vigicrues.fcst

.. autoclass:: Vigicrues_Fcst

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Vigicrues_Fcst.__init__
      ~Vigicrues_Fcst.get_trends
      ~Vigicrues_Fcst.get_varnames
      ~Vigicrues_Fcst.read
   
   

   
   
   