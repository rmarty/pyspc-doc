pyspc.model.grp22.tdelta
========================

.. automodule:: pyspc.model.grp22.tdelta

   
   
   .. rubric:: Functions

   .. autosummary::
   
      str2td
      td2str
   
   

   
   
   

   
   
   