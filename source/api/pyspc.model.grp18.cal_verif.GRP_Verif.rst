pyspc.model.grp18.cal\_verif.GRP\_Verif
=======================================

.. currentmodule:: pyspc.model.grp18.cal_verif

.. autoclass:: GRP_Verif

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Verif.__init__
      ~GRP_Verif.check_datatype
      ~GRP_Verif.get_datatypes
      ~GRP_Verif.read
      ~GRP_Verif.split_basename
   
   

   
   
   