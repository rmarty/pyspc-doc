pyspc.core.reservoir.Table
==========================

.. currentmodule:: pyspc.core.reservoir

.. autoclass:: Table

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Table.__init__
      ~Table.apply_zdz
      ~Table.convert
      ~Table.find_line
      ~Table.get_types
      ~Table.isover_zdz
      ~Table.load
      ~Table.plot
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Table.assoc
      ~Table.cols
      ~Table.datatype
      ~Table.filename
      ~Table.name
      ~Table.ratios
      ~Table.table
   
   