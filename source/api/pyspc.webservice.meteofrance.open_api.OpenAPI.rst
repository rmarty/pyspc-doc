pyspc.webservice.meteofrance.open\_api.OpenAPI
==============================================

.. currentmodule:: pyspc.webservice.meteofrance.open_api

.. autoclass:: OpenAPI

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~OpenAPI.__init__
      ~OpenAPI.check_dtype
      ~OpenAPI.get
      ~OpenAPI.get_data
      ~OpenAPI.get_loc
      ~OpenAPI.get_loc_meta
      ~OpenAPI.login
      ~OpenAPI.logout
      ~OpenAPI.retrieve
      ~OpenAPI.retrieve_data
      ~OpenAPI.retrieve_loc
      ~OpenAPI.retrieve_loc_meta
   
   

   
   
   