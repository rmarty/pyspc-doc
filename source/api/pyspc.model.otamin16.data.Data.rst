pyspc.model.otamin16.data.Data
==============================

.. currentmodule:: pyspc.model.otamin16.data

.. autoclass:: Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Data.__init__
      ~Data.join_basename
      ~Data.read
      ~Data.split_basename
      ~Data.write
   
   

   
   
   