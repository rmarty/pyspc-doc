pyspc.webservice.report.report.OnlineReport
===========================================

.. currentmodule:: pyspc.webservice.report.report

.. autoclass:: OnlineReport

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~OnlineReport.__init__
      ~OnlineReport.get_reporttypes
      ~OnlineReport.retrieve
      ~OnlineReport.retrieve_byrequests
      ~OnlineReport.retrieve_byurllib
      ~OnlineReport.set_filename
      ~OnlineReport.set_filename_bycode
      ~OnlineReport.set_filename_bycodedatetype
      ~OnlineReport.set_filename_bycodevarname
      ~OnlineReport.set_filename_bydate
      ~OnlineReport.set_filename_mf_monthlyreport
      ~OnlineReport.set_filename_noarg
      ~OnlineReport.set_url
      ~OnlineReport.set_url_bycode
      ~OnlineReport.set_url_mf_dailyreport
      ~OnlineReport.set_url_mf_monthlyreport
      ~OnlineReport.set_url_mf_warning
      ~OnlineReport.set_url_noarg
      ~OnlineReport.set_url_vigicrues
      ~OnlineReport.set_url_vigicrues1
      ~OnlineReport.set_verify
   
   

   
   
   