pyspc.core.ratingcurve.RatingCurves
===================================

.. currentmodule:: pyspc.core.ratingcurve

.. autoclass:: RatingCurves

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~RatingCurves.__init__
      ~RatingCurves.add
      ~RatingCurves.check_overlapping
      ~RatingCurves.clear
      ~RatingCurves.copy
      ~RatingCurves.extend
      ~RatingCurves.fromkeys
      ~RatingCurves.get
      ~RatingCurves.items
      ~RatingCurves.keys
      ~RatingCurves.move_to_end
      ~RatingCurves.pop
      ~RatingCurves.popitem
      ~RatingCurves.refresh
      ~RatingCurves.refresh_codes
      ~RatingCurves.refresh_nums
      ~RatingCurves.refresh_providers
      ~RatingCurves.refresh_timeintervals
      ~RatingCurves.select
      ~RatingCurves.setdefault
      ~RatingCurves.update
      ~RatingCurves.values
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~RatingCurves.codes
      ~RatingCurves.name
      ~RatingCurves.nums
      ~RatingCurves.providers
      ~RatingCurves.timeintervals
   
   