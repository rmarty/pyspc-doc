pyspc.webservice.meteofrance.open\_data.OpenData
================================================

.. currentmodule:: pyspc.webservice.meteofrance.open_data

.. autoclass:: OpenData

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~OpenData.__init__
      ~OpenData.check_timestep
      ~OpenData.get
      ~OpenData.get_timesteps
      ~OpenData.login
      ~OpenData.logout
      ~OpenData.retrieve
      ~OpenData.set_basename
   
   

   
   
   