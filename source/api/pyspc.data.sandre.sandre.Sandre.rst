pyspc.data.sandre.sandre.Sandre
===============================

.. currentmodule:: pyspc.data.sandre.sandre

.. autoclass:: Sandre

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Sandre.__init__
      ~Sandre.check_dtype
      ~Sandre.concat
      ~Sandre.get_types
      ~Sandre.process_comsim
      ~Sandre.read
      ~Sandre.write
   
   

   
   
   