pyspc.core.serie.Serie
======================

.. currentmodule:: pyspc.core.serie

.. autoclass:: Serie

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Serie.__init__
      ~Serie.above_threshold
      ~Serie.apply_RatingCurves
      ~Serie.apply_Reservoir
      ~Serie.apply_ReservoirTable
      ~Serie.apply_ReservoirZ0
      ~Serie.below_threshold
      ~Serie.between_dates
      ~Serie.comp
      ~Serie.copy
      ~Serie.copy_year
      ~Serie.counter_missing
      ~Serie.cumsum
      ~Serie.deepcopy
      ~Serie.describe
      ~Serie.downscale
      ~Serie.etp_oudin
      ~Serie.events
      ~Serie.events_basic
      ~Serie.events_scipy
      ~Serie.fill_constant
      ~Serie.fill_linear_interpolation
      ~Serie.find_annual_max
      ~Serie.find_annual_min
      ~Serie.first_valid_index
      ~Serie.last_valid_index
      ~Serie.max
      ~Serie.min
      ~Serie.nearlyequalscale
      ~Serie.plot
      ~Serie.regime
      ~Serie.regime_sauquet
      ~Serie.regularscale
      ~Serie.reindex
      ~Serie.set_timezone
      ~Serie.shift
      ~Serie.sim2fcst
      ~Serie.socose
      ~Serie.split
      ~Serie.standardize
      ~Serie.stripna
      ~Serie.subhourlyscale
      ~Serie.timecentroid
      ~Serie.timelag
      ~Serie.update
      ~Serie.upscale
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Serie.code
      ~Serie.data_frame
      ~Serie.df
      ~Serie.dtfmt
      ~Serie.fill
      ~Serie.firstdt
      ~Serie.lastdt
      ~Serie.length
      ~Serie.location
      ~Serie.long_varname
      ~Serie.missing
      ~Serie.np_dtype
      ~Serie.parameter
      ~Serie.provider
      ~Serie.spc_varname
      ~Serie.timestep
      ~Serie.timeunits
      ~Serie.timezone
      ~Serie.units
      ~Serie.varname
      ~Serie.warning
   
   