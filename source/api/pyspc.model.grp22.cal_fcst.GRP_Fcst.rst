pyspc.model.grp22.cal\_fcst.GRP\_Fcst
=====================================

.. currentmodule:: pyspc.model.grp22.cal_fcst

.. autoclass:: GRP_Fcst

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Fcst.__init__
      ~GRP_Fcst.read
      ~GRP_Fcst.split_basename
      ~GRP_Fcst.write
   
   

   
   
   