pyspc.model.grp16.rt\_data.GRPRT\_Data
======================================

.. currentmodule:: pyspc.model.grp16.rt_data

.. autoclass:: GRPRT_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Data.__init__
      ~GRPRT_Data.get_lineprefix
      ~GRPRT_Data.get_metadata
      ~GRPRT_Data.read
      ~GRPRT_Data.write
   
   

   
   
   