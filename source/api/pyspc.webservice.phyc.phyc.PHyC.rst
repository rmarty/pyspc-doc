pyspc.webservice.phyc.phyc.PHyC
===============================

.. currentmodule:: pyspc.webservice.phyc.phyc

.. autoclass:: PHyC

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PHyC.__init__
      ~PHyC.check_client
      ~PHyC.check_dtype
      ~PHyC.check_session
      ~PHyC.check_tstep
      ~PHyC.check_varname
      ~PHyC.get
      ~PHyC.get_data_fcst_hydro
      ~PHyC.get_data_obs_hydro
      ~PHyC.get_data_obs_meteo
      ~PHyC.get_datatypes
      ~PHyC.get_flowmes
      ~PHyC.get_levelcor
      ~PHyC.get_loc_hydro
      ~PHyC.get_loc_meteo
      ~PHyC.get_ratingcurve
      ~PHyC.get_user
      ~PHyC.get_varnames
      ~PHyC.login
      ~PHyC.logout
      ~PHyC.request_capteur_hydro
      ~PHyC.request_capteur_hydro_by_station
      ~PHyC.request_data_fcst_hydro
      ~PHyC.request_data_obs_hydro
      ~PHyC.request_data_obs_meteo
      ~PHyC.request_flowmes
      ~PHyC.request_levelcor
      ~PHyC.request_loc_meteo
      ~PHyC.request_ratingcurve
      ~PHyC.request_site_hydro
      ~PHyC.request_site_hydro_by_zone
      ~PHyC.request_station_hydro
      ~PHyC.request_station_hydro_by_site
      ~PHyC.request_user
      ~PHyC.request_user_loc_meteo
      ~PHyC.request_user_site_hydro
      ~PHyC.request_user_station_hydro
      ~PHyC.retrieve
   
   

   
   
   