pyspc.webservice.hydro2.export.Export
=====================================

.. currentmodule:: pyspc.webservice.hydro2.export

.. autoclass:: Export

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Export.__init__
      ~Export.check_dtime
      ~Export.check_dtype
      ~Export.check_precision
      ~Export.check_stations
      ~Export.get_datatypes
      ~Export.get_dtfmt
      ~Export.get_maxperiod
      ~Export.set_export
      ~Export.set_export_crucal
      ~Export.set_export_debcla
      ~Export.set_export_series
      ~Export.set_export_synthese
      ~Export.set_export_tousmois
      ~Export.set_index_export
      ~Export.set_shortfilename
      ~Export.write
   
   

   
   
   