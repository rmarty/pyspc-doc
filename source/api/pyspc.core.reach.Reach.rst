pyspc.core.reach.Reach
======================

.. currentmodule:: pyspc.core.reach

.. autoclass:: Reach

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Reach.__init__
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Reach.code
      ~Reach.locations
      ~Reach.name
      ~Reach.status
      ~Reach.status_dt
   
   