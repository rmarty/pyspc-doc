pyspc.core.exception
====================

.. automodule:: pyspc.core.exception

   
   
   .. rubric:: Functions

   .. autosummary::
   
      DeprecationWarning
      Information
      Warning
      check_bool
      check_dataframe
      check_dict
      check_dt
      check_float
      check_in
      check_int
      check_listlike
      check_notnone
      check_numeric
      check_str
      check_td
      raise_valueerror
      set_default
   
   

   
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      Error
   
   