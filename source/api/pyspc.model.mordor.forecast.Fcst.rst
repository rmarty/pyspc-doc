pyspc.model.mordor.forecast.Fcst
================================

.. currentmodule:: pyspc.model.mordor.forecast

.. autoclass:: Fcst

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Fcst.__init__
      ~Fcst.join_basename
      ~Fcst.read
      ~Fcst.split_basename
      ~Fcst.write
   
   

   
   
   