pyspc.data.vigicrues.data.Vigicrues\_Data
=========================================

.. currentmodule:: pyspc.data.vigicrues.data

.. autoclass:: Vigicrues_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Vigicrues_Data.__init__
      ~Vigicrues_Data.get_varnames
      ~Vigicrues_Data.read
   
   

   
   
   