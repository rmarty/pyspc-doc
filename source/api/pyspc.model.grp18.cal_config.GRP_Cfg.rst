pyspc.model.grp18.cal\_config.GRP\_Cfg
======================================

.. currentmodule:: pyspc.model.grp18.cal_config

.. autoclass:: GRP_Cfg

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Cfg.__init__
      ~GRP_Cfg.append
      ~GRP_Cfg.check_run
      ~GRP_Cfg.clear
      ~GRP_Cfg.copy
      ~GRP_Cfg.count
      ~GRP_Cfg.extend
      ~GRP_Cfg.get_calibrationvalues
      ~GRP_Cfg.index
      ~GRP_Cfg.insert
      ~GRP_Cfg.pop
      ~GRP_Cfg.product
      ~GRP_Cfg.read
      ~GRP_Cfg.remove
      ~GRP_Cfg.reverse
      ~GRP_Cfg.sort
      ~GRP_Cfg.write
   
   

   
   
   