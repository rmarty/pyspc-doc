pyspc.model.grp18.rt\_basin.GRPRT\_Basin
========================================

.. currentmodule:: pyspc.model.grp18.rt_basin

.. autoclass:: GRPRT_Basin

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Basin.__init__
      ~GRPRT_Basin.clear
      ~GRPRT_Basin.copy
      ~GRPRT_Basin.fromkeys
      ~GRPRT_Basin.get
      ~GRPRT_Basin.items
      ~GRPRT_Basin.keys
      ~GRPRT_Basin.move_to_end
      ~GRPRT_Basin.pop
      ~GRPRT_Basin.popitem
      ~GRPRT_Basin.read
      ~GRPRT_Basin.setdefault
      ~GRPRT_Basin.update
      ~GRPRT_Basin.values
      ~GRPRT_Basin.write
   
   

   
   
   