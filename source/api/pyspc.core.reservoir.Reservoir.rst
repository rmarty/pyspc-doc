pyspc.core.reservoir.Reservoir
==============================

.. currentmodule:: pyspc.core.reservoir

.. autoclass:: Reservoir

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Reservoir.__init__
      ~Reservoir.load
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Reservoir.Z0
      ~Reservoir.code
      ~Reservoir.name
      ~Reservoir.tables
   
   