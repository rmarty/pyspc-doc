pyspc.model.plathynes.results.Results
=====================================

.. currentmodule:: pyspc.model.plathynes.results

.. autoclass:: Results

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Results.__init__
      ~Results.read
      ~Results.write
   
   

   
   
   