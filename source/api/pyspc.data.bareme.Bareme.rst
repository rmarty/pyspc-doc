pyspc.data.bareme.Bareme
========================

.. currentmodule:: pyspc.data.bareme

.. autoclass:: Bareme

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Bareme.__init__
      ~Bareme.check_datatypes
      ~Bareme.check_sql_return
      ~Bareme.close
      ~Bareme.commit
      ~Bareme.connect
      ~Bareme.execute
      ~Bareme.from_datetime
      ~Bareme.get_datatypes
      ~Bareme.read_flowmes
      ~Bareme.read_levelcor
      ~Bareme.read_ratingcurve
      ~Bareme.read_rtc_points
      ~Bareme.read_rtc_power
      ~Bareme.rollback
   
   

   
   
   