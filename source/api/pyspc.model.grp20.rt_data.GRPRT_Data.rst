pyspc.model.grp20.rt\_data.GRPRT\_Data
======================================

.. currentmodule:: pyspc.model.grp20.rt_data

.. autoclass:: GRPRT_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Data.__init__
      ~GRPRT_Data.get_lineprefix
      ~GRPRT_Data.read
      ~GRPRT_Data.split_basename
      ~GRPRT_Data.write
   
   

   
   
   