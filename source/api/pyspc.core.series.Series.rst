pyspc.core.series.Series
========================

.. currentmodule:: pyspc.core.series

.. autoclass:: Series

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Series.__init__
      ~Series.above_threshold
      ~Series.add
      ~Series.apply_RatingCurves
      ~Series.apply_Reservoir
      ~Series.apply_ReservoirTable
      ~Series.apply_ReservoirZ0
      ~Series.asobs
      ~Series.below_threshold
      ~Series.between_dates
      ~Series.check_datatype
      ~Series.check_notempty
      ~Series.check_plottype
      ~Series.check_series
      ~Series.check_unique_code
      ~Series.check_unique_runtime
      ~Series.check_unique_varname
      ~Series.clear
      ~Series.concat
      ~Series.copy
      ~Series.counter_missing
      ~Series.cumsum
      ~Series.describe
      ~Series.describe_asconfig
      ~Series.describe_bydate
      ~Series.downscale
      ~Series.errors
      ~Series.etp_oudin
      ~Series.events
      ~Series.extend
      ~Series.fill_constant
      ~Series.fill_linear_interpolation
      ~Series.find_annual_max
      ~Series.find_annual_min
      ~Series.fromkeys
      ~Series.get
      ~Series.get_plottypes
      ~Series.get_types
      ~Series.items
      ~Series.keys
      ~Series.max
      ~Series.min
      ~Series.move_to_end
      ~Series.nearlyequalscale
      ~Series.percentile
      ~Series.plot
      ~Series.plot_bypandas
      ~Series.plot_series
      ~Series.pop
      ~Series.popitem
      ~Series.refresh
      ~Series.refresh_codes
      ~Series.refresh_meta
      ~Series.refresh_varnames
      ~Series.regime
      ~Series.regime_sauquet
      ~Series.regularscale
      ~Series.replace_keys
      ~Series.set_timezone
      ~Series.setdefault
      ~Series.shift
      ~Series.sim2fcst
      ~Series.socose
      ~Series.split
      ~Series.standardize
      ~Series.stripna
      ~Series.subhourlyscale
      ~Series.timecentroid
      ~Series.timelag
      ~Series.to_BdApbp
      ~Series.to_GRPRT_Archive
      ~Series.to_GRPRT_Data
      ~Series.to_GRPRT_Metscen
      ~Series.to_GRP_Data
      ~Series.to_MF_Data
      ~Series.to_PLATHYNES_Data
      ~Series.to_Prevision19
      ~Series.to_PyspcFile
      ~Series.to_Sandre
      ~Series.to_csv
      ~Series.to_prv
      ~Series.to_xls
      ~Series.update
      ~Series.upscale
      ~Series.values
      ~Series.weighted_average
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Series.codes
      ~Series.datatype
      ~Series.meta
      ~Series.name
      ~Series.varnames
   
   