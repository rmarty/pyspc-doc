pyspc.model.grp20.tdelta
========================

.. automodule:: pyspc.model.grp20.tdelta

   
   
   .. rubric:: Functions

   .. autosummary::
   
      str2td
      td2str
   
   

   
   
   

   
   
   