pyspc.data.cristal.Cristal
==========================

.. currentmodule:: pyspc.data.cristal

.. autoclass:: Cristal

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Cristal.__init__
      ~Cristal.read
      ~Cristal.write
   
   

   
   
   