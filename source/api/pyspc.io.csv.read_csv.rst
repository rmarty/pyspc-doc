pyspc.io.csv.read\_csv
======================

.. currentmodule:: pyspc.io.csv

.. autofunction:: read_csv