pyspc.model.grp18.cal\_basin.GRP\_Basin
=======================================

.. currentmodule:: pyspc.model.grp18.cal_basin

.. autoclass:: GRP_Basin

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Basin.__init__
      ~GRP_Basin.clear
      ~GRP_Basin.copy
      ~GRP_Basin.fromkeys
      ~GRP_Basin.get
      ~GRP_Basin.items
      ~GRP_Basin.join_basename
      ~GRP_Basin.keys
      ~GRP_Basin.move_to_end
      ~GRP_Basin.pop
      ~GRP_Basin.popitem
      ~GRP_Basin.read
      ~GRP_Basin.setdefault
      ~GRP_Basin.split_basename
      ~GRP_Basin.update
      ~GRP_Basin.values
      ~GRP_Basin.write
   
   

   
   
   