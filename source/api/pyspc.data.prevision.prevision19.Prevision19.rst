pyspc.data.prevision.prevision19.Prevision19
============================================

.. currentmodule:: pyspc.data.prevision.prevision19

.. autoclass:: Prevision19

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Prevision19.__init__
      ~Prevision19.check_sql_return
      ~Prevision19.close
      ~Prevision19.commit
      ~Prevision19.connect
      ~Prevision19.execute
      ~Prevision19.from_datetime
      ~Prevision19.get_datatypes
      ~Prevision19.insert_fcst
      ~Prevision19.insert_models
      ~Prevision19.insert_series
      ~Prevision19.insert_values
      ~Prevision19.lastrowid
      ~Prevision19.read_fcst
      ~Prevision19.read_models
      ~Prevision19.read_series
      ~Prevision19.read_values
      ~Prevision19.rollback
      ~Prevision19.unique_series
   
   

   
   
   