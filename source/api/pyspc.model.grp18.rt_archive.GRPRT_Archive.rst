pyspc.model.grp18.rt\_archive.GRPRT\_Archive
============================================

.. currentmodule:: pyspc.model.grp18.rt_archive

.. autoclass:: GRPRT_Archive

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Archive.__init__
      ~GRPRT_Archive.get_varnames
      ~GRPRT_Archive.read
      ~GRPRT_Archive.split_basename
      ~GRPRT_Archive.write
   
   

   
   
   