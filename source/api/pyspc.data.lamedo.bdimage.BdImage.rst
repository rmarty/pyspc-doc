pyspc.data.lamedo.bdimage.BdImage
=================================

.. currentmodule:: pyspc.data.lamedo.bdimage

.. autoclass:: BdImage

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~BdImage.__init__
      ~BdImage.read
   
   

   
   
   