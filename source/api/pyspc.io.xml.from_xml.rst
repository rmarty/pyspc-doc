pyspc.io.xml.from\_xml
======================

.. automodule:: pyspc.io.xml.from_xml

   
   
   .. rubric:: Functions

   .. autosummary::
   
      apply_cast
      from_file
      get_attrib
      get_value
      remove_namespace
   
   

   
   
   

   
   
   