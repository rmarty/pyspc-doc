pyspc.core.pyspcfile.PyspcFile
==============================

.. currentmodule:: pyspc.core.pyspcfile

.. autoclass:: PyspcFile

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PyspcFile.__init__
      ~PyspcFile.get_varnames
      ~PyspcFile.join_basename
      ~PyspcFile.read
      ~PyspcFile.split_basename
      ~PyspcFile.write
   
   

   
   
   