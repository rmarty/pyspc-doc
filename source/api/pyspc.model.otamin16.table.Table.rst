pyspc.model.otamin16.table.Table
================================

.. currentmodule:: pyspc.model.otamin16.table

.. autoclass:: Table

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Table.__init__
      ~Table.join_basename
      ~Table.read
      ~Table.split_basename
      ~Table.write
   
   

   
   
   