pyspc.core.ratingcurve.RatingCurve
==================================

.. currentmodule:: pyspc.core.ratingcurve

.. autoclass:: RatingCurve

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~RatingCurve.__init__
      ~RatingCurve.check_flowmes
      ~RatingCurve.check_hq
      ~RatingCurve.check_levelcor
      ~RatingCurve.check_update_dt
      ~RatingCurve.check_valid_dt
      ~RatingCurve.check_valid_interval
      ~RatingCurve.convert
      ~RatingCurve.interpolate_levelcor
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~RatingCurve.code
      ~RatingCurve.flowmes
      ~RatingCurve.h
      ~RatingCurve.hq
      ~RatingCurve.levelcor
      ~RatingCurve.num
      ~RatingCurve.provider
      ~RatingCurve.q
      ~RatingCurve.timeinterval
      ~RatingCurve.update_dt
      ~RatingCurve.valid_dt
      ~RatingCurve.valid_interval
   
   