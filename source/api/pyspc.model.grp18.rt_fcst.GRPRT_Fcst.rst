pyspc.model.grp18.rt\_fcst.GRPRT\_Fcst
======================================

.. currentmodule:: pyspc.model.grp18.rt_fcst

.. autoclass:: GRPRT_Fcst

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Fcst.__init__
      ~GRPRT_Fcst.check_datatype
      ~GRPRT_Fcst.get_fileprefix
      ~GRPRT_Fcst.get_lineprefix
      ~GRPRT_Fcst.get_types
      ~GRPRT_Fcst.read
      ~GRPRT_Fcst.write
   
   

   
   
   