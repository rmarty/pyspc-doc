pyspc.metadata.vigicrues.location.Vigicrues\_Location
=====================================================

.. currentmodule:: pyspc.metadata.vigicrues.location

.. autoclass:: Vigicrues_Location

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Vigicrues_Location.__init__
      ~Vigicrues_Location.read
   
   

   
   
   