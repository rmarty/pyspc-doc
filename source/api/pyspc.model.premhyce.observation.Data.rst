pyspc.model.premhyce.observation.Data
=====================================

.. currentmodule:: pyspc.model.premhyce.observation

.. autoclass:: Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Data.__init__
      ~Data.read
      ~Data.write
   
   

   
   
   