pyspc.webservice.meteofrance.open\_api.\_MF\_Client
===================================================

.. currentmodule:: pyspc.webservice.meteofrance.open_api

.. autoclass:: _MF_Client

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~_MF_Client.__init__
      ~_MF_Client.obtain_token
      ~_MF_Client.request
      ~_MF_Client.token_has_expired
   
   

   
   
   