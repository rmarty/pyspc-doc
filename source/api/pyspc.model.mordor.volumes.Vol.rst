pyspc.model.mordor.volumes.Vol
==============================

.. currentmodule:: pyspc.model.mordor.volumes

.. autoclass:: Vol

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Vol.__init__
      ~Vol.join_basename
      ~Vol.read
      ~Vol.split_basename
      ~Vol.write
   
   

   
   
   