pyspc.model.grp16.rt\_intern.GRPRT\_Intern
==========================================

.. currentmodule:: pyspc.model.grp16.rt_intern

.. autoclass:: GRPRT_Intern

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Intern.__init__
      ~GRPRT_Intern.check_datatype
      ~GRPRT_Intern.get_types
      ~GRPRT_Intern.read
      ~GRPRT_Intern.split_basename
      ~GRPRT_Intern.write
   
   

   
   
   