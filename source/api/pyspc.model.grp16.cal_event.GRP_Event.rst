pyspc.model.grp16.cal\_event.GRP\_Event
=======================================

.. currentmodule:: pyspc.model.grp16.cal_event

.. autoclass:: GRP_Event

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Event.__init__
      ~GRP_Event.read
      ~GRP_Event.split_basename
      ~GRP_Event.write
   
   

   
   
   