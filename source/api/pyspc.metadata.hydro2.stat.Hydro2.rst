pyspc.metadata.hydro2.stat.Hydro2
=================================

.. currentmodule:: pyspc.metadata.hydro2.stat

.. autoclass:: Hydro2

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Hydro2.__init__
      ~Hydro2.get_datatypes
      ~Hydro2.read
      ~Hydro2.write
   
   

   
   
   