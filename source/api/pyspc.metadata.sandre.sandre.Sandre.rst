pyspc.metadata.sandre.sandre.Sandre
===================================

.. currentmodule:: pyspc.metadata.sandre.sandre

.. autoclass:: Sandre

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Sandre.__init__
      ~Sandre.check_dtype
      ~Sandre.get_types
      ~Sandre.read
   
   

   
   
   