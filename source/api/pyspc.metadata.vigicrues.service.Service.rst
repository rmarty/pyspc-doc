pyspc.metadata.vigicrues.service.Service
========================================

.. currentmodule:: pyspc.metadata.vigicrues.service

.. autoclass:: Service

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Service.__init__
      ~Service.read
   
   

   
   
   