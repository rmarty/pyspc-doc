pyspc.data.meteofrance.MF\_OpenAPI
==================================

.. currentmodule:: pyspc.data.meteofrance

.. autoclass:: MF_OpenAPI

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MF_OpenAPI.__init__
      ~MF_OpenAPI.read
      ~MF_OpenAPI.split_basename
   
   

   
   
   