pyspc.model.otamin18.table.Table
================================

.. currentmodule:: pyspc.model.otamin18.table

.. autoclass:: Table

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Table.__init__
      ~Table.join_basename
      ~Table.read
      ~Table.split_basename
      ~Table.write
   
   

   
   
   