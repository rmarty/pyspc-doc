pyspc.data.prevision.prevision17.Prevision17
============================================

.. currentmodule:: pyspc.data.prevision.prevision17

.. autoclass:: Prevision17

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Prevision17.__init__
      ~Prevision17.check_sql_return
      ~Prevision17.close
      ~Prevision17.commit
      ~Prevision17.connect
      ~Prevision17.execute
      ~Prevision17.from_datetime
      ~Prevision17.get_datatypes
      ~Prevision17.read_fcst
      ~Prevision17.read_fcst_hydro3
      ~Prevision17.read_serie_hydro3
      ~Prevision17.rollback
   
   

   
   
   