pyspc.model.plathynes.config.Config
===================================

.. currentmodule:: pyspc.model.plathynes.config

.. autoclass:: Config

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Config.__init__
      ~Config.clear
      ~Config.copy
      ~Config.fromkeys
      ~Config.get
      ~Config.get_ext
      ~Config.get_types
      ~Config.items
      ~Config.keys
      ~Config.list_sections_options
      ~Config.move_to_end
      ~Config.pop
      ~Config.popitem
      ~Config.read
      ~Config.setdefault
      ~Config.update
      ~Config.update_config
      ~Config.values
      ~Config.write
   
   

   
   
   