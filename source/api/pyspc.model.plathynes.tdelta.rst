pyspc.model.plathynes.tdelta
============================

.. automodule:: pyspc.model.plathynes.tdelta

   
   
   .. rubric:: Functions

   .. autosummary::
   
      str2td
      td2str
   
   

   
   
   

   
   
   