pyspc.data.lamedo.bdapbp.BdApbp
===============================

.. currentmodule:: pyspc.data.lamedo.bdapbp

.. autoclass:: BdApbp

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~BdApbp.__init__
      ~BdApbp.check_datatypes
      ~BdApbp.get_types
      ~BdApbp.read
      ~BdApbp.write
   
   

   
   
   