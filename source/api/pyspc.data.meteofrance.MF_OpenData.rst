pyspc.data.meteofrance.MF\_OpenData
===================================

.. currentmodule:: pyspc.data.meteofrance

.. autoclass:: MF_OpenData

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MF_OpenData.__init__
      ~MF_OpenData.read
      ~MF_OpenData.split_basename
   
   

   
   
   