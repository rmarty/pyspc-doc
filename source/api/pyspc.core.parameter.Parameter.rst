pyspc.core.parameter.Parameter
==============================

.. currentmodule:: pyspc.core.parameter

.. autoclass:: Parameter

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Parameter.__init__
      ~Parameter.apply_RatingCurves
      ~Parameter.apply_ReservoirTable
      ~Parameter.apply_ReservoirZ0
      ~Parameter.find
      ~Parameter.get_spcvarnames
      ~Parameter.infer_timestep
      ~Parameter.isDownscalable
      ~Parameter.isNearlyequalscalable
      ~Parameter.isUpscalable
      ~Parameter.set_missing_values
      ~Parameter.to_regularscale
      ~Parameter.to_subhourlyscale
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Parameter.cumulable
      ~Parameter.dtfmt
      ~Parameter.long_varname
      ~Parameter.missing
      ~Parameter.np_dtype
      ~Parameter.provider
      ~Parameter.spc_varname
      ~Parameter.timestep
      ~Parameter.timeunits
      ~Parameter.units
      ~Parameter.varname
   
   