pyspc.data.meteofrance.BP\_Data
===============================

.. currentmodule:: pyspc.data.meteofrance

.. autoclass:: BP_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~BP_Data.__init__
      ~BP_Data.read
   
   

   
   
   