pyspc.model.grp20.cal\_data.GRP\_Data
=====================================

.. currentmodule:: pyspc.model.grp20.cal_data

.. autoclass:: GRP_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRP_Data.__init__
      ~GRP_Data.get_varnames
      ~GRP_Data.join_basename
      ~GRP_Data.read
      ~GRP_Data.split_basename
      ~GRP_Data.write
   
   

   
   
   