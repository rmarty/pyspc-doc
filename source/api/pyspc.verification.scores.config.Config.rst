pyspc.verification.scores.config.Config
=======================================

.. currentmodule:: pyspc.verification.scores.config

.. autoclass:: Config

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Config.__init__
      ~Config.check_sections_options
      ~Config.check_version
      ~Config.clear
      ~Config.convert
      ~Config.copy
      ~Config.from_bool
      ~Config.from_csv
      ~Config.from_datetime
      ~Config.from_dictoffloat
      ~Config.from_dictofint
      ~Config.from_dictofstr
      ~Config.from_float
      ~Config.from_int
      ~Config.from_listoffloat
      ~Config.from_listofint
      ~Config.from_listofintorstr
      ~Config.from_listofstr
      ~Config.from_multitxt
      ~Config.fromkeys
      ~Config.get
      ~Config.get_cfg_keys
      ~Config.items
      ~Config.keys
      ~Config.list_ordered_options
      ~Config.list_sections_options
      ~Config.list_unique_options
      ~Config.move_to_end
      ~Config.pop
      ~Config.popitem
      ~Config.read
      ~Config.setdefault
      ~Config.to_bool
      ~Config.to_csv
      ~Config.to_datetime
      ~Config.to_datetimeformat
      ~Config.to_dictoffloat
      ~Config.to_dictofint
      ~Config.to_dictofstr
      ~Config.to_float
      ~Config.to_int
      ~Config.to_listofdatetime
      ~Config.to_listoffloat
      ~Config.to_listofint
      ~Config.to_listofintorstr
      ~Config.to_listofstr
      ~Config.to_multitxt
      ~Config.to_path
      ~Config.update
      ~Config.update_config
      ~Config.values
      ~Config.write
   
   

   
   
   