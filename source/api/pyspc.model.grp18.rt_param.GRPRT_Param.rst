pyspc.model.grp18.rt\_param.GRPRT\_Param
========================================

.. currentmodule:: pyspc.model.grp18.rt_param

.. autoclass:: GRPRT_Param

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GRPRT_Param.__init__
      ~GRPRT_Param.clear
      ~GRPRT_Param.copy
      ~GRPRT_Param.fromkeys
      ~GRPRT_Param.get
      ~GRPRT_Param.items
      ~GRPRT_Param.keys
      ~GRPRT_Param.move_to_end
      ~GRPRT_Param.pop
      ~GRPRT_Param.popitem
      ~GRPRT_Param.read
      ~GRPRT_Param.setdefault
      ~GRPRT_Param.update
      ~GRPRT_Param.values
      ~GRPRT_Param.write
   
   

   
   
   