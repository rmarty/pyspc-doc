pyspc.metadata.refspc.refspc.RefSPC
===================================

.. currentmodule:: pyspc.metadata.refspc.refspc

.. autoclass:: RefSPC

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~RefSPC.__init__
      ~RefSPC.check_sql_return
      ~RefSPC.close
      ~RefSPC.commit
      ~RefSPC.connect
      ~RefSPC.execute
      ~RefSPC.get
      ~RefSPC.get_datatypes
      ~RefSPC.get_loc_hydro
      ~RefSPC.get_loc_meteo
      ~RefSPC.get_reach
      ~RefSPC.get_stat_hydro
      ~RefSPC.get_stat_meteo
      ~RefSPC.lastrowid
      ~RefSPC.rollback
   
   

   
   
   