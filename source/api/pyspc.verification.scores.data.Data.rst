pyspc.verification.scores.data.Data
===================================

.. currentmodule:: pyspc.verification.scores.data

.. autoclass:: Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Data.__init__
      ~Data.get_types
      ~Data.read
      ~Data.write
   
   

   
   
   