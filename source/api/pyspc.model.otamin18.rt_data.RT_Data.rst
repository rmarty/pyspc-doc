pyspc.model.otamin18.rt\_data.RT\_Data
======================================

.. currentmodule:: pyspc.model.otamin18.rt_data

.. autoclass:: RT_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~RT_Data.__init__
      ~RT_Data.get_types
      ~RT_Data.read
      ~RT_Data.write
   
   

   
   
   