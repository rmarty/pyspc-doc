pyspc.core.timeutil
===================

.. automodule:: pyspc.core.timeutil

   
   
   .. rubric:: Functions

   .. autosummary::
   
      dtfmt
      dtheader
      group_dates
      group_events
      lenstr2dtfmt
      overlap
      str2dt
   
   

   
   
   

   
   
   