pyspc.core.reach.Reaches
========================

.. currentmodule:: pyspc.core.reach

.. autoclass:: Reaches

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Reaches.__init__
      ~Reaches.add
      ~Reaches.check_reaches
      ~Reaches.clear
      ~Reaches.copy
      ~Reaches.from_Config
      ~Reaches.fromkeys
      ~Reaches.get
      ~Reaches.items
      ~Reaches.keys
      ~Reaches.move_to_end
      ~Reaches.pop
      ~Reaches.popitem
      ~Reaches.refresh_codes
      ~Reaches.setdefault
      ~Reaches.to_Config
      ~Reaches.update
      ~Reaches.values
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Reaches.codes
      ~Reaches.name
   
   