pyspc.data.sacha.data.Sacha
===========================

.. currentmodule:: pyspc.data.sacha.data

.. autoclass:: Sacha

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Sacha.__init__
      ~Sacha.check_datatypes
      ~Sacha.check_hydrotypes
      ~Sacha.check_prcp_src
      ~Sacha.check_sql_return
      ~Sacha.check_varnames
      ~Sacha.close
      ~Sacha.commit
      ~Sacha.connect
      ~Sacha.execute
      ~Sacha.from_datetime
      ~Sacha.get_datacoverage
      ~Sacha.get_datatypes
      ~Sacha.get_hydrotypes
      ~Sacha.get_inventory
      ~Sacha.get_locations
      ~Sacha.get_prcp_src
      ~Sacha.get_varnames
      ~Sacha.insert_locations
      ~Sacha.read
      ~Sacha.rollback
   
   

   
   
   