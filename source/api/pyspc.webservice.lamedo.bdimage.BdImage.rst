pyspc.webservice.lamedo.bdimage.BdImage
=======================================

.. currentmodule:: pyspc.webservice.lamedo.bdimage

.. autoclass:: BdImage

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~BdImage.__init__
      ~BdImage.check_client
      ~BdImage.check_image
      ~BdImage.check_precision
      ~BdImage.check_start
      ~BdImage.check_stats
      ~BdImage.get
      ~BdImage.get_datatypes
      ~BdImage.get_precision
      ~BdImage.get_stats
      ~BdImage.retrieve
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~BdImage.client
      ~BdImage.webservices
   
   