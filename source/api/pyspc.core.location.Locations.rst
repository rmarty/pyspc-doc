pyspc.core.location.Locations
=============================

.. currentmodule:: pyspc.core.location

.. autoclass:: Locations

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Locations.__init__
      ~Locations.add
      ~Locations.check_locs
      ~Locations.clear
      ~Locations.copy
      ~Locations.from_Config
      ~Locations.fromkeys
      ~Locations.get
      ~Locations.items
      ~Locations.keys
      ~Locations.move_to_end
      ~Locations.pop
      ~Locations.popitem
      ~Locations.refresh_codes
      ~Locations.setdefault
      ~Locations.to_Config
      ~Locations.update
      ~Locations.values
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Locations.codes
      ~Locations.name
   
   