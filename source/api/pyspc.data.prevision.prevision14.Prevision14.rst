pyspc.data.prevision.prevision14.Prevision14
============================================

.. currentmodule:: pyspc.data.prevision.prevision14

.. autoclass:: Prevision14

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Prevision14.__init__
      ~Prevision14.check_sql_return
      ~Prevision14.close
      ~Prevision14.commit
      ~Prevision14.connect
      ~Prevision14.execute
      ~Prevision14.from_datetime
      ~Prevision14.get_datatypes
      ~Prevision14.read_fcst
      ~Prevision14.read_fcst_hydro2
      ~Prevision14.read_fcst_hydro3
      ~Prevision14.read_serie_hydro2
      ~Prevision14.read_serie_hydro3
      ~Prevision14.rollback
   
   

   
   
   