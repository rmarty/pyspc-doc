pyspc.plotting.config.Config
============================

.. currentmodule:: pyspc.plotting.config

.. autoclass:: Config

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Config.__init__
      ~Config.clear
      ~Config.convert
      ~Config.copy
      ~Config.fromkeys
      ~Config.get
      ~Config.items
      ~Config.keys
      ~Config.list_sections_options
      ~Config.load
      ~Config.move_to_end
      ~Config.pop
      ~Config.popitem
      ~Config.read
      ~Config.setdefault
      ~Config.update
      ~Config.update_config
      ~Config.values
   
   

   
   
   