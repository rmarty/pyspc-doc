pyspc.core.location.Location
============================

.. currentmodule:: pyspc.core.location

.. autoclass:: Location

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Location.__init__
      ~Location.get_loctypes
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Location.area
      ~Location.code
      ~Location.locality
      ~Location.loctype
      ~Location.longname
      ~Location.name
      ~Location.reach
      ~Location.river
      ~Location.x
      ~Location.y
      ~Location.z
   
   