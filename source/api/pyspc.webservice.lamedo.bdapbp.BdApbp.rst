pyspc.webservice.lamedo.bdapbp.BdApbp
=====================================

.. currentmodule:: pyspc.webservice.lamedo.bdapbp

.. autoclass:: BdApbp

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~BdApbp.__init__
      ~BdApbp.get_types
      ~BdApbp.retrieve
      ~BdApbp.retrieve_byrequests
      ~BdApbp.retrieve_byurllib
      ~BdApbp.set_filename
      ~BdApbp.set_url
   
   

   
   
   