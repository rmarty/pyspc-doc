
.. _apiconvention:

.. currentmodule:: pyspc.convention

Constantes et Paramètres pour pySPC
===================================


Bareme
------

.. autosummary::
    :toctree: api/

    bareme


Cristal
-------

.. autosummary::
    :toctree: api/

    cristal



Base de données
---------------

.. autosummary::
    :toctree: api/

    dbase



GRP
---

.. autosummary::
    :toctree: api/

    grp16
    grp18
    grp20
    grp22



Hydro2
------

.. autosummary::
    :toctree: api/

    hydro2



HydroPortail
------------

.. autosummary::
    :toctree: api/

    hydroportail



Lamedo
------

.. autosummary::
    :toctree: api/

    lamedo



Météo-France
------------

.. autosummary::
    :toctree: api/

    meteofrance



OTAMIN
------

.. autosummary::
    :toctree: api/

    otamin16
    otamin18



PLATHYNES
---------

.. autosummary::
    :toctree: api/

    plathynes



PREMHYCE
--------

.. autosummary::
    :toctree: api/

    premhyce



Prévision du SPC Loire-Allier-Cher-Indre
----------------------------------------

.. autosummary::
    :toctree: api/

    prevision



Format PRV (OTAMIN, SCORES)
---------------------------

.. autosummary::
    :toctree: api/

    prv



Référentiel du SPC Loire-Allier-Cher-Indre
------------------------------------------

.. autosummary::
    :toctree: api/

    refspc



Sacha
-----

.. autosummary::
    :toctree: api/

    sacha



Sandre XML
----------

.. autosummary::
    :toctree: api/

    sandre



Scores
-------

.. autosummary::
    :toctree: api/

    scores



Vigicrues
---------

.. autosummary::
    :toctree: api/

    vigicrues
