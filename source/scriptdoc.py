# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 15:49:38 2019

@author: admin
"""
import ast
import collections
import glob
import os
import sys

MODE_DEBUG = True

BIN_DIRNAME = os.path.join('source', 'bin')
BIN_DIRNAME2 = os.path.join('..', 'bin').replace('\\', '/')
SCRIPT_DIRNAME = os.path.join('source', 'script')
SCRIPT_DIRNAME2 = 'script'
SCRIPT_DIRNAME3 = os.path.join('..', 'script').replace('\\', '/')
INC_DIRNAME = os.path.join('source', 'inc')
INC_DIRNAME2 = os.path.join('..', 'inc')

TITLE_LINES = {
    0: '=',
    1: '-',
    2: '+',
    3: '*',
    4: '~'
}
SYMB_LIST = {
    1: '-',
    2: '   +',
    3: '      *',
    4: '         -',
    5: '            +',
    6: '               *'
}

HEADER = {
    'bdimage2grp': """

Traitement des données BdImages
+++++++++++++++++++++++++++++++

Ces scripts sont dédiés au téléchargement des données BdImages par bloc. Ainsi l'utilisateur peut réduire le risque de refus de requête par le serveur si celle-ci demande trop de données.

""",
    'cristal': """

Traitement des données Cristal
++++++++++++++++++++++++++++++

Ces scripts sont dédiés aux données Cristal.

""",
    'grp20': """

Modélisation GRP: version 2020
++++++++++++++++++++++++++++++

Ces scripts sont dédiés à la création de fichiers pour le calage de modèle GRP ainsi que le calage en masse de modèles GRP dans sa version 2020.

""",
    'grpCal': """

Modélisation GRP: version 2016 (calage)
+++++++++++++++++++++++++++++++++++++++

Ces scripts sont dédiés à la création de fichiers pour le calage de modèle GRP ainsi que le calage en masse de modèles GRP

.. warning::
    Le retrait de ces scripts est prévu pour la version 3.1.0


""",
    'grpRT': """

Modélisation GRP: version 2016 (temps-réel)
+++++++++++++++++++++++++++++++++++++++++++

Ces scripts sont dédiés à la création de fichiers pour le rejeu en masse en temps-différé de son module Temps-Réel.

.. warning::
    Le retrait de ces scripts est prévu pour la version 3.1.0


""",
    'mfReports.py': """

Documentation Météo-France
++++++++++++++++++++++++++

Ce script est dédié au téléchargement des fiches et rapports de Météo-France

""",
    'phyc2xml.py': """

Traitement des données PHyC
+++++++++++++++++++++++++++

Ces scripts sont dédiés au téléchargement des données PHyC par bloc. Ainsi l'utilisateur peut réduire le risque de refus de requête par le serveur si celle-ci demande trop de données.

""",
    'Plathynes': """

Modélisation PLATHYNES
++++++++++++++++++++++

Ce script est dédié à la définition des événements hydrologiques pour Plathynes depuis la PHyC.

""",
    'postEvent': """

Retours d'expérience SPC LACI
+++++++++++++++++++++++++++++

Ces scripts sont dédiés aux retours d'expérience post-crue: récupération des données, rejeu des modèles opérationnels, synthèse graphique et statistique.

""",
    None: """

Divers
++++++

"""
}


def write_script_main(filename=None):
    if filename is None:
        return None
    with open(filename, 'w', encoding='utf-8') as f:
        # =====================================================================
        # TITRE 1 et INTRODUCTION
        # =====================================================================
        f.write(""".. fichier rst créé automatiquement par scriptdoc

.. _script:

.. role:: blue

.. role:: boldblue

Utilisation avancée par script
------------------------------

.. index:: Ligne de commande

.. index:: Script

Afin de réaliser des traitements en masse, le paquet pySPC comprend des scripts. Ceux-ci, placés dans pySPC\script\, ont le caractère _ comme préfixe. Le(s) paramètre(s) y est/sont écrit(s) “en dur”. Certains d’entre eux utilisent des programmes de ce module pySPC. Dans ce cas, ceux-ci sont mentionnés dans les pages documentaires des scripts.

.. warning::
    Les scripts ne font pas l'objet de tests unitaires. Leur fonctionnement
    ne peut être totalement garanti à chaque nouvelle version de pyspc.
    Il convient donc d'être prudent lors de toute première utilisation d'un
    script.


""")
#        if not sys.path[0].endswith('libpySPC'):
#            return None
        # =====================================================================
        # TRAITEMENT DES SCRIPTS
        # =====================================================================
        dirname = os.path.realpath(
            os.path.join(sys.path[0], '..', SCRIPT_DIRNAME2))
        print('='*80)
        print(dirname)
        # -----------------------------------------------------------------
        # Groupes de scripts
        # -----------------------------------------------------------------
        scripts = glob.glob(os.path.join(dirname, '_*.py'))
        groups = collections.OrderedDict()
        for script in scripts:
            gname = os.path.basename(script).split('_')
            groups.setdefault(gname[1], [])
            groups[gname[1]].append(script)
        # -----------------------------------------------------------------
        # Introduction du groupe
        # -----------------------------------------------------------------
        for group, scripts in groups.items():
            print(group)
            f.write(HEADER.get(group, HEADER[None]))
            f.write('\n')
            f.write('.. toctree::\n')
            f.write('   :maxdepth: 2\n')
            f.write('   :titlesonly:\n')
            f.write('   \n')
            for script in scripts:
                f.write('   {d}/{b}\n'.format(
                    d=SCRIPT_DIRNAME2,
                    b=os.path.basename(os.path.splitext(script)[0])))
        # -----------------------------------------------------------------
        # Création du fichier script / <basename>.rst
        # -----------------------------------------------------------------
                content = []
                with open(script, 'r', encoding='utf-8') as fd:
                    for line in fd.readlines():
                        content.append(line)
                module = ast.parse(''.join(content))
                rst_filename = os.path.join(
                    SCRIPT_DIRNAME,
                    os.path.basename(os.path.splitext(script)[0]) + '.rst'
                )
                print(rst_filename)
                with open(rst_filename, 'w', encoding='utf-8') as fd:
                    fd.write("""
.. fichier rst créé automatiquement par scriptdoc

""")
                    fd.write(ast.get_docstring(module))
#                break
            f.write('\n')
#            break


def setup(app):
    # Nettoyage du dossier SCRIPT_DIRNAME
    for filename in glob.glob(os.path.join(SCRIPT_DIRNAME, '*.rst')):
        print(filename)
        try:
            os.remove(filename)
        except OSError:
            pass
    # Ecriture du main api
    filename = os.path.join(app.confdir, 'userguide_script.rst')
#    filename = os.path.join('tmp', 'userguide_scriptindex.rst')
    write_script_main(filename)
    return None


#if __name__ == "__main__":
#    for p in [r'D:\Utilisateurs\renaud.marty\Documents\3-bitbucket\pySPC\pyspc']:
#        if os.path.exists(p):
#            sys.path.insert(0, os.path.abspath(p))
#    setup(None)
