
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_313_verif_plot:

.. role:: blue

.. role:: boldblue

Tracer les critères de vérification des prévisions (_postEvent_313_verif_plot.py)
---------------------------------------------------------------------------------

Description
+++++++++++

Calculer les critères de vérification des prévisions

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`MODEL` : :blue:`Nom du modèle (sous-répertoire de fcst/)`

:boldblue:`SCENS` : :blue:`Nom des scénarios (STATION_SCEN-FREQ_*.csv), sous forme de liste`

.. rubric:: Configuration des statistiques

:boldblue:`SCORES_ABS` : :blue:`Calcul en valeur absolue ? (T/F) sous forme de liste`

:boldblue:`SCORES_REL` : :blue:`Calcul en valeur relative ? (T/F) sous forme de liste`

:boldblue:`SCORES_DELTA` : :blue:`Pas, en fréquence, des statistiques d'erreur`

:boldblue:`SCORES_FREQS` : :blue:`Fréquences des statistiques d'erreur`