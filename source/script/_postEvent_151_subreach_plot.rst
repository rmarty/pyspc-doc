
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_151_subreach_plot:

.. role:: blue

.. role:: boldblue

Synthèse graphiques par sous-tronçon (_postEvent_151_subreach_plot.py)
----------------------------------------------------------------------

Description
+++++++++++

Synthèse graphiques par sous-tronçon

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`COLORS` : :blue:`Liste des couleurs`

:boldblue:`LINESTYLES` : :blue:`Liste des types de ligne`