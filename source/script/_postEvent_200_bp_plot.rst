
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_200_bp_plot:

.. role:: blue

.. role:: boldblue

Tracer les BP et les observations pluviométriques (_postEvent_200_bp_plot.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les BP et les observations pluviométriques

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`FIRST_DTIME` : :blue:`Référentiel du SPC Loire-Cher-Indre`

:boldblue:`LAST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`TARGET_DAYS` : :blue:`Liste des jours à tracer (version 1 fig/jour)`

Fichiers créés
++++++++++++++

.. figure:: ../_static/41003.png
   :alt: Figure
   :figclass: figurecentre

   Figure 41003.png

.. figure:: ../_static/20191020.png
   :alt: Figure
   :figclass: figurecentre

   Figure 20191020.png