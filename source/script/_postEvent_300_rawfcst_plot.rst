
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_300_rawfcst_plot:

.. role:: blue

.. role:: boldblue

Tracer les prévisions opérationnelles par lieu/runtime (_postEvent_300_rawfcst_plot.py)
---------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les prévisions opérationnelles par lieu/runtime

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`TREND` : :blue:`Répertoire de MOHYS`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`