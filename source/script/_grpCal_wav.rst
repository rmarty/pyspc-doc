
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grpCal_wav:

.. role:: blue

.. role:: boldblue

Calcul des lames d'eau des bassins GRP (_grpCal_wav.py)
-------------------------------------------------------

Description
+++++++++++

Calcul de lames d'eau de bassin à partir de fichiers de pluie et du fichier bassin de GRP

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES


:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_P_DIRNAME` : :blue:`Répertoire des données de pluie`

:boldblue:`DATA_AP_DIRNAME` : :blue:`Répertoire de sortie`

:boldblue:`DATA_AP_SUFFIX` : :blue:`Suffixe des séries de pluie`