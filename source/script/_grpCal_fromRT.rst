
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grpCal_fromRT:

.. role:: blue

.. role:: boldblue

Extraction de données de GRP TEMPS REEL et conversion au format GRP Calage (_grpCal_fromRT.py)
----------------------------------------------------------------------------------------------

Description
+++++++++++

Paramètres
++++++++++

.. rubric:: Configuration des donnees

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`STATIONS_FILENAME` : :blue:`Fichier contenant la liste des stations`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`RT_DIRNAME` : :blue:`Répertoire des fichiers Temps Réel`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`