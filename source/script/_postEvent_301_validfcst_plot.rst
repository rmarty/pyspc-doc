
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_301_validfcst_plot:

.. role:: blue

.. role:: boldblue

Tracer les prévisions opérationnelles validées par lieu/runtime (_postEvent_301_validfcst_plot.py)
--------------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les prévisions opérationnelles validées par lieu/runtime

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`RAW_TREND` : :blue:`Nom de la prévision brute à conserver`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`COLORS` : :blue:`Couleurs par prévision/tendance`

:boldblue:`LABELS` : :blue:`Libellés par prévision/tendance`