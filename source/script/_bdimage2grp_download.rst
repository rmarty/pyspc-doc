
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_bdimage_download:

.. role:: blue

.. role:: boldblue

Télécharger les données BdImages (_bdimage_download.py)
-------------------------------------------------------

Description
+++++++++++

Télécharger les données (valeurs, statistique) d'images BdImages

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES IMAGES

:boldblue:`BDI_TYPE` : :blue:`Type d'image`

:boldblue:`BDI_STYPE` : :blue:`Sous-type d'image`

:boldblue:`BDI_BAND` : :blue:`Bande de l'image`

:boldblue:`VARNAME` : :blue:`Variable`

:boldblue:`ZONES` : :blue:`Zones`

:boldblue:`STATS` : :blue:`'standard', 'complete' ou <None>`

:boldblue:`PRECISION` : :blue:`'standard', 'forte' ou <None>`

.. rubric:: CONFIGURATION DES DONNEES LOCALES

:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de stockage`