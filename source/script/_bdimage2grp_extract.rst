
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_bdimage2csv_extract:

.. role:: blue

.. role:: boldblue

Convertir les données BdImages (_bdimage2csv_extract.py)
--------------------------------------------------------

Description
+++++++++++

Extraire les données BdImages et les convertir au format csv (GRP, PyspcFile)

.. warning: Ce script est à reprendre.

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES BDIMAGES

:boldblue:`BDI_TYPE` : :blue:`Type d'image (ex: antilope)`

:boldblue:`BDI_STYPE` : :blue:`Sous-type d'image (ex: temps-reel)`

:boldblue:`BDI_DURATION` : :blue:`Durée de cumul de l'image (ex: 000100)`

:boldblue:`BDI_FCT` : :blue:`Fonction appliquée`

:boldblue:`BDI_FCTPAR` : :blue:`Paramètre de la fonction appliquée`

.. rubric:: CONFIGURATION DES DONNEES LOCALES

:boldblue:`RADAR_DIRNAME` : :blue:`Répertoire de stockage`

:boldblue:`RAD_FCT` : :blue:`Fonction image`

:boldblue:`RAD_FCTPAR` : :blue:`Paramètre de la fonction image`

:boldblue:`ZONES_FILENAME` : :blue:`Fichier contenant la liste des zones`

.. rubric:: AUTRE CONFIGURATION

:boldblue:`OVERWRITE` : :blue:`Ecraser la donnée existante ?`