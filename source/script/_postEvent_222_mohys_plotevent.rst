
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_222_mohys_plotevent:

.. role:: blue

.. role:: boldblue

Tracer la simulation événementielle et les prévisions (_postEvent_222_mohys_plotevent.py)
---------------------------------------------------------------------------------------

Description
+++++++++++

Tracer la simulation événementielle et les prévisions MOHYS

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des prévisions au format csv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

.. rubric:: Configuration des figures

:boldblue:`INCLUDE_FORECAST` : :blue:`Inclure les prévisions ? (T/F)`

:boldblue:`MAXLTIME_FORECAST` : :blue:`Echéance maximal. Si None, reprend la valeur EVENT`

:boldblue:`COLORS_FORECAST` : :blue:`Liste de couleurs pour les prévisions`