
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_122_bdapbp_download:

.. role:: blue

.. role:: boldblue

Télécharger les BP depuis BDApBp (Lamedo) (_postEvent_122_bdapbp_download.py)
-----------------------------------------------------------------------------

Description
+++++++++++

Télécharger les BP depuis BDApBp (Lamedo)

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`