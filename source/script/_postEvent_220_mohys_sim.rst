
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_220_mohys_sim:

.. role:: blue

.. role:: boldblue

Lancement de MOHYS en SIMULATION et conversion en csv (_postEvent_220_mohys_sim.py)
-----------------------------------------------------------------------------------

Description
+++++++++++

Lancement de MOHYS en SIMULATION et conversion en csv

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de MOHYS

:boldblue:`MOH_DIRNAME` : :blue:`Répertoire de MOHYS`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`