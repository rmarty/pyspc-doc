
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_120_lamedo_download:

.. role:: blue

.. role:: boldblue

Téléchargement des données BdImages (_postEvent_120_lamedo_download.py)
-----------------------------------------------------------------------

Description
+++++++++++

Téléchargement des données BdImages

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

Fichier de configuration
++++++++++++++++++++++++

.. rubric:: Fichier de configuration des images

.. code-block:: cfg

   [obs]
   image = antilope;j1;rr
   stats = standard
   precision = forte