
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_121_lamedo_convert:

.. role:: blue

.. role:: boldblue

Conversion des données BdImages (_postEvent_121_lamedo_convert.py)
-------------------------------------------------------------------

Description
+++++++++++

Conversion des données BdImages

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`VARNAMES` : :blue:`Liste des grandeurs`