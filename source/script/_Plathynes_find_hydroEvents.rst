
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_Plathynes_find_hydroEvents:

.. role:: blue

.. role:: boldblue

Détermination des événements hydrométriques pour PLATHYNES (_Plathynes_find_hydroEvents.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Détermination des événements hydrométriques pour PLATHYNES

Paramètres
++++++++++


.. rubric:: CONFIGURATION DES DONNEES

:boldblue:`LOCS_HYDRO` : :blue:`Liste des sites hydro`

:boldblue:`THRESHOLDS` : :blue:`Dictionnaire des seuils définissant un événement = station:valeur`

:boldblue:`EVENT_PERIOD` : :blue:`Période de recherche`

:boldblue:`EVENT_DELTA` : :blue:`Durée minimale d'un événement`

.. rubric:: CONFIGURATION DE LA PHYC

:boldblue:`PHYC_CONFIG_FILENAME` : :blue:`Fichier de configuration pour l'accès à la PHyC`

:boldblue:`PHYC_DATATYPE` : :blue:`Type de données PHyC`

.. rubric:: CONFIGURATION LOCALE

:boldblue:`OUTPUT_HOME` : :blue:`Répertoire racine local`

:boldblue:`OUTPUT_QIXM` : :blue:`Sous-répertoire des données QIXM`

:boldblue:`OUTPUT_QIXJ` : :blue:`Sous-répertoire des données QIXJ`

:boldblue:`OUTPUT_QI` : :blue:`Sous-répertoire des données QI`

:boldblue:`EVENT_FILENAME` : :blue:`Fichiers contenant les informations des événements retenus`

:boldblue:`PLOT_CONFIG` : :blue:`Configuration des figures`

.. rubric:: CONFIGURATION DES ACTIONS

:boldblue:`REQUEST_QIXM` : :blue:`Réquêter les débits instantanés maximals mensuels`

:boldblue:`REQUEST_QIXJ` : :blue:`Réquêter les débits instantanés maximals journaliers`

:boldblue:`REQUEST_QI` : :blue:`Réquêter les débits instantanés`

:boldblue:`CONVERT_QI` : :blue:`Convertir les débits instantanés`