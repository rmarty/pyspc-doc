
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_000_locations:

.. role:: blue

.. role:: boldblue

Récupération des méta-données des lieux hydro (_postEvent_000_locations.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Récupération des méta-données des lieux hydro à partir du référentiel

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`REFSPC_FILENAME` : :blue:`Référentiel du SPC Loire-Cher-Indre`

Fichier de configuration
++++++++++++++++++++++++

.. rubric:: Fichier de configuration de l'événement

.. code-block:: cfg

   [event]
   name = 202006_Loire_amont
   title = Loire - Juin 2020
   firstdt = 2020061000
   lastdt = 2020061600
   maxltime = 120
   reaches = LC105;LC115;LC123;LC126;LC130;LC134;LC137;LC140;LC145;LC155
   subreaches = subreaches.txt
   meteo_depts = 03;07;21;42;43;58;63;69;71;89
   zap = 21016;21018;21019;21020;41003;41004;41005;41007;41008;41009;41014

Fichier créé
++++++++++++

.. rubric:: Fichier de configuration des lieux (locs_hydro.txt)

.. code-block:: cfg

   [K026001002]
   name = Chadrac [Pont du Monteil]
   code = K026001002
   ltime = 6

   [K0260010]
   name = Chadrac-sur-Loire
   code = K0260010
   code_hydro2 = K0260020;K0260030
   ltime = 6
   bnbv = LO808
   river = Loire
   area = 1328
   stats_t2 = 285
   stats_t5 = 585
   stats_t10 = 819
   stats_t20 = 1043
   stats_t50 = 1333
   stats_t100 = 1550
   stats_period = 1919 – 2018
   stats_size = 99