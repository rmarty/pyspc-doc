
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_111_phyc_convert:

.. role:: blue

.. role:: boldblue

Conversion des données PHyC (_postEvent_111_phyc_convert.py)
------------------------------------------------------------

Description
+++++++++++

Conversion des données PHyC

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`VARNAMES` : :blue:`Liste des grandeurs`