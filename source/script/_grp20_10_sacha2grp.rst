
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grp20_10_sacha2grp:

.. role:: blue

.. role:: boldblue

Création de la base de données GRP v2020 à partir des données SACHA (_grp20_10_sacha2grp.py)
--------------------------------------------------------------------------------------------

Description
+++++++++++

Les fichiers nécessaires pour GRP sont créés automatiquement à partir de fichiers de bassin GRP et de bases SACHA

Paramètres
++++++++++

.. rubric:: CONFIGURATION DE GRP

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`

.. rubric:: CONFIGURATION DE SACHA

:boldblue:`SACHA_DIRNAME` : :blue:`Répertoir des bases SACHA`

:boldblue:`SACHA_FILENAMES` : :blue:`Base Sacha par couple (variable, fournisseur)`

:boldblue:`HYDRO_VERSIONS` : :blue:`Codification Hydro2 ou Hydro3 par couple (variable, fournisseur)`

:boldblue:`SUFFIX` : :blue:`Suffixe du nom des postes pour reconnaitre le fournisseur`

.. rubric:: CONFIGURATION TEMPORELLE

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps`

.. rubric:: CONFIGURATION DES EXPORTS

:boldblue:`EXPORTS` : :blue:`Couples (variable, fournisseur) à exporter`