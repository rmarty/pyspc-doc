
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grpCal_extendETP:

.. role:: blue

.. role:: boldblue

Extension des données ETP des bassins GRP (_grpCal_extendETP.py)
----------------------------------------------------------------

Description
+++++++++++

Paramètres
++++++++++

.. rubric:: Configuration des donnees

:boldblue:`DATA_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`DATA_DIRNAME_BV` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME_E` : :blue:`Répertoires des données d'ETP`

:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de sortie`

.. rubric:: Configuration de la periode de calcul

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps à considérer`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps à considérer`

:boldblue:`SRC_YEAR` : :blue:`Année à dupliquer`