
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_210_grp_inputs:

.. role:: blue

.. role:: boldblue

Conversion des données obs en fichier d'entrée GRP (_postEvent_210_grp_inputs.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des données obs en fichier d'entrée GRP

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`GRP_DIRNAME` : :blue:`Répertoire Temps-Réel de GRP`