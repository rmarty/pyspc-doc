
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_100_hydro2_export:

.. role:: blue

.. role:: boldblue

Définir les procédures d'export de données HYDRO-2 (_postEvent_100_hydro2_export.py)
------------------------------------------------------------------------------------

Description
+++++++++++

Définir les procédures d'export de données HYDRO-2

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`HYDRO2_TYPE` : :blue:`Liste des types de données HYDRO-2 à exporter`