
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_130_mf_convert:

.. role:: blue

.. role:: boldblue

Conversion des données de la Publithèque Météo-France (_postEvent_130_mf_convert.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des données de la Publithèque Météo-France

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`VARNAMES` : :blue:`Liste des grandeurs`