
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_002_subreach_bygrp:

.. role:: blue

.. role:: boldblue

Définition des lieux météo par sous-tronçon via GRP (_postEvent_002_subreach_bygrp.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Définition des lieux météo par sous-tronçon via GRP

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

Fichier de configuration
++++++++++++++++++++++++

.. rubric:: Fichier de configuration des sous-tronçons

.. code-block:: cfg

    [Loire_vellave]
    name = Loire_vellave
    title = Loire vellave
    sites_hydro = K0030020;K0100020;K0260010;K0340001;K0550010
    stations_hydro = K003002010;K010002010;K026001002;K034000101;K055001010

Fichier créé
++++++++++++

.. rubric:: Fichier de configuration des sous-tronçons

.. code-block:: cfg

   [Loire_vellave]
   name = Loire_vellave
   title = Loire vellave
   sites_hydro = K0030020;K0100020;K0260010;K0340001;K0550010
   stations_hydro = K003002010;K010002010;K026001002;K034000101;K055001010
   sites_meteo = 07235005;07154005;07105003;43111002;43130002;43101002;43091005