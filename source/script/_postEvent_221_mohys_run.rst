
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_221_mohys_run:

.. role:: blue

.. role:: boldblue

Lancement de MOHYS en PREVISION et conversion en csv (_postEvent_221_mohys_run.py)
----------------------------------------------------------------------------------

Description
+++++++++++

Lancement de MOHYS en PREVISION et conversion en csv

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de MOHYS

:boldblue:`MOH_DIRNAME` : :blue:`Répertoire de MOHYS`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`PRV_DIRNAME` : :blue:`Répertoire des données au format prv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 pas de temps`

:boldblue:`LTIME_DT` : :blue:`Horizon maximal des prévisions`