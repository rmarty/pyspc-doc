
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grpCal_run16:

.. role:: blue

.. role:: boldblue

Calage automatique de GRP-16r (_grpCal_run16.py)
------------------------------------------------

.. warning: Script à reprendre

Description
+++++++++++

Script realisant le calage automatique de GRP dans sa version 2016

Paramètres
++++++++++


.. rubric:: Operations


:boldblue:`DEBUG` : :blue:`Mode débug`

:boldblue:`DRY_RUN` : :blue:`Exécution sans lancement d'exécutables GRP ni copie fichier`

:boldblue:`RUN_CAL` : :blue:`Calage-contrôle`

:boldblue:`RUN_RTIME` : :blue:`Calage complet`

:boldblue:`RUN_COPY` : :blue:`Copie sur le réseau`


.. rubric:: Arborescence grp


:boldblue:`HOME_DIRNAME` : :blue:`Répertoire principale`

:boldblue:`GRP_DIRNAME` : :blue:`Répertoire de GRP`

:boldblue:`dirnames` : :blue:`Dictionnaires des répertoires de calcul`

:boldblue:`BVINI_FILENAME` : :blue:`Fichier bassin des modèles à caler`

:boldblue:`BVALL_PARAMS` : :blue:`Dictionnaires des fichiers des paramètres supplémentaires à tester`

:boldblue:`PAR` : :blue:`fichier, avec PAR dans ['SC', 'SV', 'HC']`


.. rubric:: Archivage


:boldblue:`ARCHIVE_DIRNAME` : :blue:`Répertoire de stockage`

:boldblue:`ARCHIVE_DIRNAME_DATA` : :blue:`Sous-répertoire des données`

:boldblue:`ARCHIVE_DIRNAME_CFG` : :blue:`Sous-répertoire des config`

:boldblue:`ARCHIVE_DIRNAME_VERIF` : :blue:`Sous-répertoire des fiches`

:boldblue:`ARCHIVE_PREFIX` : :blue:`Préfixe du dossier de stockage`

:boldblue:`ARCHIVE_PATTERN_VERIF` : :blue:`Chaine de caractère permettant de reconnaitre les fiches de performance`

:boldblue:`ARCHIVE_PATTERN_EVENT` : :blue:`Chaine de caractère permettant de reconnaitre les fiches d'événements`

:boldblue:`ARCHIVE_TIME` : :blue:`Date du calage`

:boldblue:`TABLE_MODSTA_FILENAME` : :blue:`Fichier de correspondance Modèle Stations`

:boldblue:`TABLE_STAMOD_FILENAME` : :blue:`Fichier de correspondance Station Modèles`


.. rubric:: Executables grp


:boldblue:`EXE_OBS` : :blue:`Exécutable Chroniques`

:boldblue:`EXE_CAL` : :blue:`Exécutable calage-contrôle`

:boldblue:`EXE_VERIF` : :blue:`Exécutable fiche de performance`

:boldblue:`EXE_EVENT` : :blue:`Exécutable fiche d'événements`

:boldblue:`EXE_RTIME` : :blue:`Exécutable calage-complet`

:boldblue:`STDIN_OBS` : :blue:`Entrée standard pour l'exécutable Chroniques`

:boldblue:`STDIN_CAL` : :blue:`Entrée standard pour l'exécutable calage`

:boldblue:`STDIN_VERIF` : :blue:`Entrée standard pour l'exécutable fiche de performance`

:boldblue:`STDIN_EVENT` : :blue:`Entrée standard pour l'exécutable fiche d'événements`

:boldblue:`STDIN_RTIME` : :blue:`Entrée standard pour l'exécutable calage complet`

:boldblue:`BVINI_FILENAME` : :blue:`Fichier bassin des modèles à caler`

:boldblue:`BV_FILENAME` : :blue:`Fichier bassin de GRP`

:boldblue:`BV_HEADER_LEN` : :blue:`Longueur de l'entête du fichier de bassin`

:boldblue:`CFG_FILENAME` : :blue:`Fichier de configuration de GRP`