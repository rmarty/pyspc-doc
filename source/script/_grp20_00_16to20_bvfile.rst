
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grp20_00_16to20_bvfile:

.. role:: blue

.. role:: boldblue

Conversion des fichiers bassins de GRPv2016 à GRPv2020 (_grp20_00_16to20_bvfile.py)
-----------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers de bassin de GRP, de la version 2016 à la version 2020 et copie des fichiers hypsométriques

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16

:boldblue:`BDD_16` : :blue:`Répertoire de la base de données`

:boldblue:`BV_16` : :blue:`Répertoire des fichiers bassin et hypso`

:boldblue:`CODES` : :blue:`Liste des bassins à exporter`


.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de la base de données`

:boldblue:`BV_20` : :blue:`Répertoire des fichiers bassin et hypso`