
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_314_verif_plotuncertainty:

.. role:: blue

.. role:: boldblue

Tracer les critères de vérification des incertitudes de prévision (_postEvent_314_verif_plotuncertainty.py)
-----------------------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les critères de vérification des incertitudes de prévision

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`MODEL` : :blue:`Nom du modèle (sous-répertoire de fcst/)`

:boldblue:`SCENS` : :blue:`Nom des scénarios (STATION_SCEN-FREQ_*.csv), sous forme de liste`