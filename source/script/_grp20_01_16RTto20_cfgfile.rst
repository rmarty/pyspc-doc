
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grp20_01_16RTto20_cfgfile:

.. role:: blue

.. role:: boldblue

Conversion des fichiers de runs de GRPv2016 Temps-Réel à GRPv2020 (_grp20_01_16RTto20_cfgfile.py)
-------------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers de runs de GRP Temps-Réel, de la version 2016 à la version 2020

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16 Temps-Réel

:boldblue:`BDD_16` : :blue:`Répertoire de configuration de GRP Temps-Réel`

:boldblue:`CFG_16` : :blue:`Fichier de configuration Temps-Réel`

:boldblue:`CODES` : :blue:`Liste des bassins à exporter`


.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de configuration de GRP (Paramétrage)`

:boldblue:`CFG_20` : :blue:`Fichier de configuration`