
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_140_regularscale:

.. role:: blue

.. role:: boldblue

Mise au pas de temps horaire des données instantanées (_postEvent_140_regularscale.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Mise au pas de temps horaire des données instantanées. L'archivage suit une arborescence obs/PROVIDER/VARNAME

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`VARNAMES` : :blue:`Liste des grandeurs`

:boldblue:`PROVIDERS` : :blue:`Liste des producteurs`