
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_mfReports:

.. role:: blue

.. role:: boldblue

Télécharger les rapports/bulletins/fiches MF (_mfReports.py)
------------------------------------------------------------

Description
+++++++++++

Télécharger les rapports/bulletins/fiches MF

Paramètres
++++++++++


.. rubric:: Elements communs


:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de stockage local`

:boldblue:`DATA_TYPE` : :blue:`Type de rapport MF`


.. rubric:: Bulletin climatologique mensuel


:boldblue:`YEARS` : :blue:`Liste des années`

:boldblue:`MONTHS` : :blue:`Liste des mois`

:boldblue:`DAYS` : :blue:`Liste des jours`

:boldblue:`DEPTS` : :blue:`Liste des départements (avant 2012)`

:boldblue:`REGIONS` : :blue:`Liste des régions (après 2012)`


.. rubric:: Bulletin climatologique journalier


:boldblue:`FIRST_DT` : :blue:`Date du premier bulletin`

:boldblue:`LAST_DT` : :blue:`Date du dernier bulletin`

:boldblue:`DELTA_DT` : :blue:`Intervalle de temps entre deux bulletins`


.. rubric:: Fiches stations


:boldblue:`STATIONS` : :blue:`Liste des stations à considérer`