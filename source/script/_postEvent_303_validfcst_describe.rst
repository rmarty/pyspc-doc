
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_303_validfcst_describe:

.. role:: blue

.. role:: boldblue

Synthèse des modèles et outils de validation (_postEvent_303_validfcst_describe.py)
-----------------------------------------------------------------------------------

Description
+++++++++++

Synthèse des modèles et outils de validation

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`MODEL_CMAPS` : :blue:`Couleurs des modèles`

:boldblue:`TEND_COLORS` : :blue:`Couleurs des tendances`

:boldblue:`SAMPLESIZE_FONTSIZE` : :blue:`Taille de police`