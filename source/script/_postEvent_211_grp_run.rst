
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_211_grp_run:

.. role:: blue

.. role:: boldblue

Lancement de GRP en Temps Diff et conversion en csv (_postEvent_211_grp_run.py)
-------------------------------------------------------------------------------

Description
+++++++++++

Lancement de GRP en Temps Diff et conversion en csv

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de GRP

:boldblue:`GRP_SCEN_NORAIN` : :blue:`Identifiant du scénario Pluie Nulle`

:boldblue:`GRP_MODEL_POM` : :blue:`Code modèle POM`

:boldblue:`GRP_DTYPES` : :blue:`Type de scénario (prv et sim)`

:boldblue:`GRP_SCENS` : :blue:`Codes scénario (prv et sim)`

.. rubric:: Configuration de l'archivage

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`PRV_DIRNAME` : :blue:`Répertoire des données au format prv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 pas de temps`

:boldblue:`LTIME_DT` : :blue:`Horizon maximal des prévisions`