
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_110_phyc_download:

.. role:: blue

.. role:: boldblue

Téléchargement des données depuis la PHyC (_postEvent_110_phyc_download.py)
------------------------------------------------------------------------------------

Description
+++++++++++

Téléchargement des données depuis la PHyC

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`