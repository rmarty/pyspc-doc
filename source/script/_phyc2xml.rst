
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_phyc2xml:

.. role:: blue

.. role:: boldblue

Télécharger les données PHyC (_phyc2xml.py)
-------------------------------------------------------

Description
+++++++++++

Télécharger les données (valeurs, statistique) d'images BdImages

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES

:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de stockage`

:boldblue:`LOCS` : :blue:`Liste des stations`

:boldblue:`VARNAME` : :blue:`Grandeur`

:boldblue:`DATATYPE` : :blue:`Type de donnée PHyC`

.. rubric:: CONFIGURATION DE LA PERIODE

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps à considérer`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps à considérer`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 requêtes`