
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_150_subreach_stats:

.. role:: blue

.. role:: boldblue

Synthèse des pointes de crue par sous-tronçon (_postEvent_150_subreach_stats.py)
--------------------------------------------------------------------------------

Description
+++++++++++

Synthèse des pointes de crue par sous-tronçon (valeur, date et temps de retour).

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`