
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_112_phyc_cristaledf2mfdata:

.. role:: blue

.. role:: boldblue

Conversion au format MF des pluvios hors-MF de la PHyC (_postEvent_112_phyc_cristaledf2mfdata.py)
-------------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion au format MF des pluvios hors-MF de la PHyC pour les formatter en vue d'être intégrée dans SACHA

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`