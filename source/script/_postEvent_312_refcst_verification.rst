
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_312_refcst_verification:

.. role:: blue

.. role:: boldblue

Calculer les critères de vérification des prévisions rejouées (_postEvent_312_refcst_verification.py)
-----------------------------------------------------------------------------------------------------

Description
+++++++++++

Calculer les critères de vérification des prévisions rejouées

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`REFORCAST_MODEL` : :blue:`Nom du modèle (sous-répertoire de fcst/)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

.. rubric:: Configuration des statistiques

:boldblue:`SCORES_ABS` : :blue:`Calcul en valeur absolue ? (T/F) sous forme de liste`

:boldblue:`SCORES_REL` : :blue:`Calcul en valeur relative ? (T/F) sous forme de liste`

:boldblue:`SCORES_DELTA` : :blue:`Pas, en fréquence, des statistiques d'erreur`

:boldblue:`SCORES_FREQS` : :blue:`Fréquences des statistiques d'erreur`