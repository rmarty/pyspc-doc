Documentation du module pySPC
=============================

Introduction
------------

Ce document a pour objet d'expliquer l'installation, l'usage du module **pySPC** et de détailler le contenu de la bibliothèque **pyspc**.

:Version: |version|
:Date: |today|


Licence
-------
Le module *pySPC* est placé sous la licence GPL (GNU Public License). Vous trouverez la description de cette licence dans le fichier `COPYING.txt <_static/COPYING.txt>`_. L'interface graphique intègre des icônes dont les licences sont décrites dans `COPYING_icons.txt <_static/COPYING_icons.txt>`_. 

Contacts
--------

.. index:: Contacts
.. index:: Dépôt Bitbucket

`Renaud Marty <renaud.marty@developpement-durable.gouv.fr>
<mailto:renaud.marty@developpement-durable.gouv.fr>`_

|bitbucket| |version_b| |python| |issues| |readthedocs| |coverage| |codefactor| |License|

.. |bitbucket| image:: https://img.shields.io/badge/source-pyspc-blueviolet
    :target: https://bitbucket.org/rmarty/pyspc
.. |version_b| image:: https://img.shields.io/badge/version-3.0.0-blue
   :target: https://bitbucket.org/rmarty/pyspc/get/v3.0.0.zip
.. |python| image:: https://img.shields.io/badge/python-3.7-blue
   :target: https://www.python.org/
.. |issues| image:: https://img.shields.io/bitbucket/issues/rmarty/pyspc
   :target: https://bitbucket.org/rmarty/pyspc/issues?status=new&status=open
.. |readthedocs| image:: https://readthedocs.org/projects/pyspc-doc/badge/?version=latest
   :target: https://pyspc-doc.readthedocs.io/fr/latest
.. |coverage| image:: https://img.shields.io/badge/coverage-82%25-green
   :target: https://coverage.readthedocs.io
.. |codefactor| image:: https://www.codefactor.io/repository/bitbucket/rmarty/pyspc/badge
   :target: https://www.codefactor.io/repository/bitbucket/rmarty/pyspc
.. |License| image:: https://img.shields.io/badge/license-GPLv3-blue
   :target: https://www.gnu.org/licenses/quick-guide-gplv3.fr.html

Sommaire
--------
  
.. toctree::
   :maxdepth: 2
   :caption: Module pyspc

   desc
   desc_proc
   desc_date
   desc_var
   desc_file
   desc_usage
  
.. toctree::
   :maxdepth: 2
   :caption: Installation

   install
   install_python
   install_pyspc
   install_api
   install_lib
   

.. toctree::
   :maxdepth: 2
   :caption: Guide utilisateur

   userguide
   userguide_bin
   userguide_gui
   userguide_script


.. _api:  

.. toctree::
   :maxdepth: 2
   :caption: API reference

   api_convention
   api_core
   api_data
   api_io
   api_metadata
   api_model
   api_plotting
   api_statistics
   api_verification
   api_webservice


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
