
.. currentmodule:: pyspc.core

Objets natifs et convention de pyspc
====================================

Configuration
-------------

.. autosummary::
    :toctree: api/

    config.Config

Série de données
----------------

.. autosummary::
    :toctree: api/

    serie.Serie
    series.Series
    keyseries

Grandeur
--------

.. autosummary::
    :toctree: api/

    parameter.Parameter

Fournisseur
-----------

.. autosummary::
    :toctree: api/

    provider

Lieu
----

.. autosummary::
    :toctree: api/

    location.Location
    location.Locations

Tronçon
-------

.. autosummary::
    :toctree: api/

    reach.Reach
    reach.Reaches

Courbe de tarage
----------------

.. autosummary::
    :toctree: api/

    ratingcurve.RatingCurve
    ratingcurve.RatingCurves

Réservoir
---------

.. autosummary::
    :toctree: api/

    reservoir.Reservoir
    reservoir.Table

Fichier csv natif
-----------------

.. autosummary::
    :toctree: api/

    pyspcfile.PyspcFile

Manipulation de dates
---------------------

.. autosummary::
    :toctree: api/

    timeutil

Convention
----------

.. autosummary::
    :toctree: api/

    convention

Exceptions
----------

.. autosummary::
    :toctree: api/

    exception
