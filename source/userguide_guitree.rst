.. fichier rst créé automatiquement par bindoc


.. role:: blue

.. role:: boldblue



- .. image:: _static/data.png
     :height: 20px
     :alt: alternate text
     :align: left

  :boldblue:`Données` (:blue:`Observations et prévisions: Cristal, Hydro-2, Météo-France, Vigicrues`)

   + .. image:: _static/database.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Bases de données` (:blue:`Bases locales: SACHA, BAREME, Prévision, RefSPC`)

      * :boldblue:`csv2dbase.py` (:blue:`Insérer des prévisions au format pyspc dans une base Prevision19`)
      * :boldblue:`dbase2csv.py` (:blue:`Extraire les données de base de données MDB/SQLite et conversion au format de type csv (grp16, grp18, grp20, pyspc)`)
      * :boldblue:`dbase2plathynes.py` (:blue:`Extraire les données de base de données (SACHA) pour PLATHYNES`)
      * :boldblue:`dbaseCreate.py` (:blue:`Création de bases de données vierges`)
      * :boldblue:`dbaseInfo.py` (:blue:`Informations sur les lieux et tronçons de vigilance`)

   + .. image:: _static/cristal.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`CRISTAL` (:blue:`Archives Cristal`)

      * :boldblue:`cristal2csv.py` (:blue:`Convertir les données CRISTAL au format de type csv (grp16, grp18, grp20, pyspc)`)
      * :boldblue:`cristal2xmlSandre.py` (:blue:`Convertir les données CRISTAL au format XML Sandre`)

   + .. image:: _static/webbrowser.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Documents internet` (:blue:`Documents en ligne: Hydroclim, Shyreg, Météo-France`)

      * :boldblue:`onlineReport.py` (:blue:`Télécharger les rapports et bulletins de MF, Inrae et Vigicrues`)

   + .. image:: _static/hydro2.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Hydro-2` (:blue:`Données, exports et statistiques Hydro-2`)

      * :boldblue:`hydro2csv.py` (:blue:`Convertir les données Hydro2 au format de type csv (GRP16, GRP18, pyspc)`)
      * :boldblue:`hydroExport.py` (:blue:`Créer les procédures d'export Hydro2`)
      * :boldblue:`hydroStat.py` (:blue:`Lire les statistiques exportées depuis HYDRO-2`)

   + .. image:: _static/meteo_france.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`MeteoFrance` (:blue:`Données issues de Météo-France`)

      * :boldblue:`csv2mf.py` (:blue:`Convertir les données de type csv (GRP16, GRP18, pyspc) au format data de Météo France`)
      * :boldblue:`mf2csv.py` (:blue:`Convertir les données MétéoFrance au format de type csv (grp16, grp18, grp20, pyspc)`)
      * :boldblue:`mf2mf.py` (:blue:`Traiter les données MeteoFrance au format .data`)
      * :boldblue:`mfOpenWS.py` (:blue:`Télécharger les données de Météo-France par webservice`)

   + .. image:: _static/schapi.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Vigicrues` (:blue:`Bases Vigicrues: BdApbp, BdImage, PHyC, Vigicrues`)


      * .. image:: _static/schapi_vector.png
           :height: 20px
           :alt: alternate text
           :align: left

        :boldblue:`BdApbp` (:blue:`Bulletin APBP`)

         - :boldblue:`bdapbp2json.py` (:blue:`Extraire les données de BdApbp (LAMEDO) au format json`)
         - :boldblue:`bdapbpjson2csv.py` (:blue:`Convertir les données BdAPBP du format JSON au format de type csv (pyspc)`)

      * .. image:: _static/schapi_raster.png
           :height: 20px
           :alt: alternate text
           :align: left

        :boldblue:`BdImage` (:blue:`Images Radar, Prévisions en point de grille`)

         - :boldblue:`bdimage2xml.py` (:blue:`Extraire les données de BdImage`)
         - :boldblue:`bdimagexml2csv.py` (:blue:`Convertir les données BdImage du XML au format de type csv (pyspc)`)

      * .. image:: _static/schapi_database2.png
           :height: 20px
           :alt: alternate text
           :align: left

        :boldblue:`PHyC` (:blue:`Observations, Prévisions et Informations PHyC`)

         - :boldblue:`hydroportailStats.py` (:blue:`Télécharger les statistiques Hydroportail au format csv.`)
         - :boldblue:`phyc2plathynes.py` (:blue:`Télécharger / convertir les données PHYC (xml) pour PLATHYNES`)
         - :boldblue:`phyc2xml.py` (:blue:`Télécharger les données (obs, fcst) et informations (lieux, courbe de tarage, courbe de correction, jaugeage) depuis la PHyC au format XML-Sandre`)
         - :boldblue:`xmlSandre2csv.py` (:blue:`Convertir les données XML-Sandre au format de type csv (grp16, grp18, grp20, pyspc)`)
         - :boldblue:`xmlSandreInfo.py` (:blue:`Informations sur les lieux et tronçons de vigilance`)


- .. image:: _static/evaluation.png
     :height: 20px
     :alt: alternate text
     :align: left

  :boldblue:`Evaluation` (:blue:`Évaluation de prévision (OTAMIN, SCORES)`)

   + .. image:: _static/irstea_otamin.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`OTAMIN` (:blue:`Préparation des incertitudes de modèle (OTAMIN)`)

      * :boldblue:`csv2prv.py` (:blue:`Convertir les données de type csv (GRP16, GRP18, pyspc) au format prv Scores`)
      * :boldblue:`grpRT2prv.py` (:blue:`Convertir les prévisions de GRP *Temps Réel* au format prv Scores / prv OTAMIN`)
      * :boldblue:`prv2csv.py` (:blue:`Convertir les données prv Scores/OTAMIN au format de type csv (grp16, grp18, grp20, pyspc)`)

   + .. image:: _static/schapi_scores.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`SCORES` (:blue:`Évaluation de séries hydrologiques par SCORES`)

      * :boldblue:`csv2prv.py` (:blue:`Convertir les données de type csv (GRP16, GRP18, pyspc) au format prv Scores`)
      * :boldblue:`duplicateScoresCfg.py` (:blue:`Dupliquer la configuration SCORES et modifier son contenu`)
      * :boldblue:`grpRT2prv.py` (:blue:`Convertir les prévisions de GRP *Temps Réel* au format prv Scores / prv OTAMIN`)
      * :boldblue:`prv2csv.py` (:blue:`Convertir les données prv Scores/OTAMIN au format de type csv (grp16, grp18, grp20, pyspc)`)
      * :boldblue:`xmlScores2png.py` (:blue:`Convertir les résultats Scores du xml au csv/png`)


- .. image:: _static/modhydro.png
     :height: 20px
     :alt: alternate text
     :align: left

  :boldblue:`Modélisation Hydrologique` (:blue:`Modélisation (GRP, Plathynes)`)

   + .. image:: _static/irstea_verification.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`GRP` (:blue:`Vérification des modèles GRP calés`)

      * :boldblue:`grpVerif.py` (:blue:`Récupérer les performances des modèles GRP (Fiches de Performance)`)
      * :boldblue:`plotGrpVerif.py` (:blue:`Tracer sous forme graphique les performances GRP fournies par grpVerif.py`)

   + .. image:: _static/schapi_plathynes.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Plathynes` (:blue:`Import/Export de PLATHYNES`)

      * :boldblue:`dbase2plathynes.py` (:blue:`Extraire les données de base de données (SACHA) pour PLATHYNES`)
      * :boldblue:`duplicatePlathynesEvent.py` (:blue:`Dupliquer un événement au sein d'un même projet PLATHYNES`)
      * :boldblue:`phyc2plathynes.py` (:blue:`Télécharger / convertir les données PHYC (xml) pour PLATHYNES`)
      * :boldblue:`plathynes2csv.py` (:blue:`Convertir les données PLATHYNES au format de type csv (grp16, grp18, grp20, pyspc)`)


- .. image:: _static/irstea_rtime.png
     :height: 20px
     :alt: alternate text
     :align: left

  :boldblue:`Prévision Hydrologique` (:blue:`Prévision hydrologique (GRP)`)

   + .. image:: _static/irstea_rtime_data.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`GRP` (:blue:`Configuration et données de GRP Temps-Réel`)

      * :boldblue:`csv2grpRT.py` (:blue:`Convertir les données obs/prev du format csv (GRP16, GRP18, pyspc) à GRP Temps-Réel`)
      * :boldblue:`duplicateGrpRTCfg.py` (:blue:`Dupliquer la configuration GRP Temps Reel et modifier son contenu`)
      * :boldblue:`grpRT2csv.py` (:blue:`Convertir les prévisions de GRP *Temps Réel* (grp16, grp18) au format de type csv (grp16, grp18, grp20, pyspc)`)


- .. image:: _static/data_processing.png
     :height: 20px
     :alt: alternate text
     :align: left

  :boldblue:`Traitement de données` (:blue:`Traitement de données`)

   + .. image:: _static/figure.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Image` (:blue:`Affichage graphique des données`)

      * :boldblue:`plotCsvData.py` (:blue:`Tracer les données observées et/ou prévues sous forme graphique`)

   + .. image:: _static/data_processing2.png
        :height: 20px
        :alt: alternate text
        :align: left

     :boldblue:`Traitements` (:blue:`Traitements: conversion, information`)

      * :boldblue:`comparePeakFlow.py` (:blue:`Comparer les pointes de crues entre deux sites`)
      * :boldblue:`csv2csv.py` (:blue:`Traiter les données format de type csv (grp16, grp18, grp20, pyspc), selon une méthode choisie`)
      * :boldblue:`csvInfo.py` (:blue:`Extraire des informations de séries au format de type csv(grp16, grp18, grp20, pyspc), selon une méthode choisie`)
