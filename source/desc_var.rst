
.. _descvar:

Grandeurs
---------

Le module pySPC est capable de manipuler des données de plusieurs grandeurs, listées dans le tableau ci-dessous.

.. _table_spcvarnames:

.. csv-table:: Grandeurs disponibles
   :header: "Variable", "Description", "unité de temps", "unité", "Format de date"
   
   "P5m", "Précipitation 5-minutes", "5-min", "mm", "AAAAMMJJHHmm"
   "P6m", "Précipitation 6-minutes", "6-min", "mm", "AAAAMMJJHHmm"
   "P15m", "Précipitation 15-minutes", "15-min", "mm", "AAAAMMJJHHmm"
   "PH", "Précipitation horaire", "heure", "mm", "AAAAMMJJHH"
   "P3H", "Précipitation tri-horaire", "heure", "mm", "AAAAMMJJHH"
   "PJ", "Précipitation journalière", "jour", "mm", "AAAAMMJJ"
   "PM", "Précipitation mensuelle", "mois", "mm", "AAAAMM"
   "TI", "Température instantanée", , "degrés Celsius", "AAAAMMJJHHmm"
   "TH", "Température horaire", "heure", "degrés Celsius", "AAAAMMJJHH"
   "TJ", "Température journalière", "jour", "degrés Celsius", "AAAAMMJJ"
   "EH", "Evapo-transpiration horaire", "heure", "mm", "AAAAMMJJHH"
   "EJ", "Evapo-transpiration journalière", "jour", "mm", "AAAAMMJJ"
   "QI", "Débit instantané", , "m3/s", "AAAAMMJJHHmm"
   "QH", "Débit horaire", "heure", "m3/s", "AAAAMMJJHH"
   "QJ", "Débit moyen journalier", "jour", "m3/s", "AAAAMMJJ"
   "QM", "Débit moyen mensuel", "mois", "m3/s", "AAAAMM"
   "HI", "Hauteur instantanée", , "m", "AAAAMMJJHHmm"
   "HH", "Hauteur horaire", "heure", "m", "AAAAMMJJHH"
   "ZI", "Cote instantanée", , "mNGF", "AAAAMMJJHHmm"
   "ZH", "Cote horaire", "heure", "mNGF", "AAAAMMJJHH"
   "VI", "Volume retenue instantanée", , "m3", "AAAAMMJJHHmm"
   "VH", "Volume retenue horaire", "heure", "m3", "AAAAMMJJHH"
   "HU2J", "Humidité du sol moyenne journalière", "jour", "%", "AAAAMMJJ"

.. note:: Les variables "PM" et "QM" sont introduites dans la version 2.2.3

.. note:: La variable "P15m est introduite dans la version 2.2.5
