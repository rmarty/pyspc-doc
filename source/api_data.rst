
.. currentmodule:: pyspc.data

Données externes
================


Bareme
------

.. autosummary::
    :toctree: api/

    bareme.Bareme


Cristal
-------

.. autosummary::
    :toctree: api/

    cristal.Cristal


Hydro2
------

.. autosummary::
    :toctree: api/

    hydro2.Hydro2


Lamedo
------

.. autosummary::
    :toctree: api/

    lamedo.bdapbp.BdApbp
    lamedo.bdimage.BdImage

.. _table_bdimages:

.. csv-table:: Images disponibles
   :header: "Image", "Sous-image", "bande"
   :align: center

   "antilope", "france-td-15mn", "rr"
   "antilope", "france-tr-15mn", "rr"
   "antilope", "france-td-60mn", "rr"
   "antilope", "france-tr-60mn", "rr"
   "antilope", "j1", "rr"
   "antilope", "temps-reel", "rr"
   "comephore", "france", "rr"
   "panthere", "france", "rr"
   "panthere", "france-iq", "rr"
   "sim", "evap", "rr"
   "sim", "neige", "rr"
   "sim", "rr", "ecoulement"
   "sim", "rr", "fonte"
   "sim", "rr", "liquide"
   "sim", "rr", "solide"
   "sim", "rr", "total"
   "sim", "t", "t"
   "sympo", "rr", "rr"
   "sympo", "t", "t"
   "arpege", "rr", "total"
   "arpege", "rr", "liquide"
   "arpege", "rr", "solde"
   "arpege", "iso0", "altitude"
   "arpege", "lpn", "altitude"
   "arpege", "t", "t"
   "arome", "rr", "total"
   "arome", "rr", "liquide"
   "arome", "rr", "neige"
   "arome", "rr", "graupel"
   "arome", "iso0", "altitude"
   "arome", "lpn", "altitude"
   "arome", "t", "t"
   "arome-ifs", "rr", "total"
   "arome-ifs", "rr", "liquide"
   "arome-ifs", "rr", "neige"
   "arome-ifs", "rr", "graupel"
   "arome-ifs", "iso0", "altitude"
   "arome-ifs", "lpn", "altitude"
   "arome-ifs", "t", "t"
   "arome-pi", "rr", "total"
   "piaf", "france-antilope-15mn", "rr"


.. note:: Les images Antilope 15-min sont disponibles à partir de la version 2.2.5

.. seealso:: La `documentation <http://services.schapi.e2.rie.gouv.fr/bdimage/documentation/2016/utilisateur/>`_ est accessible pour les agents du réseau Vigicrues


Météo-France
------------

.. autosummary::
    :toctree: api/

    meteofrance.MF_Data
    meteofrance.MF_OpenAPI
    meteofrance.MF_OpenData
    meteofrance.BP_Data
    meteofrance.BP_value_to_interval
    meteofrance.Sympo_Data


Prévision du SPC Loire-Allier-Cher-Indre
----------------------------------------

.. autosummary::
    :toctree: api/

    prevision.prevision14.Prevision14
    prevision.prevision17.Prevision17
    prevision.prevision19.Prevision19


Sacha
-----

.. autosummary::
    :toctree: api/

    sacha.data.Sacha


Sandre (XML)
------------

.. autosummary::
    :toctree: api/

    sandre.sandre.Sandre


Vigicrues
---------

.. autosummary::
    :toctree: api/

    vigicrues.data.Vigicrues_Data
    vigicrues.fcst.Vigicrues_Fcst
