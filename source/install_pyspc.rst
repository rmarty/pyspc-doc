
.. role:: blue

.. role:: boldblue

.. _pyspcinstall:

Module complet pySPC
--------------------

Dépôt du projet pySPC
+++++++++++++++++++++

Il suffit de récupérer l'archive depuis le dépôt `Bitbucket <https://bitbucket.org/rmarty/pyspc/downloads/>`_

.. _varenv:

Variables d'environnement
+++++++++++++++++++++++++

Il faut ensuite indiquer à Python où trouver *pySPC* en ajoutant le chemin où se trouve le paquet *pyspc* dans la variable d'environnement *PYTHONPATH* de l'utilisateur afin de permettre l'import de la librairie depuis n'importe quel fichier \*.py

-  Panneau de configuration > Système > Variables d'environnement
-  "Nouveau" et renseigner *Nom* par *PYTHONPATH* et *Valeur* par *C:\\chemin\\vers\\lerepertoirecontenantpyspc* (ex: D:\\3-pySPC). Si la variable d'environnement *PYTHONPATH* existe déjà, il faut rajouter une *Valeur* après celle(s) existante(s) en utilisant le caractère ';' comme séparateur.

.. note:: La définition de *PYTHONPATH* est inutile si vous avez installé la bibliothèque selon :ref:`classicinstall`.

Enfin, pour profiter pleinement des scripts de `tests <test.html>`__ fournis avec le module *pySPC*, il est nécessaire des définir les variables d'environnement suivantes, en suivant la procédure du point 2 ci-dessus:

:boldblue:`PYSPC_HOME`
    :blue:`Chemin absolu du répertoire contenant le module *pySPC* (ex: D:\\3-pySPC)`

:boldblue:`PYSPC_BIN`
    :blue:`Chemin absolu du répertoire contenant les *binaires* du module *pySPC* (ex: D:\\3-pySPC\\bin)`

:boldblue:`PYSPC_SCRIPT`
    :blue:`Chemin absolu du répertoire contenant les *scripts* du module *pySPC* (ex: D:\\3-pySPC\\script)`

Ainsi, si vous souhaitez lancer un *binaire* de pySPC, il suffit d'écrire

.. container:: cmdimg

   .. container:: cmdline
   
      python %PYSPC_BIN%\*binaire*.py [args]

.. note:: La procédure de définition de ces variables d'environment est nécessaire pour utiliser l'interface graphique et les binares en ligne de commande.
      
.. warning:: Selon les options de votre installation, Windows n'associe pas toujours l'extension py au python.exe afin d'éviter tout conflit si vous avez plusieurs environnements python différents.

