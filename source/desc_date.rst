
.. index:: Format des dates

.. _descdate:

Formats des dates
-----------------

Entête de fichier
+++++++++++++++++

Le paquet **pySPC** est destiné à traiter, convertir, afficher, analyser des séries de données. Lorsque les séries proviennent d'un fichier au format *csv*, le format des dates dépend du pas de temps des données :

- les dates des données **mensuelles** sont écrites sous la forme **AAAAMM** ;
- les dates des données **journalières** sont écrites sous la forme **AAAAMMJJ** pour le jour entre **JJ 06:00** et **JJ+1 06:00** ;
- les dates des données **horaires** sont écrites sous la forme **AAAAMMJJHH** pour le pas de temps entre **HH-1:00** et **HH:00** ;
- les dates des données **instantanées** sont écrites sous la forme **AAAAMMJJHHmm** ;

Date dans le nom d'un fichier
+++++++++++++++++++++++++++++

La convention de nommage introduit la possibilité de définir la date de production de la prévision dans le nom du fichier. Celle-ci doit être au format AAAAMMJJHH.

.. seealso:: :ref:`descfile`.

