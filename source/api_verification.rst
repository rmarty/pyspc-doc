
.. currentmodule:: pyspc.verification

Evaluation
==========


Scores
------

.. autosummary::
    :toctree: api/

    scores.config.Config
    scores.data.Data
    scores.scores.Results
