
.. _gui:

Utilisation par interface graphique
===================================

.. index:: Interface graphique

Principe et lancement
---------------------

L'interface graphique fournie avec le module *pySPC* est dédiée au lancement de ses utilitaires (**binaires seulement**). Pour la lancer, il suffit de lancer l'exécutable *gui.bat* situé dans le répertoire *bin* du module *pySPC*

.. container:: cmdimg

   .. container:: cmdline
   
      bin/gui.bat

Le fichier *gui.ini* également situé dans le répertoire *bin* du module *pySPC* permet de configurer l'accès à l'aide, en renseignant le chemin local contenant l'arborescence HTML (*local*), le lien url de la documentation sur readthedocs (*rtd*), ainsi que les autres pages accessibles depuis le menu Aide

.. code-block:: cfg

   [doc]
   local = chemin/vers/pySPC-doc/build/html
   rtd = https://pyspc-doc.readthedocs.io/fr/latest/
   root = index.html
   bin = userguide_bin.html
   api = api_core.html
   gui = userguide_gui.html
   description = desc.html

.. seealso:: Voir en fin de cette page pour les menus de l'Aide


.. figure:: _static/gui_start.png
   :alt: Interface graphique lors de son lancement
   :figclass: figurecentre

   Interface graphique lors de son lancement

Présentation de l'interface
---------------------------

Explorateur
+++++++++++

.. index:: Explorateur de l'interface graphique


L'interface graphique dispose d'un explorateur permettant de choisir l'utilitaire du module *pySPC* à exécuter selon le besoin de l'utilisateur. Celui-ci suit l'aborescence suivante :


.. include:: userguide_guitree.rst


.. figure:: _static/gui_explorer.png
   :alt: Explorateur de l'interface graphique
   :figclass: figurecentre
   :scale: 50%

   Explorateur de l'interface graphique

Menu
++++

.. index:: Menu de l'interface graphique

Les utilitaires sont disponibles selon la même arborescence à partir du menu situé en haut de l'interface. Le menu contient deux entrées supplémentaires : 

- .. image:: _static/tree2.png
         :height: 20 px
         :alt: alternate text
         :align: left

  *Affichage* > *Explorateur* : (dés)afficher l'explorateur

- *Aide* : aide générale du module *pySPC*. Elle repose sur le projet **pyspc-doc**.

    - .. image:: _static/index2.png
         :height: 20 px
         :alt: alternate text
         :align: left

      `Table des matières <index.html>`_

    - .. image:: _static/description.png
         :height: 20 px
         :alt: alternate text
         :align: left

      `Description du module *pySPC* <desc.html>`_

    - .. image:: _static/gui.png
         :height: 20 px
         :alt: alternate text
         :align: left

      `Documentation de l'interface graphique (Graphic User Interface) <userguide_gui.html>`_

    - .. image:: _static/cmd2.png
         :height: 20 px
         :alt: alternate text
         :align: left

      `Présentation des utilitaires <userguide_bin.html>`_

    - .. image:: _static/api.png
         :height: 20 px
         :alt: alternate text
         :align: left

      `Présentation de l'API (Application Interface Programming) <api_core.html>`_


Application d'un utilitaire
---------------------------

.. index:: Utilitaire lancé par l'interface graphique

Sélection
+++++++++

La sélection de l'utilitaire se fait en double-cliquant sur son nom, depuis l'explorateur ou le menu. La section principale de l'interface graphique affiche alors 

- en **haut**: le nom et la # Description de cet utilitaire. La zone grise devient **bleue** ;
- au **milieu**: les options disponibles pour celui-ci.    

Définition des options
++++++++++++++++++++++

Dans le premier onglet *Options*, l'utilisateur doit définir les options obligatoires pour l'utilitaire retenu. Il peut aussi définir les arguments optionnels.

.. figure:: _static/gui_example_opt.png
   :alt: Exemple: Récupération des valeurs Antilope J+1 (antilope j1 rr) horaire (PH) depuis la base BdImage (projet LAMEDO), pour la période définie entre le 13/06/2017 12:00 et le 14/06/2017 00:00, pour la zone LO808, en ciblant les valeurs statistiques standard selon une précision standard
   :figclass: figurecentre

   Exemple: Récupération des valeurs Antilope J+1 (antilope j1 rr) horaire (PH) depuis la base BdImage (projet LAMEDO), pour la période définie entre le 13/06/2017 12:00 et le 14/06/2017 00:00, pour la zone LO808, en ciblant les valeurs statistiques standard selon une précision standard

Ligne de commande et exécution
++++++++++++++++++++++++++++++

Une fois les arguments définis par l'utilisateur, celui-ci peut basculer sur le second onglet *Exécution*. Celui-ci offre les fonctionnalités suivantes:

- définir la ligne de commande correspondant à la demande de l'utilisateur en cliquant sur le bouton **Générer la ligne de commande**. Ainsi, il est facile de construire ces propres scripts ;
- lancer le script avec ses arguments en cliquant sur le bouton **Lancer l'utilitaire**. Cette action créera et affichera la ligne de commande correspondante. La grande zone de texte contiendra le journal issu de l'# Exécution de l'utilitaire

.. figure:: _static/gui_example_exe_cmd.png
   :alt: Exemple: Affichage de la ligne de commande
   :figclass: figurecentre

   Exemple: Affichage de la ligne de commande
   
.. figure:: _static/gui_example_exe_launch.png
   :alt: Exemple: Lancement de dbase2csv.py
   :figclass: figurecentre

   Exemple: Lancement de dbase2csv.py
   
.. figure:: _static/gui_example_exe_run.png
   :alt: Exemple: Journal de l'exécution de dbase2csv.py
   :figclass: figurecentre

   Exemple: Journal de l'exécution de dbase2csv.py


.. image:: _static/build.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Générer la ligne de commande** permet de construire la ligne de commande correspondant à l'utilitaire courant et aux options définies par l'utilisateur.


.. image:: _static/run.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Lancer l'utilitaire** permet d'exécuter la ligne de commande correspondant à l'utilitaire courant et aux options définies par l'utilisateur.


.. image:: _static/stop.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Arrêter l'utilitaire** permet d'arrêter l'exécution de la ligne de commande correspondant à l'utilitaire courant et aux options définies par l'utilisateur.


.. image:: _static/audio_on.png
   :height: 20 px
   :alt: alternate text
   :align: left

Si le bouton **Son** est mis sur *On*, un son est émis en cas d'erreur lors de l'exécution de la ligne de commande correspondant à l'utilitaire courant et aux options définies par l'utilisateur.


.. image:: _static/audio_off.png
   :height: 20 px
   :alt: alternate text
   :align: left

Si le bouton **Son** est mis sur *Off*, une erreur lors de l'exécution n'engendre pas d'émission de son



Aide
++++

Dans le dernier onglet *Aide*, l'utilisateur peut lire l'aide correspondant à l'utilitaire sélectionné. Cet onglet fonctionne comme un navigateur web. Il peut donc aussi accéder aux autres pages *HTML* de la documentation du module *pySPC* qui repose sur le projet **pyspc-doc**.
   
.. figure:: _static/gui_example_help.png
   :alt: Exemple: Aide de l'utilitaire duplicatePlathynesEvent.py
   :figclass: figurecentre

   Exemple: Aide de l'utilitaire duplicatePlathynesEvent.py


.. image:: _static/webbrowser.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Voir dans un navigateur** offre la possibilité de parcourir l'aide via le navigateur web par défaut.


.. image:: _static/initial.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Vue initiale** offre la possibilité de retourner à la première page selon la navigation dans l'aide


.. image:: _static/forward.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Suivant** offre la possibilité de revoir la page suivante de la navigation dans l'aide


.. image:: _static/back.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Précédent** offre la possibilité de revoir la page précédente de la navigation dans l'aide


.. image:: _static/reload.png
   :height: 20 px
   :alt: alternate text
   :align: left

Le bouton **Actualiser** offre la possibilité de recharger la page courante

