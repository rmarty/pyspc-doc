.. fichier rst créé automatiquement par scriptdoc

.. _script:

.. role:: blue

.. role:: boldblue

Utilisation avancée par script
------------------------------

.. index:: Ligne de commande

.. index:: Script

Afin de réaliser des traitements en masse, le paquet pySPC comprend des scripts. Ceux-ci, placés dans pySPC\script\, ont le caractère _ comme préfixe. Le(s) paramètre(s) y est/sont écrit(s) “en dur”. Certains d’entre eux utilisent des programmes de ce module pySPC. Dans ce cas, ceux-ci sont mentionnés dans les pages documentaires des scripts.

.. warning::
    Les scripts ne font pas l'objet de tests unitaires. Leur fonctionnement
    ne peut être totalement garanti à chaque nouvelle version de pyspc.
    Il convient donc d'être prudent lors de toute première utilisation d'un
    script.




Traitement des données BdImages
+++++++++++++++++++++++++++++++

Ces scripts sont dédiés au téléchargement des données BdImages par bloc. Ainsi l'utilisateur peut réduire le risque de refus de requête par le serveur si celle-ci demande trop de données.


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_bdimage2grp_download
   script/_bdimage2grp_extract



Traitement des données Cristal
++++++++++++++++++++++++++++++

Ces scripts sont dédiés aux données Cristal.


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_cristal_export



Modélisation GRP: version 2020
++++++++++++++++++++++++++++++

Ces scripts sont dédiés à la création de fichiers pour le calage de modèle GRP ainsi que le calage en masse de modèles GRP dans sa version 2020.


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_grp20_00_16RTto20_bvfile
   script/_grp20_00_16to20_bvfile
   script/_grp20_00_renamestation_bvfile
   script/_grp20_00_rrta_file
   script/_grp20_01_16RTto20_cfgfile
   script/_grp20_01_16to20_cfgfile
   script/_grp20_01_refspc2cfgfile
   script/_grp20_02_16to20_etp
   script/_grp20_10_sacha2grp
   script/_grp20_20_create_localtree
   script/_grp20_20_report_model
   script/_grp20_21_gpdplot_verification
   script/_grp20_21_report_verification
   script/_grp20_22_build_runs4rtime
   script/_grp20_22_gpdplot_runs4rtime
   script/_grp20_23_gpdplot_rtime
   script/_grp20_23_report_rtime
   script/_grp20_24_archive_otamin



Modélisation GRP: version 2016 (calage)
+++++++++++++++++++++++++++++++++++++++

Ces scripts sont dédiés à la création de fichiers pour le calage de modèle GRP ainsi que le calage en masse de modèles GRP

.. warning::
    Le retrait de ces scripts est prévu pour la version 3.1.0



.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_grpCal_computeETP
   script/_grpCal_extendETP
   script/_grpCal_fromRT
   script/_grpCal_plotHypso
   script/_grpCal_run16
   script/_grpCal_sacha2grp
   script/_grpCal_wav



Modélisation GRP: version 2016 (temps-réel)
+++++++++++++++++++++++++++++++++++++++++++

Ces scripts sont dédiés à la création de fichiers pour le rejeu en masse en temps-différé de son module Temps-Réel.

.. warning::
    Le retrait de ces scripts est prévu pour la version 3.1.0



.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_grpRT_fcst16



Documentation Météo-France
++++++++++++++++++++++++++

Ce script est dédié au téléchargement des fiches et rapports de Météo-France


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_mfReports



Traitement des données PHyC
+++++++++++++++++++++++++++

Ces scripts sont dédiés au téléchargement des données PHyC par bloc. Ainsi l'utilisateur peut réduire le risque de refus de requête par le serveur si celle-ci demande trop de données.


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_phyc2xml



Modélisation PLATHYNES
++++++++++++++++++++++

Ce script est dédié à la définition des événements hydrologiques pour Plathynes depuis la PHyC.


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_Plathynes_find_hydroEvents



Retours d'expérience SPC LACI
+++++++++++++++++++++++++++++

Ces scripts sont dédiés aux retours d'expérience post-crue: récupération des données, rejeu des modèles opérationnels, synthèse graphique et statistique.


.. toctree::
   :maxdepth: 2
   :titlesonly:
   
   script/_postEvent_000_locations
   script/_postEvent_001_subreach_bylochydro
   script/_postEvent_002_subreach_bygrp
   script/_postEvent_100_hydro2_export
   script/_postEvent_110_phyc_download
   script/_postEvent_111_phyc_convert
   script/_postEvent_112_phyc_cristaledf2mfdata
   script/_postEvent_120_lamedo_download
   script/_postEvent_121_lamedo_convert
   script/_postEvent_122_bdapbp_download
   script/_postEvent_130_mf_convert
   script/_postEvent_140_regularscale
   script/_postEvent_150_subreach_stats
   script/_postEvent_151_subreach_plot
   script/_postEvent_160_rrtable_bp
   script/_postEvent_200_bp_plot
   script/_postEvent_210_grp_inputs
   script/_postEvent_211_grp_run
   script/_postEvent_212_grp_plotevent
   script/_postEvent_220_mohys_sim
   script/_postEvent_221_mohys_run
   script/_postEvent_222_mohys_plotevent
   script/_postEvent_230_barrage_sim
   script/_postEvent_232_barrage_plotevent
   script/_postEvent_300_rawfcst_plot
   script/_postEvent_301_validfcst_plot
   script/_postEvent_302_validfcst_plotevent
   script/_postEvent_303_validfcst_describe
   script/_postEvent_310_rawfcst_verification
   script/_postEvent_311_validfcst_verification
   script/_postEvent_312_refcst_verification
   script/_postEvent_313_verif_plot
   script/_postEvent_314_verif_plotuncertainty

