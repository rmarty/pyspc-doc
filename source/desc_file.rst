
.. _descfile:

Format de fichier csv
---------------------

Un peu d'histoire
+++++++++++++++++

Le projet pyspc s'est d'abord construit pour faciliter le calage de modèles GRP. Ce format est ainsi devenu le format csv *natif* du projet pyspc. La version 2018 de GRP est issue du développement de l'approche multi pas-de-temps et d'un nouveau module Neige. Le pas de temps, au format *nnJnnHnnM*, est désormais indiqué dans les fichiers d'entrée de précipitation et d'évapotranspiration. **A partir de la version 3, pyspc dispose de son propre fichier csv *natif*, distinct de celui de GRP.**

Convention de nommage
+++++++++++++++++++++

La convention suivante définit le lien entre la nature des données et le nommage des fichiers. Le
nommage des simulations et prévisions intégrera l'identifiant du modèle (voire ensuite le scénario et la probabilité).

- *observation* : IDENTIFIANT_GRANDEUR.txt
- *simulation* : IDENTIFIANT_MODELE_GRANDEUR.txt
- *prévision* : IDENTIFIANT_DTDEROBS_MODELE_GRANDEUR.txt
- *prévision avec scénario* : IDENTIFIANT_DTDEROBS_MODELE_SCENARIO_GRANDEUR.txt
- *prévision avec tendance* : IDENTIFIANT_DTDEROBS_MODELE_SCENARIO_INCERTITUDE_GRANDEUR.txt

.. warning:: IDENTIFIANT, MODELE, SCENARIO et TENDANCE ne doivent pas comporter le caractère "_"

.. warning:: DTDEROBS est à définir au format AAAAMMJJHH

.. warning:: GRANDEUR est à définir parmi la colonne "Variable" du tableau :ref:`table_spcvarnames`

.. note:: Il est possible de forcer la prise en compte d'un nom de fichier en tant qu'observation ou en tant que simulation

Le format natif accepte plusieurs séries dans un même fichier de façon à réduire le nombre de *petits* fichiers. A minima, les termes IDENTIFIANT et GRANDEUR sont obligatoires. Les termes complémentaires sont à spécifier dans l'entête de colonne. Si une série correspond à une autre grandeur, son entête doit être défini selon le schéma ci-dessus.

.. note:: Des exemples de fichiers sont situés dans le dépôt `Bitbucket <https://bitbucket.org/rmarty/pyspc/src/master/test/data/io/pyspcfile/>`_

.. seealso:: :ref:`descvar`.

.. seealso:: :ref:`descdate`.


Fichier de configuration
++++++++++++++++++++++++

Les fichiers de configuration sont des fichiers *texte*, encodés en **utf-8**. Ils se présentent dans la structure suivante

.. code-block:: cfg

   [section]
   option = valeur de l'option
   
   [autre section]
   text = hello world
   valeur = 123


