Installation du module pySPC
============================

Le module pySPC fonctionne sur python 3 dont les conseils d'installation sont précisés dans :ref:`pyinstall`.

Selon son besoin, l'utilisateur peut installer:

- soit le :ref:`pyspcinstall`
- soit seulement l':ref:`classicinstall`

Dans les deux cas, il est important d'installer les :ref:`listsitepackage` nécessaires.
