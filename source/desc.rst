
.. _descintro:

Introduction
============

Le paquet **pySPC** est destiné à traiter, convertir, afficher, analyser les séries de données pluviométriques, hydrométriques, statistiques, de modélisation à partir de bases de données, de fichiers.

.. seealso:: :ref:`descproc`.


Il est capable de manipuler un grand nombre de grandeurs hydro-météorologiques et hydrologiques, à pas de temps fixes ou irréguliers.

.. seealso:: :ref:`descvar`.

.. seealso:: :ref:`descusage`.


De nombreux fichiers peuvent être lus par **pySPC**, dont son format csv spécifique contenant des données au format *AAAAMM[JJ[HH[mm]]];Valeur*

.. seealso:: :ref:`descfile`.

.. seealso:: :ref:`descdate`.
