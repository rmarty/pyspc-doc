.. fichier rst créé automatiquement par bindoc

.. _usage:

Utilisation par ligne de commande
=================================

.. role:: blue

.. role:: boldblue

.. index:: Ligne de commande


Vous trouverez ci-dessous les utilitaires disponibles dans pySPC, organisés par grande thématique du panel d'activités du modélisateur 


.. note:: Chaque utilitaire peut être exécuté via une interface graphique, voir :ref:`gui`. Les utilitaires y sont organisés de la même façon.


.. seealso:: Si l'utilitaire fait référence à des dates, vous pouvez obtenir leur description en cherchant parmi les :ref:`descdate`.


.. seealso:: Si l'utilitaire fait référence à des fichiers, vous pouvez obtenir leur description en cherchant parmi les :ref:`descfile`.


.. seealso:: Si l'utilitaire fait référence à des grandeurs, vous pouvez obtenir leur description en cherchant parmi les :ref:`descvar`.


.. seealso:: Si l'utilitaire fait référence à des types de données, vous pouvez obtenir leur description en cherchant, parmi les :ref:`apiconvention`, les variables souvent nommées DATATYPES, DATANAMES, ou assimilées.



Observations et prévisions: Cristal, Hydro-2, Météo-France, Vigicrues
---------------------------------------------------------------------



Bases locales: SACHA, BAREME, Prévision, RefSPC
+++++++++++++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/csv2dbase
   bin/dbase2csv
   bin/dbase2plathynes
   bin/dbaseCreate
   bin/dbaseInfo


Archives Cristal
++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/cristal2csv
   bin/cristal2xmlSandre


Documents en ligne: Hydroclim, Shyreg, Météo-France
+++++++++++++++++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/onlineReport


Données, exports et statistiques Hydro-2
++++++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/hydro2csv
   bin/hydroExport
   bin/hydroStat


Données issues de Météo-France
++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/csv2mf
   bin/mf2csv
   bin/mf2mf
   bin/mfOpenWS


Bases Vigicrues: BdApbp, BdImage, PHyC, Vigicrues
+++++++++++++++++++++++++++++++++++++++++++++++++



:boldblue:`Bulletin APBP`


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/bdapbp2json
   bin/bdapbpjson2csv


:boldblue:`Images Radar, Prévisions en point de grille`


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/bdimage2xml
   bin/bdimagexml2csv


:boldblue:`Observations, Prévisions et Informations PHyC`


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/hydroportailStats
   bin/phyc2plathynes
   bin/phyc2xml
   bin/xmlSandre2csv
   bin/xmlSandreInfo


Évaluation de prévision (OTAMIN, SCORES)
----------------------------------------



Préparation des incertitudes de modèle (OTAMIN)
+++++++++++++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/csv2prv
   bin/grpRT2prv
   bin/prv2csv


Évaluation de séries hydrologiques par SCORES
+++++++++++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/csv2prv
   bin/duplicateScoresCfg
   bin/grpRT2prv
   bin/prv2csv
   bin/xmlScores2png


Modélisation (GRP, Plathynes)
-----------------------------



Vérification des modèles GRP calés
++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/grpVerif
   bin/plotGrpVerif


Import/Export de PLATHYNES
++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/dbase2plathynes
   bin/duplicatePlathynesEvent
   bin/phyc2plathynes
   bin/plathynes2csv


Prévision hydrologique (GRP)
----------------------------



Configuration et données de GRP Temps-Réel
++++++++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/csv2grpRT
   bin/duplicateGrpRTCfg
   bin/grpRT2csv


Traitement de données
---------------------



Affichage graphique des données
+++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/plotCsvData


Traitements: conversion, information
++++++++++++++++++++++++++++++++++++


.. toctree::
   :maxdepth: 2
   :titlesonly:

   bin/comparePeakFlow
   bin/csv2csv
   bin/csvInfo
