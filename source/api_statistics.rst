
.. currentmodule:: pyspc.statistics

Statistiques
============

Fréquence
---------

.. autosummary::
    :toctree: api/

    freq.RANK2FREQ
    freq.from_rank
    freq.to_period
    freq.to_ugumbel


Gradex
------

.. autosummary::
    :toctree: api/

    gradex.apply
    gradex.compute
    gradex.rr2q
    gradex.montana


Période de retour
-----------------

.. autosummary::
    :toctree: api/

    period.asstr
    period.to_freq
