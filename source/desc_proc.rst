
.. _descproc:

Traitements de données
----------------------

Le projet **pySPC** permet d'accéder à de nombreuses sources de données couramment utilisées au sein du réseau Vigicrues, de faciliter la modélisation par une meilleure portabilité des formats de fichier et de réaliser un grand nombre de traitements de données.

Hydro-météorologie
~~~~~~~~~~~~~~~~~~

-  récupérer les données hydrométéorologiques (`lames d'eau radar <http://wikhydro.developpement-durable.gouv.fr/index.php/B.04_-_Estimation_d%27une_pluie_de_bassin_par_observation_RADAR>`__, `Bulletins Précipitations <http://wikhydro.developpement-durable.gouv.fr/index.php/B.13_-_Pr%C3%A9visions_num%C3%A9riques_de_pr%C3%A9cipitation#.C3.80_l.27.C3.A9chelle_des_SPC.C2.A0:_les_bulletins_de_pr.C3.A9vision_.28AP.2FBP.29>`__, `prévisions Symposium <https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=135&id_rubrique=50>`__) issues de la base de données `LAMEDO <https://gitlab.com/vigicrues/lamedo/bdimage3>`__ du **Schapi** ;
-  convertir les données de **Météo-France** (`publithèque <https://publitheque.meteo.fr>`__, `Bulletins Précipitations <http://wikhydro.developpement-durable.gouv.fr/index.php/B.13_-_Pr%C3%A9visions_num%C3%A9riques_de_pr%C3%A9cipitation#.C3.80_l.27.C3.A9chelle_des_SPC.C2.A0:_les_bulletins_de_pr.C3.A9vision_.28AP.2FBP.29>`__, `prévisions Symposium <https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=135&id_rubrique=50>`__) vers/depuis le format *csv* de GRP ;
-  récupérer `les bulletins climatologiques <https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=129&id_rubrique=52>`__ et `les fiches stations <https://donneespubliques.meteofrance.fr/?fond=contenu&id_contenu=37>`__ de **Météo-France** ;
-  récupérer `les statistiques **Shyreg Pluie** <https://shyreg.pluie.recover.inrae.fr>`__ de l'**INRAE** ;

Observation hydrologique
~~~~~~~~~~~~~~~~~~~~~~~~

-  appliquer des courbes de tarage depuis **Bareme** ou la `PHyC <https://www.hydroportail.developpement-durable.gouv.fr/>`__ ;
-  lire des bases de données Access: **SACHA**, **Bareme** ;
-  convertir les données issues des archives `CRISTAL <http://www.centre.developpement-durable.gouv.fr/le-reseau-cristal-a86.html>`__ au format de la `Banque HYDRO-2 <https://www.ecologique-solidaire.gouv.fr/banque-hydro>`__ (*H-TEMPS*, *QTVAR*) ;
-  créer les procédures d'export de la `Banque HYDRO-2 <http://www.hydro.eaufrance.fr/>`__ (*H-TEMPS*, *QTVAR*, *QTFIX*, *TOUSMOIS*, *DEBCLA*, *CRUCAL*)
-  convertir les données de la `Banque HYDRO-2 <http://www.hydro.eaufrance.fr/>`__ (*H-TEMPS*, *QTVAR*, *QTFIX*) vers/depuis le format *csv* de GRP ;
-  récupérer les statistiques des exports de la `Banque HYDRO-2 <http://www.hydro.eaufrance.fr/>`__ (*DEBCLA*, *CRUCAL*) ;
-  convertir les *observations* et les *prévisions* issues de la `PHyC <https://www.hydroportail.developpement-durable.gouv.fr/>`__ du format `XML-Sandre <http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1>`__ au format *csv* de GRP ;
-  récupérer les *informations hydrométriques* (courbe de tarage, courbe de correction, jaugeage) issues de la `PHyC <https://www.hydroportail.developpement-durable.gouv.fr/>`__ du format `XML-Sandre <http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1>`__ ;
-  convertir les données exportées depuis **Graphyte** vers les formats *csv* de GRP et `Banque HYDRO-2 <https://www.ecologique-solidaire.gouv.fr/banque-hydro>`__;
-  convertir les données selon la cote de déversement (Z0) et le barème d'un `Réservoir <https://fr.wikipedia.org/wiki/Barrage>`__;
-  récupérer `les fiches des bassins versants <https://webgr.inrae.fr/activites/base-de-donnees>`__ et `les fiches SHYREG Débit <https://shyreg.recover.inrae.fr/>`__ de l'**Inrae** (*Irstea*) (*shyreg*: nécessite **pdfminer** dans sa version `python3 <https://pypi.python.org/pypi/pdfminer3k>`__) ;

Modélisation hydrologique
~~~~~~~~~~~~~~~~~~~~~~~~~

-  préparer les données pour `GRP <https://webgr.inrae.fr/modeles/modele-de-prevision-grp/>`__ en mode Calage et en mode Temps-Réel ;
-  caler en *masse* des modèles **GRP-16**, **GRP-18** **GRP-20** et **GRP-22** ;
-  récupérer les scores de performance des modèles GRP calés depuis les fiches de performance (nécessite **pdfminer** dans sa version `python3 <https://pypi.python.org/pypi/pdfminer3k>`__) ;
-  tracer sous forme graphique (radar) les valeurs des scores POD, FAR et CSI selon les seuils de calage, les horizons de prévisions et les seuils de vigilance ;
-  rejouer en *masse* des événements passés par **GRP-16**, **GRP-18** **GRP-20** et **GRP-22** ;
-  lire et traiter les **fichiers d'observations** (.mqo, .mqi, .mgr) de la plate-forme de modélisation hydrologique `PLATHYNES <https://gitlab.com/vigicrues/plathynes/plathynes>`__ ;
-  lire et traiter les **simulations** de la plate-forme de modélisation hydrologique `PLATHYNES <https://gitlab.com/vigicrues/plathynes/plathynes>`__ ;
-  lire et traiter les **exports** de la plate-forme de modélisation hydrologique `PLATHYNES <https://gitlab.com/vigicrues/plathynes/plathynes>`__ ;
-  construire et visualiser les **hydrogrammes unitaires** selon le formalisme de `PLATHYNES <https://gitlab.com/vigicrues/plathynes/plathynes>`__ ou de MOHYS;
-  lire et traiter les **fichiers de configuration** (projet, événement...) de la plate-forme de modélisation hydrologique `PLATHYNES <https://gitlab.com/vigicrues/plathynes/plathynes>`__ ;
-  convertir les données *prv* des applications **SCORES** et `OTAMIN-Temps Réel <https://webgr.inrae.fr/modeles/otamin/>`__ vers/depuis le format *csv* de GRP ;
-  extraire et tracer les scores calculés par l'application **SCORES**

Prévision et vigilance hydrologique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  extraire les prévisions brutes, validées et expértisées depuis la base **Prévision** du SPC LCI dans sa version 2014-2016 ;
-  extraire les prévisions brutes, validées/expértisées et diffusées depuis la base **Prévision** du SPC LCI dans sa version 2017-2019 ;
-  extraire les prévisions brutes, validées/expértisées et diffusées depuis la base **Prévision** du SPC LCI dans sa version 2019-.... ;
-  convertir les prévisions brutes, validées et/ou expértisées issues de la `PHyC <https://www.hydroportail.developpement-durable.gouv.fr/>`__ ou de la `POM <https://www.ecologique-solidaire.gouv.fr/sites/default/files/Schapi_BILAN_2019_SR-WEB-planche.pdf>`__ du format `XML-Sandre <http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1>`__ au format *csv* de GRP ;
-  récupérer et convertir les données relatives aux **tronçons** de vigilance, aux **observations** et aux **prévisions** de `Vigicrues <https://www.vigicrues.gouv.fr/>`__ ;
-  préparer les données d'observation, télécharger et synthétiser les prévisions du projet `PREMHYCE <https://webgr.inrae.fr/projets/projets-en-cours/onema-premhyce/>`__ ;

