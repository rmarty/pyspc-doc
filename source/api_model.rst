
.. currentmodule:: pyspc.model

Modélisation Hydrologique
=========================


GRPv2016
--------

Le module pySPC est capable de manipuler des données de plusieurs fichiers liés au modèle pluie-débit GRP

- en condition de calage : GRP *Calage*
- en condition opérationnelle : GRP *Temps-Réel*


.. _table_grpfiles:

.. table:: Correspondance entre les fichiers GRP et les classes pySPC
   :widths: 1,1,1

   +------------------+------------------------------------+-----------------------------+
   +    Catégorie     +              Fichier               +           Classe            +
   +==================+====================================+=============================+
   +   GRP *Calage*   + Bassins / 12345678.DAT             + GRP_Basin                   +
   +------------------+------------------------------------+-----------------------------+
   +   GRP *Calage*   + BDD_P / 12345678_P.txt             + GRP_Data                    +
   +------------------+------------------------------------+-----------------------------+
   +   GRP *Calage*   + BDD_Q / 12345678_Q.txt             + GRP_Data                    +
   +------------------+------------------------------------+-----------------------------+
   +   GRP *Calage*   + BDD_E / 12345678_E.txt             + GRP_Data                    +
   +------------------+------------------------------------+-----------------------------+
   +   GRP *Calage*   + BDD_T / 12345678_T.txt             + GRP_Data                    +
   +------------------+------------------------------------+-----------------------------+
   +   GRP *Calage*   + Resultats / \*_PP_P0P0.TXT         + GRP_Fcst                    +
   +------------------+------------------------------------+-----------------------------+
   +   GRP *Calage*   + Ficher performance PDF             + GRP_Verif                   +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + BD_Debit / 12345678 / QV_10A.DAT   + GRPRT_Archive               +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + BD_Pluie / 12345678 / PF_10A.DAT   + GRPRT_Archive               +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + BD_Debit / 12345678 / BASSIN.DAT   + GRPRT_Basin                 +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Paramétrage / Config_Prevision.txt + GRPRT_Cfg                   +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Entrées / Debit.txt                + GRPRT_Data                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Entrées / Pluie.txt                + GRPRT_Data                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Entrées / Temp.txt                 + GRPRT_Data                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Entrées / Scen_XXX_PluScenXXX.txt  + GRPRT_Data ou GRPRT_Metscen +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Sorties / GRP_Obs.txt              + GRPRT_Fcst                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Sorties / GRP_Sim.txt              + GRPRT_Fcst                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Sorties / GRP_Prev_xxxx.txt        + GRPRT_Fcst                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Sorties / GRP_D_Obs.txt            + GRPRT_Fcst                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Sorties / GRP_D_Sim.txt            + GRPRT_Fcst                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + Sorties / GRP_D_Prev_xxxx.txt      + GRPRT_Fcst                  +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + BD_Debit / 12345678 / PQE_1A.DAT   + GRPRT_Intern                +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + BD_Debit / 12345678 / PQE_1A_D.DAT + GRPRT_Intern                +
   +------------------+------------------------------------+-----------------------------+
   + GRP *Temps-Réel* + BD_Debit / 12345678 / PARAM.DAT    + GRPRT_Param                 +
   +------------------+------------------------------------+-----------------------------+

Calage
++++++

.. autosummary::
    :toctree: api/

    grp16.cal_basin.GRP_Basin
    grp16.cal_config.GRP_Cfg
    grp16.cal_data.GRP_Data
    grp16.cal_event.GRP_Event
    grp16.cal_fcst.GRP_Fcst
    grp16.cal_verif.GRP_Verif

Temps-Réel
++++++++++

.. autosummary::
    :toctree: api/

    grp16.rt_archive.GRPRT_Archive
    grp16.rt_basin.GRPRT_Basin
    grp16.rt_config.GRPRT_Cfg
    grp16.rt_data.GRPRT_Data
    grp16.rt_fcst.GRPRT_Fcst
    grp16.rt_intern.GRPRT_Intern
    grp16.rt_param.GRPRT_Param


GRPv2018
--------

Le module pySPC est capable de manipuler des données de plusieurs fichiers liés au modèle pluie-débit GRP

- en condition de calage : GRP *Calage*
- en condition opérationnelle : GRP *Temps-Réel*

Calage
++++++

.. autosummary::
    :toctree: api/

    grp18.cal_basin.GRP_Basin
    grp18.cal_config.GRP_Cfg
    grp18.cal_data.GRP_Data
    grp18.cal_event.GRP_Event
    grp18.cal_fcst.GRP_Fcst
    grp18.cal_verif.GRP_Verif

Temps-Réel
++++++++++

.. autosummary::
    :toctree: api/

    grp18.rt_archive.GRPRT_Archive
    grp18.rt_basin.GRPRT_Basin
    grp18.rt_config.GRPRT_Cfg
    grp18.rt_data.GRPRT_Data
    grp18.rt_metscen.GRPRT_Metscen
    grp18.rt_fcst.GRPRT_Fcst
    grp18.rt_intern.GRPRT_Intern
    grp18.rt_param.GRPRT_Param



GRPv2020
--------

Le module pySPC est capable de manipuler des données de plusieurs fichiers liés au modèle pluie-débit GRP

- en condition de calage : GRP *Calage*
- en condition opérationnelle : GRP *Temps-Réel*

Calage
++++++

.. autosummary::
    :toctree: api/

    grp20.cal_basin.GRP_Basin
    grp20.cal_config.GRP_Cfg
    grp20.cal_data.GRP_Data
    grp20.cal_event.GRP_Event
    grp20.cal_fcst.GRP_Fcst
    grp20.cal_report.report_model
    grp20.cal_report.report_verif
    grp20.cal_verif.GRP_Verif
    grp20.tdelta

Temps-Réel
++++++++++

.. autosummary::
    :toctree: api/

    grp20.rt_archive.GRPRT_Archive
    grp20.rt_basin.GRPRT_Basin
    grp20.rt_config.GRPRT_Cfg
    grp20.rt_data.GRPRT_Data
    grp20.rt_metscen.GRPRT_Metscen
    grp20.rt_fcst.GRPRT_Fcst
    grp20.rt_intern.GRPRT_Intern
    grp20.rt_param.GRPRT_Param



GRPv2022
--------

Le module pySPC est capable de manipuler des données de plusieurs fichiers liés au modèle pluie-débit GRP

- en condition de calage : GRP *Calage*
- en condition opérationnelle : GRP *Temps-Réel*

Calage
++++++

.. autosummary::
    :toctree: api/

    grp22.cal_basin.GRP_Basin
    grp22.cal_config.GRP_Cfg
    grp22.cal_data.GRP_Data
    grp22.cal_event.GRP_Event
    grp22.cal_fcst.GRP_Fcst
    grp22.cal_report.report_model
    grp22.cal_report.report_verif
    grp22.cal_verif.GRP_Verif
    grp22.tdelta

Temps-Réel
++++++++++

.. autosummary::
    :toctree: api/

    grp22.rt_archive.GRPRT_Archive
    grp22.rt_basin.GRPRT_Basin
    grp22.rt_config.GRPRT_Cfg
    grp22.rt_data.GRPRT_Data
    grp22.rt_metscen.GRPRT_Metscen
    grp22.rt_fcst.GRPRT_Fcst
    grp22.rt_intern.GRPRT_Intern
    grp22.rt_param.GRPRT_Param
    grp22.rt_utils



MOHYS
-----

.. autosummary::
    :toctree: api/

    mohys.unit_hydrograph



MORDOR
------

.. autosummary::
    :toctree: api/

    mordor.forecast.Fcst
    mordor.volumes.Vol



OTAMINv2016
-----------

.. autosummary::
    :toctree: api/

    otamin16.data.Data
    otamin16.rt_data.RT_Data
    otamin16.table.Table
    


OTAMINv2018
-----------

.. autosummary::
    :toctree: api/

    otamin18.data.Data
    otamin18.rt_data.RT_Data
    otamin18.table.Table



PLATHYNES
---------

.. autosummary::
    :toctree: api/

    plathynes.config.Config
    plathynes.observation.Data
    plathynes.export.Export
    plathynes.results.Results
    plathynes.tdelta
    plathynes.unit_hydrograph



PREMHYCE
--------

.. autosummary::
    :toctree: api/

    premhyce.observation.Data
    premhyce.forecast.Fcst



ETP Oudin
---------

.. autosummary::
    :toctree: api/

    oudin.daily2hourly
    oudin.daily_ETP

.. code-block:: bib

   @PHDTHESIS{oudin:pastel-00000931,
     author = {Oudin, Ludovic},
     title = {{Recherche d'un mod{\`e}le d'{\'e}vapotranspiration potentielle pertinent
       comme entr{\'e}e d'un mod{\`e}le pluie-d{\'e}bit global}},
     school = {{ENGREF (AgroParisTech)}},
     year = {2004},
     type = {Theses},
     month = Oct,
     file = {memoire.pdf:https\://pastel.archives-ouvertes.fr/pastel-00000931/file/memoire.pdf:PDF},
     hal_id = {pastel-00000931},
     hal_version = {v1},
     keywords = {Sciences de l'eau ; Hydrologie ; Evapotranspiration ; Mod{\'e}lisation
       pluie-d{\'e}bit ; {\'e}vapotranspiration potentielle ; Changement
       climatique},
     url = {https://pastel.archives-ouvertes.fr/pastel-00000931}
   }

   @ARTICLE{Oudin2005290,
     author = {Ludovic Oudin and Frederic Hervieu and Claude Michel and Charles
       Perrin and Vazken Andreassian and Francois Anctil and Cecile Loumagne},
     title = {Which potential evapotranspiration input for a lumped rainfall-runoff
       model?: Part 2 Towards a simple and efficient potential evapotranspiration
       model for rainfall-runoff modelling},
     journal = {Journal of Hydrology },
     year = {2005},
     volume = {303},
     pages = {290 - 306},
     number = {1-4},
     abstract = {This research sought to identify the most relevant approach to calculate
       potential evapotranspiration (PE) for use in a daily rainfall-runoff
       model, while answering the following question: How can we use available
       atmospheric variables to represent the evaporative demand at the
       basin scale? The value of 27 \{PE\} models was assessed in terms
       of streamflow simulation efficiency over a large sample of 308 catchments
       located in France, Australia and the United States. While trying
       to identify which atmospheric variables were the most relevant to
       compute \{PE\} as input to rainfall-runoff models, we showed that
       the formulae based on temperature and radiation tend to provide the
       best streamflow simulations. Surprisingly, \{PE\} approaches based
       on the Penman approach seem less advantageous to feed rainfall-runoff
       models. This investigation has resulted in a proposal for a temperature-based
       \{PE\} model, combining simplicity and efficiency, and adapted to
       four rainfall-runoff models. This \{PE\} model only requires mean
       air temperature (derived from long-term averages) and leads to a
       slight but steady improvement in rainfall-runoff model efficiency.},
     doi = {http://dx.doi.org/10.1016/j.jhydrol.2004.08.026},
     issn = {0022-1694},
     keywords = {Rainfall-runoff modelling},
     url = {http://www.sciencedirect.com/science/article/pii/S0022169404004056}
   }



Régime hydrologique de Sauquet
------------------------------

.. autosummary::
    :toctree: api/

    sauquet.REGIMES
    sauquet.compute_distance


Méthode SOCOSE
--------------

.. autosummary::
    :toctree: api/

    socose.socose
    oudin.daily_ETP

