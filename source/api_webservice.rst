
.. currentmodule:: pyspc.webservice

WebServices
===========


Hydro2
------

.. autosummary::
    :toctree: api/

    hydro2.export.Export


HydroPortail
------------

.. autosummary::
    :toctree: api/

    hydroportail.hydroportail.Hydroportail


Lamedo
------

.. autosummary::
    :toctree: api/

    lamedo.bdapbp.BdApbp
    lamedo.bdimage.BdImage


Météo-France
------------

.. autosummary::
    :toctree: api/

    meteofrance.open_api.OpenAPI
    meteofrance.open_api._MF_Client
    meteofrance.open_data.OpenData


PHyC
----

.. autosummary::
    :toctree: api/

    phyc.phyc.PHyC


Documents et rapports en ligne
------------------------------

.. autosummary::
    :toctree: api/

    report.report.OnlineReport
    report.convention

Météo-France
++++++++++++

Les archives BP et Sympo sont celles fournies par R. Lamblin, le pilote du projet LAMEDO. 
Les bulletins sont accessibles gratuitement par le site de données publiques de Météo-France.

- `Bulletins Climatologiques <https://donneespubliques.meteofrance.fr/donnees_libres/bulletins/BCMR/BCMR_03_201706.pdf>`_
- `Fiche Station <https://donneespubliques.meteofrance.fr/metadonnees_publiques/fiches/fiche_07154005.pdf>`_
- `Fiche climatologique des stations <https://donneespubliques.meteofrance.fr/FichesClim/FICHECLIM_03155003.pdf>`_
- `Archive des bulletins de vigilance <https://igilance-public.meteo.fr/>`_


INRAE
+++++

L'INRAE met à disposition différentes bases de données, de statistiques et de prévisions:

- `base hydroclim <https://hydrogr.github.io/BDD-HydroClim>`_
- `base Shyreg Pluie <https://shyreg.pluie.recover.inrae.fr/>`_
- `base Shyreg Débit <https://shyreg.recover.inrae.fr>`_
- `base Premhyce <https://sunshine.irstea.fr/app_direct/premhyce>`_


.. code-block:: bib

   @data{UV01P1_2020,
    author = {Brigode, Pierre and Génot, Benoît and Lobligeois, Florent and Delaigue, Olivier},
    publisher = {Recherche Data Gouv},
    title = {{Summary sheets of watershed-scale hydroclimatic observed data for France}},
    year = {2020},
    version = {V1},
    doi = {10.15454/UV01P1},
    url = {https://doi.org/10.15454/UV01P1}
   }


Vigicrues
+++++++++

Il est possible d'accéder aux informations diffusées par Vigicrues par le biais de webservices:

- `service / tronçon <https://www.vigicrues.gouv.fr/services/vigicrues.geojson'>`_
- `service / station <https://www.vigicrues.gouv.fr/services/station.json>`_
- `service / observation au format Sandre <https://www.vigicrues.gouv.fr/services/observations.xml>`_
- `service / observation <https://www.vigicrues.gouv.fr/services/observations.json>`_
- `service / prévision <https://www.vigicrues.gouv.fr/services/previsions.json>`_

- `service v1 / information crues <https://www.vigicrues.gouv.fr/services/vigicrues.jsonld>`_
- `service v1 / geo-information <https://www.vigicrues.gouv.fr/services/vigicrues.geojson'>`_
- `service v1 / territoire <https://www.vigicrues.gouv.fr/services/TerEntVigiCru.jsonld'>`_
- `service v1 / tronçon <https://www.vigicrues.gouv.fr/services/TronEntVigiCru.jsonld'>`_
- `service v1 / station <https://www.vigicrues.gouv.fr/services/TStaEntVigiCru.jsonld'>`_

