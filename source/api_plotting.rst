
.. currentmodule:: pyspc.plotting

Images
======

Configuration
-------------

.. autosummary::
    :toctree: api/

    config.Config


Bulletin AP-BP
--------------

.. autosummary::
    :toctree: api/

    bp.plot_bp_byzone
    bp.plot_bp_byday


Analyse de pointes de crue
--------------------------

.. autosummary::
    :toctree: api/

    peakflow.plot_peakflow_analysis



Vérification de prévision
-------------------------

.. autosummary::
    :toctree: api/

    verification.plot
    verification.plot_uncertainty

