
.. currentmodule:: pyspc.metadata

Méta-données
============

Hydro2
------

.. autosummary::
    :toctree: api/

    hydro2.stat.Hydro2


Référentiel du SPC Loire-Allier-Cher-Indre
------------------------------------------

.. autosummary::
    :toctree: api/

    refspc.refspc.RefSPC


SHYREG
------

.. autosummary::
    :toctree: api/

    shyreg.precipitation.Precipitation
    shyreg.streamflow.Streamflow


Vigicrues
---------

.. autosummary::
    :toctree: api/

    vigicrues.location.Vigicrues_Location
    vigicrues.reach.Vigicrues_Reach
    vigicrues.service.Service


XML Sandre
----------

.. autosummary::
    :toctree: api/

    sandre.sandre.Sandre
