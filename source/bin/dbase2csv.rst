
.. fichier rst créé automatiquement par bindoc

.. _dbase2csv:

.. role:: blue

.. role:: boldblue

.. index:: dbase2csv


dbase2csv
=========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.dbase2csv
    :func: set_parser
    :prog: dbase2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les données de la grandeur None depuis la base Prévision prevision_2014_hydro3.mdb de type ('previ14') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2016053116 au 2016053116.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d prevision_2014_hydro3.mdb -O data/_bin/dbase2csv/out -s K6373020 -F 2016053116 -L 2016053116 -t previ14 -3 -C pyspc -1


:blue:`Extraire les données de la grandeur None depuis la base Prévision prevision_2014_hydro3.mdb de type ('previ14_val') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2016053116 au 2016053116.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d prevision_2014_hydro3.mdb -O data/_bin/dbase2csv/out -s K6402520 -F 2016053116 -L 2016053116 -t previ14_val -3 -C pyspc -1


:blue:`Extraire les données de la grandeur None depuis la base Prévision prevision_2017.mdb de type ('previ17') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2016112112 au 2016112112.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d prevision_2017.mdb -O data/_bin/dbase2csv/out -s K0260010 -F 2016112112 -L 2016112112 -t previ17 -3 -C pyspc -1


:blue:`Extraire les données de la grandeur None depuis la base Prévision prevision_2017.mdb de type ('previ17_val') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2016112112 au 2016112112.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d prevision_2017.mdb -O data/_bin/dbase2csv/out -s K0403010 -F 2016112112 -L 2016112112 -t previ17_val -3 -C pyspc -1


:blue:`Extraire les données de la grandeur None depuis la base Prévision PRV_201801.mdb de type ('previ19') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2018010412 au 2018010412.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d PRV_201801.mdb -O data/_bin/dbase2csv/out -s K1321810 -F 2018010412 -L 2018010412 -t previ19 -3 -C pyspc -1


:blue:`Extraire les données de la grandeur None depuis la base Prévision PRV_201801.mdb de type ('previ19_val') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2018010412 au 2018010412.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d PRV_201801.mdb -O data/_bin/dbase2csv/out -s K1321810 -F 2018010412 -L 2018010412 -t previ19_val -3 -C pyspc -1


:blue:`Extraire les données de la grandeur None depuis la base Prévision PRV_201801.sqlite de type ('previ19') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2018010412 au 2018010412.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d PRV_201801.sqlite -O data/_bin/dbase2csv/out -s K1321810 -F 2018010412 -L 2018010412 -t previ19 -3 -C pyspc -1


:blue:`Extraire les données de la grandeur HH depuis la base SACHA sacha_montpezat.mdb de type ('sacha') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2008110100 au 2008110223.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d sacha_montpezat.mdb -O data/_bin/dbase2csv/out -n HH -s K010002010 -F 2008110100 -L 2008110223 -t sacha -3 -C pyspc


:blue:`Extraire les données de la grandeur PH depuis la base SACHA sacha_montpezat.mdb de type ('sacha') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. La donnée pluviométrique est celle du radar. L'extraction concerne la période du 2008110100 au 2008110223.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d sacha_montpezat.mdb -O data/_bin/dbase2csv/out -n PH -s 43101002 -F 2008110100 -L 2008110223 -t sacha -S radar -3 -C pyspc


:blue:`Extraire les données de la grandeur PH depuis la base SACHA sacha_montpezat.mdb de type ('sacha') situé dans le répertoire data/io/dbase au format csv de type 'grp18' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-3. L'extraction concerne la période du 2008110100 au 2008110223.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d sacha_montpezat.mdb -O data/_bin/dbase2csv/out -n PH -s 07235005 -F 2008110100 -L 2008110223 -t sacha -3 -C grp18


:blue:`Extraire les données de la grandeur QH depuis la base SACHA sacha_montpezat.mdb de type ('sacha') situé dans le répertoire data/io/dbase au format csv de type 'grp16' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-2. L'extraction concerne la période du 2008110100 au 2008110223.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d sacha_montpezat.mdb -O data/_bin/dbase2csv/out -n QH -s K0100020 -F 2008110100 -L 2008110223 -t sacha -2 -C grp16


:blue:`Extraire les données de la grandeur QH depuis la base SACHA sacha_montpezat.mdb de type ('sacha_TR') situé dans le répertoire data/io/dbase au format csv de type 'pyspc' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-2. L'extraction concerne la période du 2016112100 au 2016112223.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d sacha_montpezat.mdb -O data/_bin/dbase2csv/out -n QH -s K0010020 -F 2016112100 -L 2016112223 -t sacha_TR -2 -C pyspc


:blue:`Extraire les données de la grandeur TH depuis la base SACHA sacha_montpezat.mdb de type ('sacha') situé dans le répertoire data/io/dbase au format csv de type 'grp16' dans le répertoire data/_bin/dbase2csv/out. Le référentiel est celui d'HYDRO-2. L'extraction concerne la période du 2008110100 au 2008110223.`

.. container:: cmdimg

   .. container:: cmdline

      dbase2csv.py -I data/io/dbase -d sacha_montpezat.mdb -O data/_bin/dbase2csv/out -n TH -s 07154005 -F 2008110100 -L 2008110223 -t sacha -2 -C grp16



