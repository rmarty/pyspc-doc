
.. fichier rst créé automatiquement par bindoc

.. _grpVerif:

.. role:: blue

.. role:: boldblue

.. index:: grpVerif


grpVerif
========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.grpVerif
    :func: set_parser
    :prog: grpVerif
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les valeurs des critères de performance des fiches de calage-contrôle de GRP v2016 ('grp16_cal') situées dans le répertoie data/model/grp16/cal pour la station K0403010. Les valeurs sont exportées dans le fichier data/_bin/grpVerif/out/K0403010_cal.csv.`

.. container:: cmdimg

   .. container:: cmdline

      grpVerif.py -I data/model/grp16/cal -c data/_bin/grpVerif/out/K0403010_cal.csv -s K0403010 -t grp16_cal


:blue:`Extraire les valeurs des critères de performance des fiches de calage-contrôle de GRP v2016 ('grp18_cal') situées dans le répertoie data/model/grp18/cal pour la station RH10585x. Les valeurs sont exportées dans le fichier data/_bin/grpVerif/out/RH10585x_cal.csv.`

.. container:: cmdimg

   .. container:: cmdline

      grpVerif.py -I data/model/grp18/cal -c data/_bin/grpVerif/out/RH10585x_cal.csv -s RH10585x -t grp18_cal


:blue:`Extraire les valeurs des critères de performance des fiches de calage complet de GRP v2016 ('grp16_rtime') situées dans le répertoie data/model/grp16/cal pour la station K0403010. Les valeurs sont exportées dans le fichier data/_bin/grpVerif/out/K0403010_rtime.csv.`

.. container:: cmdimg

   .. container:: cmdline

      grpVerif.py -I data/model/grp16/cal -c data/_bin/grpVerif/out/K0403010_rtime.csv -s K0403010 -t grp16_rtime


:blue:`Extraire les valeurs des critères de performance des fiches de calage complet de GRP v2018 ('grp18_rtime') situées dans le répertoie data/model/grp18/cal pour la station RH10585x. Les valeurs sont exportées dans le fichier data/_bin/grpVerif/out/RH10585x_rtime.csv.`

.. container:: cmdimg

   .. container:: cmdline

      grpVerif.py -I data/model/grp18/cal -c data/_bin/grpVerif/out/RH10585x_rtime.csv -s RH10585x -t grp18_rtime



