
.. fichier rst créé automatiquement par bindoc

.. _xmlScores2png:

.. role:: blue

.. role:: boldblue

.. index:: xmlScores2png


xmlScores2png
=============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.xmlScores2png
    :func: set_parser
    :prog: xmlScores2png
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Conversion des critères numériques des données (fcst) contenues dans le fichier resultat_prevision.xml situé dans le répertoire data/verification/scores aux formats csv et png vers le répertoire data/_bin/xmlScores2png/out`

.. container:: cmdimg

   .. container:: cmdline

      xmlScores2png.py -I data/verification/scores -d resultat_prevision.xml -O data/_bin/xmlScores2png/out -t fcst


.. figure:: ../_static/resultat_prevision_BIAS.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_BIAS.png


.. figure:: ../_static/resultat_prevision_MAE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_MAE.png


.. figure:: ../_static/resultat_prevision_MAE_CDF.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_MAE_CDF.png


.. figure:: ../_static/resultat_prevision_MARE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_MARE.png


.. figure:: ../_static/resultat_prevision_PREC.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_PREC.png


.. figure:: ../_static/resultat_prevision_RMSE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_RMSE.png


.. figure:: ../_static/resultat_prevision_STDERR.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_prevision_STDERR.png


:blue:`Conversion des critères numériques des données (sim) contenues dans le fichier resultat_simulation.xml situé dans le répertoire data/verification/scores aux formats csv et png vers le répertoire data/_bin/xmlScores2png/out`

.. container:: cmdimg

   .. container:: cmdline

      xmlScores2png.py -I data/verification/scores -d resultat_simulation.xml -O data/_bin/xmlScores2png/out -t sim


.. figure:: ../_static/resultat_simulation_BIAS.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_BIAS.png


.. figure:: ../_static/resultat_simulation_C2MP.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_C2MP.png


.. figure:: ../_static/resultat_simulation_MAE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_MAE.png


.. figure:: ../_static/resultat_simulation_MAE_CDF.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_MAE_CDF.png


.. figure:: ../_static/resultat_simulation_MARE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_MARE.png


.. figure:: ../_static/resultat_simulation_MAX_MAE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_MAX_MAE.png


.. figure:: ../_static/resultat_simulation_MAX_ME.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_MAX_ME.png


.. figure:: ../_static/resultat_simulation_ME.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_ME.png


.. figure:: ../_static/resultat_simulation_ME_CDF.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_ME_CDF.png


.. figure:: ../_static/resultat_simulation_MRE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_MRE.png


.. figure:: ../_static/resultat_simulation_NASH.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_NASH.png


.. figure:: ../_static/resultat_simulation_PERS.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_PERS.png


.. figure:: ../_static/resultat_simulation_PREC.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_PREC.png


.. figure:: ../_static/resultat_simulation_RMSE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_RMSE.png


.. figure:: ../_static/resultat_simulation_STDERR.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_STDERR.png


.. figure:: ../_static/resultat_simulation_table.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_table.png


.. figure:: ../_static/resultat_simulation_TMAX_MAE.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_TMAX_MAE.png


.. figure:: ../_static/resultat_simulation_TMAX_ME.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_TMAX_ME.png


.. figure:: ../_static/resultat_simulation_VOL.png
   :alt: Figure
   :figclass: figurecentre

   Figure resultat_simulation_VOL.png



