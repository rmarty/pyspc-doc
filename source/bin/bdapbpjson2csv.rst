
.. fichier rst créé automatiquement par bindoc

.. _bdapbpjson2csv:

.. role:: blue

.. role:: boldblue

.. index:: bdapbpjson2csv


bdapbpjson2csv
==============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.bdapbpjson2csv
    :func: set_parser
    :prog: bdapbpjson2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir le fichier BdApbp bp_long.json situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdapbpjson2csv/out. Seule la zone 41003 est extraite. Les données sont exportées dans un seul fichier.`

.. container:: cmdimg

   .. container:: cmdline

      bdapbpjson2csv.py -I data/data/lamedo -d bp_long.json -O data/_bin/bdapbpjson2csv/out -C pyspc -S 41003 -1


:blue:`Convertir le fichier BdApbp bp_short.json situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdapbpjson2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      bdapbpjson2csv.py -I data/data/lamedo -d bp_short.json -O data/_bin/bdapbpjson2csv/out -C pyspc



