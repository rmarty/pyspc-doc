
.. fichier rst créé automatiquement par bindoc

.. _csv2prv:

.. role:: blue

.. role:: boldblue

.. index:: csv2prv


csv2prv
=======

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.csv2prv
    :func: set_parser
    :prog: csv2prv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Conversion au format prv ('otamin18_fcst') des données de débits ('QH') des séries listées dans le fichier data/_bin/csv2prv/in/list_fcst_otamin.txt depuis le fichier csv de type 'pyspc' dans le répertoire data/core/csv vers le répertoire data/_bin/csv2prv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2prv.py -I data/core/csv -l data/_bin/csv2prv/in/list_fcst_otamin.txt -O data/_bin/csv2prv/out -n QH -C pyspc -t otamin18_fcst


Fichier de stations : data/_bin/csv2prv/in/list_fcst_otamin.txt

.. code-block:: text

   K1251810_2018010412_2001_brut
   K1251810_2018010412_2011_brut



:blue:`Conversion au format prv ('otamin18_trend') des données de débits ('QH') des séries listées dans le fichier data/_bin/csv2prv/in/list_trend_otamin.txt depuis le fichier csv de type 'pyspc' dans le répertoire data/core/csv vers le répertoire data/_bin/csv2prv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2prv.py -I data/core/csv -l data/_bin/csv2prv/in/list_trend_otamin.txt -O data/_bin/csv2prv/out -n QH -C pyspc -t otamin18_trend


Fichier de stations : data/_bin/csv2prv/in/list_trend_otamin.txt

.. code-block:: text

   K1251810_2018010412_2001_brut
   K1251810_2018010412_2001_brut_10
   K1251810_2018010412_2001_brut_50
   K1251810_2018010412_2001_brut_90



:blue:`Conversion au format prv ('scores_fcst') des données de débits ('QI') des séries listées dans le fichier data/_bin/csv2prv/in/list_fcst_scores.txt depuis le fichier csv de type 'pyspc' dans le répertoire data/core/csv vers le répertoire data/_bin/csv2prv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2prv.py -I data/core/csv -l data/_bin/csv2prv/in/list_fcst_scores.txt -O data/_bin/csv2prv/out -n QI -C pyspc -t scores_fcst


Fichier de stations : data/_bin/csv2prv/in/list_fcst_scores.txt

.. code-block:: text

   K1251810_2018010412_2001_brut
   K1251810_2018010412_2011_brut



:blue:`Conversion au format prv ('scores_obs') des données de débits ('QI') à la station RH10585x depuis le fichier csv de type GRP v2018 ('grp18') dans le répertoire data/model/grp18/cal vers le répertoire data/_bin/csv2prv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2prv.py -I data/model/grp18/cal -s RH10585x -O data/_bin/csv2prv/out -n QI -C grp18 -t scores_obs


:blue:`Conversion au format prv ('scores_obs') des données de débits ('QI') à la station K1341810 depuis le fichier csv de type 'pyspc' dans le répertoire data/core/csv vers le répertoire data/_bin/csv2prv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2prv.py -I data/core/csv -s K1341810 -O data/_bin/csv2prv/out -n QI -C pyspc -t scores_obs


:blue:`Conversion au format prv ('scores_sim') des données de débits ('QI') à la station K1321810_mohys depuis le fichier csv de type 'pyspc' dans le répertoire data/core/csv vers le répertoire data/_bin/csv2prv/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2prv.py -I data/core/csv -s K1321810_mohys -O data/_bin/csv2prv/out -n QI -C pyspc -t scores_sim



