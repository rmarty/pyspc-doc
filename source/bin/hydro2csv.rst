
.. fichier rst créé automatiquement par bindoc

.. _hydro2csv:

.. role:: blue

.. role:: boldblue

.. index:: hydro2csv


hydro2csv
=========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.hydro2csv
    :func: set_parser
    :prog: hydro2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir le fichier Hydro2 qtfix.txt situé dans le répertoire data/data/hydro2 au format csv de type 'grp16' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur QH.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d qtfix.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n QH -C grp16


:blue:`Convertir le fichier Hydro2 qtvar.txt situé dans le répertoire data/data/hydro2 au format csv de type 'grp18' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur QI.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d qtvar.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n QI -C grp18


:blue:`Convertir le fichier Hydro2 htemps.txt situé dans le répertoire data/data/hydro2 au format csv de type 'pyspc' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur HI.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d htemps.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n HI -C pyspc


:blue:`Convertir le fichier Hydro2 qjm.txt situé dans le répertoire data/data/hydro2 au format csv de type 'pyspc' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur QJ.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d qjm.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n QJ -C pyspc


:blue:`Convertir le fichier Hydro2 qtfix.txt situé dans le répertoire data/data/hydro2 au format csv de type 'pyspc' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur QH.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d qtfix.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n QH -C pyspc


:blue:`Convertir le fichier Hydro2 qtvar.txt situé dans le répertoire data/data/hydro2 au format csv de type 'pyspc' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur QI.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d qtvar.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n QI -C pyspc


:blue:`Convertir le fichier Hydro2 tousmois.txt situé dans le répertoire data/data/hydro2 au format csv de type 'pyspc' dans le répertoire data/_bin/hydro2csv/out. Les données correspondent à la grandeur QM.`

.. container:: cmdimg

   .. container:: cmdline

      hydro2csv.py -d tousmois.txt -I data/data/hydro2 -O data/_bin/hydro2csv/out -n QM -C pyspc



