
.. fichier rst créé automatiquement par bindoc

.. _csvInfo:

.. role:: blue

.. role:: boldblue

.. index:: csvInfo


csvInfo
=======

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.csvInfo
    :func: set_parser
    :prog: csvInfo
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Comparaison de la série K040301mvl lue dans le répertoire data/_bin/csvInfo/in avec la référence K0403010 lue dans data/_bin/csvInfo/in pour la grandeur QH. La comparaison est exportée dans le fichier data/_bin/csvInfo/out/K0403010_diff.txt`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QH -s K040301mvl -M diff K0403010 data/_bin/csvInfo/in -c data/_bin/csvInfo/out/K0403010_diff.txt


:blue:`Comparaison des séries listées dans data/_bin/csvInfo/in/Chambon.txt et lues dans le répertoire data/_bin/csvInfo/in pour la grandeur QH. La référence est la première listée dans data/_bin/csvInfo/in/Chambon.txt. La comparaison est exportée dans le fichier data/_bin/csvInfo/out/Chambon_diff.txt`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QH -l data/_bin/csvInfo/in/Chambon.txt -M diff -c data/_bin/csvInfo/out/Chambon_diff.txt


Fichier de stations : data/_bin/csvInfo/in/Chambon.txt

.. code-block:: text

   K0403010
   K040301mvl



:blue:`Etablir la liste des événements contenus dans la série K1251810 lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QJ. L'algorithme 'basic' est utilisé, en appliquant le seuil 60. L'analyse est exportée dans le fichier data/_bin/csvInfo/out/K1251810_evt.csv`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QJ -s K1251810 -M evt 60 -c data/_bin/csvInfo/out/K1251810_evt.csv


:blue:`Etablir la liste des événements contenus dans la série K0260010 lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QJ. L'algorithme scipy est utilisé, en appliquant le seuil 50 et les autres pamraètres (20 2 -7 7). L'analyse est exportée dans le fichier data/_bin/csvInfo/out/K0260010_evt.csv`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QJ -s K0260010 -M evt 50 scipy 20 2 -7 7 -c data/_bin/csvInfo/out/K0260010_evt.csv


:blue:`Extraction des maximas annuels de la série K1251810 lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QJ. L'analyse est exportée dans le fichier data/_bin/csvInfo/out/K1251810_max.csv`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QJ -s K1251810 -M max -c data/_bin/csvInfo/out/K1251810_max.csv


:blue:`Etablir la complétude des données de la série K040301mvl lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QH. L'analyse est exportée dans le fichier data/_bin/csvInfo/out/K040301mvl.csv`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QH -s K040301mvl -M mvl -c data/_bin/csvInfo/out/K040301mvl.csv


:blue:`Etablir la complétude des données de la série K040301mvl lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QH. Les données sont censurées à la période 2008110120 - 2008110222. L'analyse est exportée dans le fichier data/_bin/csvInfo/out/K040301mvl_sub.csv`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QH -s K040301mvl -M mvl -c data/_bin/csvInfo/out/K040301mvl_sub.csv -F 2008110120 -L 2008110222


:blue:`Etablir la complétude des données de la série K040301mvl lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QH. Les données manquantes en début et fin de série sont ignorées. L'analyse est exportée dans le fichier data/_bin/csvInfo/out/K040301mvs.csv`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QH -s K040301mvl -M mvs -c data/_bin/csvInfo/out/K040301mvs.csv


:blue:`Etablir le régime hydrologique de la série K1251810 lue dans le répertoire data/_bin/csvInfo/in pour la grandeur QJ. L'algorithme est utilisé, en appliquant les paramètres: 
- 'Groupe' = month
- 'Freqs' = 0.10,0.25,0.50,0.75,0.90
- 'Dayhour' = 6
- 'Strict' = False
- 'Echelle' = False
- 'Répertoire' = data/_bin/csvInfo/out
- 'Boxplot' = Oui
- 'Fill' = yes`

.. container:: cmdimg

   .. container:: cmdline

      csvInfo.py -I data/_bin/csvInfo/in -n QJ -s K1251810 -M reg month 0.10,0.25,0.50,0.75,0.90 6 False False data/_bin/csvInfo/out Oui yes



