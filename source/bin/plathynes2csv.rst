
.. fichier rst créé automatiquement par bindoc

.. _plathynes2csv:

.. role:: blue

.. role:: boldblue

.. index:: plathynes2csv


plathynes2csv
=============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.plathynes2csv
    :func: set_parser
    :prog: plathynes2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir le fichier PLATHYNES 8001_RRobs.mgr de type 'data' situé dans le répertoire data/model/plathynes au format csv de type 'grp18' dans le répertoire data/_bin/plathynes2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      plathynes2csv.py -O data/_bin/plathynes2csv/out -I data/model/plathynes -t data -d 8001_RRobs.mgr -C grp18


:blue:`Convertir le fichier PLATHYNES 8001_1.mqi de type 'data' situé dans le répertoire data/model/plathynes au format csv de type 'grp18' dans le répertoire data/_bin/plathynes2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      plathynes2csv.py -O data/_bin/plathynes2csv/out -I data/model/plathynes -t data -d 8001_1.mqi -C grp18


:blue:`Convertir le fichier PLATHYNES 8001_1.mqo de type 'data' situé dans le répertoire data/model/plathynes au format csv de type 'grp16' dans le répertoire data/_bin/plathynes2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      plathynes2csv.py -O data/_bin/plathynes2csv/out -I data/model/plathynes -t data -d 8001_1.mqo -C grp16


:blue:`Convertir le fichier PLATHYNES plathynes_export_1.txt de type 'export' situé dans le répertoire data/model/plathynes au format csv de type 'pyspc' dans le répertoire data/_bin/plathynes2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      plathynes2csv.py -O data/_bin/plathynes2csv/out -I data/model/plathynes -t export -d plathynes_export_1.txt -C pyspc


:blue:`Convertir le fichier PLATHYNES 8001_ResultsRaw.txt de type 'results' situé dans le répertoire data/model/plathynes au format csv de type 'pyspc' dans le répertoire data/_bin/plathynes2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      plathynes2csv.py -O data/_bin/plathynes2csv/out -I data/model/plathynes -t results -d 8001_ResultsRaw.txt -C pyspc -1



