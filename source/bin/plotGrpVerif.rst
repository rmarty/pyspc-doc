
.. fichier rst créé automatiquement par bindoc

.. _plotGrpVerif:

.. role:: blue

.. role:: boldblue

.. index:: plotGrpVerif


plotGrpVerif
============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.plotGrpVerif
    :func: set_parser
    :prog: plotGrpVerif
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Tracer les valeurs des critères de performance des fiches de calage de GRP dont la synthèse est située dans le fichier synthese.txt dans le répertoie data/model/grp16/cal. Les figures sont créées dans le fichier data/_bin/plotGrpVerif/out. La synthèse correspond aux expériences où le seuil de calage SC est dans la liste ['ref', 'DC50', 'DC90', 'DC95'] où l'horizon de calage HC est dans la liste ['ref'] où le seuil de vigilance SV est dans la liste ['ref', 'DC95'] où le MODE est dans la liste ['SMN_TAN', 'SMN_RNA']`

.. container:: cmdimg

   .. container:: cmdline

      plotGrpVerif.py -I data/model/grp16/cal -d synthese.txt -O data/_bin/plotGrpVerif/out -U SC ref DC50 DC90 DC95 -U HC ref -U SV ref DC95 -U MODE SMN_TAN SMN_RNA


.. figure:: ../_static/synthese_K0260010_HC-6.png
   :alt: Figure
   :figclass: figurecentre

   Figure synthese_K0260010_HC-6.png


.. figure:: ../_static/synthese_K0403010_HC-3.png
   :alt: Figure
   :figclass: figurecentre

   Figure synthese_K0403010_HC-3.png


.. figure:: ../_static/synthese_K0550010_HC-6.png
   :alt: Figure
   :figclass: figurecentre

   Figure synthese_K0550010_HC-6.png



