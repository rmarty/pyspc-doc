
.. fichier rst créé automatiquement par bindoc

.. _bdapbp2json:

.. role:: blue

.. role:: boldblue

.. index:: bdapbp2json


bdapbp2json
===========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.bdapbp2json
    :func: set_parser
    :prog: bdapbp2json
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraction des prévisions BP à la date 2020061206 au format long pour le domaine spatiale 41005 dans le répertoire local data/_bin/bdapbp2json/out. L'accès à BdApbp est défini au travers du fichier ../bin/bdapbp2json_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      bdapbp2json.py -O data/_bin/bdapbp2json/out -S 41005 -r 2020061206 -t long -c ../bin/bdapbp2json_RM.txt


:blue:`Extraction des prévisions BP à la date 2020061206 au format short pour le domaine spatiale 41005+41003 (2 zones) dans le répertoire local data/_bin/bdapbp2json/out. L'accès à BdApbp est défini au travers du fichier ../bin/bdapbp2json_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      bdapbp2json.py -O data/_bin/bdapbp2json/out -S 41005+41003 -r 2020061206 -t short -c ../bin/bdapbp2json_RM.txt



