
.. fichier rst créé automatiquement par bindoc

.. _duplicateGrpRTCfg:

.. role:: blue

.. role:: boldblue

.. index:: duplicateGrpRTCfg


duplicateGrpRTCfg
=================

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.duplicateGrpRTCfg
    :func: set_parser
    :prog: duplicateGrpRTCfg
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Dupliquer la configuration de référence de GRP v2016 ('grp16') du fichier Config_Prevision.txt dans le répertoire data/model/grp16/rt vers le fichier Config_Prevision_2016.txt dans le répertoire data/_bin/duplicateGrpRTCfg/out pour la balise MODFON valant Temps_diff, pour la balise INSTPR valant "2016-06-01 06:00:00".`

.. container:: cmdimg

   .. container:: cmdline

      duplicateGrpRTCfg.py -I data/model/grp16/rt -D Config_Prevision.txt -O data/_bin/duplicateGrpRTCfg/out -t grp16 -c Config_Prevision_2016.txt -U MODFON Temps_diff -U INSTPR "2016-06-01 06:00:00"


:blue:`Dupliquer la configuration de référence de GRP v2016 ('grp18') du fichier Config_Prevision.txt dans le répertoire data/model/grp18/rt vers le fichier Config_Prevision_2018.txt dans le répertoire data/_bin/duplicateGrpRTCfg/out pour la balise MODFON valant Temps_diff, pour la balise SIMULA valant 1.`

.. container:: cmdimg

   .. container:: cmdline

      duplicateGrpRTCfg.py -I data/model/grp18/rt -D Config_Prevision.txt -O data/_bin/duplicateGrpRTCfg/out -t grp18 -c Config_Prevision_2018.txt -U MODFON Temps_diff -U SIMULA 1


:blue:`Dupliquer la configuration de référence de GRP v2016 ('grp20') du fichier config_prevision.ini dans le répertoire data/model/grp20/rt vers le fichier config_prevision_2020.ini dans le répertoire data/_bin/duplicateGrpRTCfg/out pour la balise GENERAL_MODFON valant Temps_reel, pour la balise SORTIES_SIMULA valant NON.`

.. container:: cmdimg

   .. container:: cmdline

      duplicateGrpRTCfg.py -I data/model/grp20/rt -D config_prevision.ini -O data/_bin/duplicateGrpRTCfg/out -t grp20 -c config_prevision_2020.ini -U GENERAL_MODFON Temps_reel -U SORTIES_SIMULA NON



