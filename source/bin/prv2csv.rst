
.. fichier rst créé automatiquement par bindoc

.. _prv2csv:

.. role:: blue

.. role:: boldblue

.. index:: prv2csv


prv2csv
=======

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.prv2csv
    :func: set_parser
    :prog: prv2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir les données du format prv de type otamin16_fcst du fichier GRP_B_20200911_1515_DA_2.prv situé dans le répertoire data/model/otamin16 au format csv de type 'pyspc' dans le répertoire data/_bin/prv2csv/out. Un seul fichier de sortie est créé.`

.. container:: cmdimg

   .. container:: cmdline

      prv2csv.py -I data/model/otamin16 -d GRP_B_20200911_1515_DA_2.prv -O data/_bin/prv2csv/out -t otamin16_fcst -C pyspc -1


:blue:`Convertir les données du format prv de type otamin16_fcst du fichier GRP_B_20200911_1515_2.prv situé dans le répertoire data/model/otamin16 au format csv de type 'pyspc' dans le répertoire data/_bin/prv2csv/out. Un seul fichier de sortie est créé.`

.. container:: cmdimg

   .. container:: cmdline

      prv2csv.py -I data/model/otamin16 -d GRP_B_20200911_1515_2.prv -O data/_bin/prv2csv/out -t otamin16_fcst -C pyspc -1


:blue:`Convertir les données du format prv de type otamin16_trend du fichier GRP_B_20200203_1200_2.prv situé dans le répertoire data/model/otamin16 au format csv de type 'pyspc' dans le répertoire data/_bin/prv2csv/out. Un seul fichier de sortie est créé.`

.. container:: cmdimg

   .. container:: cmdline

      prv2csv.py -I data/model/otamin16 -d GRP_B_20200203_1200_2.prv -O data/_bin/prv2csv/out -t otamin16_trend -C pyspc -1


:blue:`Convertir les données du format prv de type scores_fcst du fichier K6373020_Q_9.csv situé dans le répertoire data/verification/scores au format csv de type 'pyspc' dans le répertoire data/_bin/prv2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      prv2csv.py -I data/verification/scores -d K6373020_Q_9.csv -O data/_bin/prv2csv/out -t scores_fcst -C pyspc


:blue:`Convertir les données du format prv de type scores_obs du fichier K6373020_Q.csv situé dans le répertoire data/verification/scores au format csv de type 'grp16' dans le répertoire data/_bin/prv2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      prv2csv.py -I data/verification/scores -d K6373020_Q.csv -O data/_bin/prv2csv/out -t scores_obs -C grp16


:blue:`Convertir les données du format prv de type scores_sim du fichier K6373020_Q_sim.csv situé dans le répertoire data/verification/scores au format csv de type 'pyspc' dans le répertoire data/_bin/prv2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      prv2csv.py -I data/verification/scores -d K6373020_Q_sim.csv -O data/_bin/prv2csv/out -t scores_sim -C pyspc



