
.. fichier rst créé automatiquement par bindoc

.. _bdimagexml2csv:

.. role:: blue

.. role:: boldblue

.. index:: bdimagexml2csv


bdimagexml2csv
==============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.bdimagexml2csv
    :func: set_parser
    :prog: bdimagexml2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir le fichier BdImage getPrevByNetworkStatsByPixels_sympo-t-t.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getPrevByNetworkStatsByPixels_sympo-t-t.xml -C pyspc -1


:blue:`Convertir le fichier BdImage getPrevByNetworkStatsByZones_sympo-rr-rr.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getPrevByNetworkStatsByZones_sympo-rr-rr.xml -C pyspc -1


:blue:`Convertir le fichier BdImage getPrevByNetworkStatsByZones_arome-rr-total.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getPrevByNetworkStatsByZones_arome-rr-total.xml -C pyspc -1


:blue:`Convertir le fichier BdImage getPrevByNetworkStatsByZones_arome-ifs-rr-total.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getPrevByNetworkStatsByZones_arome-ifs-rr-total.xml -C pyspc -1


:blue:`Convertir le fichier BdImage getPrevByNetworkStatsByZones_arome-pi-rr-total.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getPrevByNetworkStatsByZones_arome-pi-rr-total.xml -C pyspc


:blue:`Convertir le fichier BdImage getPrevByNetworkStatsByZones_arpege-t-t.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getPrevByNetworkStatsByZones_arpege-t-t.xml -C pyspc -1


:blue:`Convertir le fichier BdImage getObsStatsByPixels_sim-t-t.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getObsStatsByPixels_sim-t-t.xml -C pyspc -1


:blue:`Convertir le fichier BdImage getObsStatsByZones_antilope-j1-rr_010000_RATIO.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en spécifiant les ratios déclarant une image/stat manquante : {'ratio_image': 0.8, 'ratio_stats': 0.5}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getObsStatsByZones_antilope-j1-rr_010000_RATIO.xml -U ratio_image 0.8 -U ratio_stats 0.5 -C pyspc -1


:blue:`Convertir le fichier BdImage getObsValuesByBBox_antilope-j1-rr_010000.xml situé dans le répertoire data/data/lamedo au format csv de type 'pyspc' dans le répertoire data/_bin/bdimagexml2csv/out , en appliquant les ratios par défaut : {'ratio_image': 1, 'ratio_stats': 1}`

.. container:: cmdimg

   .. container:: cmdline

      bdimagexml2csv.py -O data/_bin/bdimagexml2csv/out -I data/data/lamedo -d getObsValuesByBBox_antilope-j1-rr_010000.xml -C pyspc -1



