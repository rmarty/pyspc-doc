
.. fichier rst créé automatiquement par bindoc

.. _cristal2xmlSandre:

.. role:: blue

.. role:: boldblue

.. index:: cristal2xmlSandre


cristal2xmlSandre
=================

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.cristal2xmlSandre
    :func: set_parser
    :prog: cristal2xmlSandre
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les données de la station K0550010 depuis les archives CRISTAL situé dans le répertoire data/data/cristal au format xml Sandre dans le répertoire data/_bin/cristal2xmlSandre/out. L'extraction concerne la période du 2008103118 au 2008110200. Le scénario xml Sandre correspond aux informations utilisateur: {'sender': 'me', 'user': 'org', 'target': 'you'}`

.. container:: cmdimg

   .. container:: cmdline

      cristal2xmlSandre.py -I data/data/cristal -O data/_bin/cristal2xmlSandre/out -F 2008103118 -L 2008110200 -s K0550010 -U sender me -U user org -U target you


:blue:`Extraire les données des stations listés dans data/_bin/cristal2xmlSandre/in/pluvios.txt depuis les archives CRISTAL situé dans le répertoire data/data/cristal au format xml Sandre dans le répertoire data/_bin/cristal2xmlSandre/out. L'extraction concerne la période du 2008103118 au 2008110200. Le scénario xml Sandre correspond aux informations utilisateur: {'sender': 'me', 'user': 'org', 'target': 'you'}`

.. container:: cmdimg

   .. container:: cmdline

      cristal2xmlSandre.py -I data/data/cristal -O data/_bin/cristal2xmlSandre/out -F 2008103118 -L 2008110200 -l data/_bin/cristal2xmlSandre/in/pluvios.txt


Fichier de stations : data/_bin/cristal2xmlSandre/in/pluvios.txt

.. code-block:: text

   K0559910




