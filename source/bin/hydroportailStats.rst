
.. fichier rst créé automatiquement par bindoc

.. _hydroportailStats:

.. role:: blue

.. role:: boldblue

.. index:: hydroportailStats


hydroportailStats
=================

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.hydroportailStats
    :func: set_parser
    :prog: hydroportailStats
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Récupérer des statistiques hydrologiques (DEBCLA) établies par Hydroportail pour le lieu None et enregistrer les fichiers csv dans le répertoire data/_bin/hydroportailStats/out. L'adresse de l'Hydroportail est renseignée dans le fichier data/_bin/hydroportailStats/cfg/config.txt`

.. container:: cmdimg

   .. container:: cmdline

      hydroportailStats.py -l data/_bin/hydroportailStats/cfg/siteshydro.txt -t DEBCLA -c data/_bin/hydroportailStats/cfg/config.txt -O data/_bin/hydroportailStats/out -v


Fichier de stations : data/_bin/hydroportailStats/cfg/siteshydro.txt

.. code-block:: text

   K1321810
   K1341810



:blue:`Récupérer des statistiques hydrologiques (Q-X) établies par Hydroportail pour le lieu K1251810 et enregistrer les fichiers csv dans le répertoire data/_bin/hydroportailStats/out. L'adresse de l'Hydroportail est renseignée dans le fichier data/_bin/hydroportailStats/cfg/config.txt`

.. container:: cmdimg

   .. container:: cmdline

      hydroportailStats.py -s K1251810 -t Q-X -c data/_bin/hydroportailStats/cfg/config.txt -O data/_bin/hydroportailStats/out -v


Fichier de stations : data/_bin/hydroportailStats/cfg/siteshydro.txt

.. code-block:: text

   K1321810
   K1341810




