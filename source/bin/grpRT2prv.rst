
.. fichier rst créé automatiquement par bindoc

.. _grpRT2prv:

.. role:: blue

.. role:: boldblue

.. index:: grpRT2prv


grpRT2prv
=========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.grpRT2prv
    :func: set_parser
    :prog: grpRT2prv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir les prévisions de type grp16_rt_fcst_diff situées dans le répertoire data/model/grp16/rt au format prv de type 'otamin16_fcst' dans le répertoire data/_bin/grpRT2prv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2prv.py -I data/model/grp16/rt -O data/_bin/grpRT2prv/out -t grp16_rt_fcst_diff otamin16_fcst -U Previ 45gGRPd000


:blue:`Convertir les prévisions de type grp16_rt_fcst_diff situées dans le répertoire data/model/grp16/rt au format prv de type 'scores_fcst' dans le répertoire data/_bin/grpRT2prv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2prv.py -I data/model/grp16/rt -O data/_bin/grpRT2prv/out -t grp16_rt_fcst_diff scores_fcst -U Previ 45gGRPd000


:blue:`Convertir les prévisions de type grp18_rt_fcst_diff situées dans le répertoire data/model/grp18/rt au format prv de type 'otamin18_fcst' dans le répertoire data/_bin/grpRT2prv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2prv.py -I data/model/grp18/rt -O data/_bin/grpRT2prv/out -t grp18_rt_fcst_diff otamin18_fcst -U Previ 45gGRPd000


:blue:`Convertir les prévisions de type grp18_rt_fcst_diff situées dans le répertoire data/model/grp18/rt au format prv de type 'scores_fcst' dans le répertoire data/_bin/grpRT2prv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2prv.py -I data/model/grp18/rt -O data/_bin/grpRT2prv/out -t grp18_rt_fcst_diff scores_fcst -U Previ 45gGRPd000



