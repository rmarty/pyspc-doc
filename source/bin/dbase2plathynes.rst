
.. fichier rst créé automatiquement par bindoc

.. _dbase2plathynes:

.. role:: blue

.. role:: boldblue

.. index:: dbase2plathynes


dbase2plathynes
===============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.dbase2plathynes
    :func: set_parser
    :prog: dbase2plathynes
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les données nécessaires à PLATHYNES depuis les bases de données de type sacha : ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'], situées dans le répertoire data/_bin/dbase2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/dbase2plathynes/plathynes: la station K0030020 est une station d'injection. Tous les événements sont considérés. Les codes stations suivent la convention Hydro3`

.. container:: cmdimg

   .. container:: cmdline

      dbase2plathynes.py -I data/_bin/dbase2plathynes/in -d plathynes_debitpluie.mdb plathynes_pluie.mdb -t sacha -O data/_bin/dbase2plathynes/plathynes -c plathynes_project.prj -s K0030020 -3


:blue:`Extraire les données nécessaires à PLATHYNES depuis les bases de données de type sacha : ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'], situées dans le répertoire data/_bin/dbase2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/dbase2plathynes/plathynes: la station K0030020 est une station d'injection. Seul l'événement 201911 est considéré. Les codes stations suivent la convention Hydro3`

.. container:: cmdimg

   .. container:: cmdline

      dbase2plathynes.py -I data/_bin/dbase2plathynes/in -d plathynes_debitpluie.mdb plathynes_pluie.mdb -t sacha -O data/_bin/dbase2plathynes/plathynes -c plathynes_project.prj -s K0030020 -S 201911 -3


:blue:`Extraire les données nécessaires à PLATHYNES depuis les bases de données de type sacha : ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'], situées dans le répertoire data/_bin/dbase2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/dbase2plathynes/plathynes: la station K0030020 est une station d'injection. Tous les événements sont considérés. Les codes stations suivent la convention Hydro3. Seules les données 'P' sont exportées`

.. container:: cmdimg

   .. container:: cmdline

      dbase2plathynes.py -I data/_bin/dbase2plathynes/in -d plathynes_debitpluie.mdb plathynes_pluie.mdb -t sacha -O data/_bin/dbase2plathynes/plathynes -c plathynes_project.prj -s K0030020 -n P -3


:blue:`Extraire les données nécessaires à PLATHYNES depuis les bases de données de type sacha : ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'], situées dans le répertoire data/_bin/dbase2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/dbase2plathynes/plathynes: la station K0030020 est une station d'injection. Tous les événements sont considérés. Les codes stations suivent la convention Hydro3. Seules les données 'Q' sont exportées`

.. container:: cmdimg

   .. container:: cmdline

      dbase2plathynes.py -I data/_bin/dbase2plathynes/in -d plathynes_debitpluie.mdb plathynes_pluie.mdb -t sacha -O data/_bin/dbase2plathynes/plathynes -c plathynes_project.prj -s K0030020 -n Q -3


:blue:`Extraire les données nécessaires à PLATHYNES depuis les bases de données de type sacha : ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'], situées dans le répertoire data/_bin/dbase2plathynes/in. Le projet PLATHYNES est plathynes_project.prj dans le répertoire data/_bin/dbase2plathynes/plathynes: la station None est une station d'injection. Tous les événements sont considérés. Les codes stations suivent la convention Hydro3`

.. container:: cmdimg

   .. container:: cmdline

      dbase2plathynes.py -I data/_bin/dbase2plathynes/in -d plathynes_debitpluie.mdb plathynes_pluie.mdb -t sacha -O data/_bin/dbase2plathynes/plathynes -c plathynes_project.prj -l data/_bin/dbase2plathynes/in/list_injection.txt -3


Fichier de stations : data/_bin/dbase2plathynes/in/list_injection.txt

.. code-block:: text

   K0030020




