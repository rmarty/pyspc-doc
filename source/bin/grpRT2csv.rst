
.. fichier rst créé automatiquement par bindoc

.. _grpRT2csv:

.. role:: blue

.. role:: boldblue

.. index:: grpRT2csv


grpRT2csv
=========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.grpRT2csv
    :func: set_parser
    :prog: grpRT2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir les données d'archive de GRP v2016 (grp16_rt_archive) du fichier PV_10A.DAT situé dans le répertoire data/model/grp16/rt au format csv de type 'grp16' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp16/rt -O data/_bin/grpRT2csv/out -d PV_10A.DAT -t grp16_rt_archive -C grp16


:blue:`Convertir les données d'archive de GRP v2018 (grp18_rt_archive) du fichier QV_10A.DAT situé dans le répertoire data/model/grp18/rt au format csv de type 'grp18' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp18/rt -O data/_bin/grpRT2csv/out -d QV_10A.DAT -t grp18_rt_archive -C grp18


:blue:`Convertir les données d'observation de GRP v2016 (grp16_rt_data) du fichier Debit.txt situé dans le répertoire data/model/grp16/rt au format csv de type 'grp16' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp16/rt -O data/_bin/grpRT2csv/out -d Debit.txt -t grp16_rt_data -C grp16


:blue:`Convertir les données d'observation de GRP v2018 (grp18_rt_data) du fichier Pluie_00J01H00M.txt situé dans le répertoire data/model/grp18/rt au format csv de type 'grp18' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp18/rt -O data/_bin/grpRT2csv/out -d Pluie_00J01H00M.txt -t grp18_rt_data -C grp18


:blue:`Convertir les prévisions hydro de GRP v2016 (grp16_rt_fcst_diff) du fichier GRP_D_Prev_2001.txt situé dans le répertoire data/model/grp16/rt au format csv de type 'pyspc' dans le répertoire data/_bin/grpRT2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp16/rt -O data/_bin/grpRT2csv/out -d GRP_D_Prev_2001.txt -t grp16_rt_fcst_diff -C pyspc -1


:blue:`Convertir les prévisions hydro de GRP v2018 (grp18_rt_fcst_diff) du fichier GRP_D_Prev_2001.txt situé dans le répertoire data/model/grp18/rt au format csv de type 'pyspc' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp18/rt -O data/_bin/grpRT2csv/out -d GRP_D_Prev_2001.txt -t grp18_rt_fcst_diff -C pyspc


:blue:`Convertir les données internes de GRP v2016 (grp16_rt_intern_diff) du fichier PQE_1A_D.DAT situé dans le répertoire data/model/grp16/rt/intern au format csv de type 'pyspc' dans le répertoire data/_bin/grpRT2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp16/rt/intern -O data/_bin/grpRT2csv/out -d PQE_1A_D.DAT -t grp16_rt_intern_diff -C pyspc -1


:blue:`Convertir les données internes de GRP v2018 (grp18_rt_intern_diff) du fichier PQE_1A_D.DAT situé dans le répertoire data/model/grp18/rt/intern au format csv de type 'pyspc' dans le répertoire data/_bin/grpRT2csv/out. Un seul fichier est créé.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp18/rt/intern -O data/_bin/grpRT2csv/out -d PQE_1A_D.DAT -t grp18_rt_intern_diff -C pyspc -1


:blue:`Convertir les scénarios météo de GRP v2016 (grp16_rt_data) du fichier Scen_006_PluMA.txt situé dans le répertoire data/model/grp16/rt au format csv de type 'pyspc' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp16/rt -O data/_bin/grpRT2csv/out -d Scen_006_PluMA.txt -t grp16_rt_data -C pyspc


:blue:`Convertir les scénarios météo de GRP v2018 (grp18_rt_metscen) du fichier Scen_001_PluRR_00J01H00M.txt situé dans le répertoire data/model/grp18/rt au format csv de type 'pyspc' dans le répertoire data/_bin/grpRT2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      grpRT2csv.py -I data/model/grp18/rt -O data/_bin/grpRT2csv/out -d Scen_001_PluRR_00J01H00M.txt -t grp18_rt_metscen -C pyspc



