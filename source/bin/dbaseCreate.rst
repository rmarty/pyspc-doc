
.. fichier rst créé automatiquement par bindoc

.. _dbaseCreate:

.. role:: blue

.. role:: boldblue

.. index:: dbaseCreate


dbaseCreate
===========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.dbaseCreate
    :func: set_parser
    :prog: dbaseCreate
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Créer une base vierge de type previ19 dans le répertoire data/_bin/dbaseCreate/out, l'utilisateur ayant défini le nom de la base vierge previ2019_empty.mdb`

.. container:: cmdimg

   .. container:: cmdline

      dbaseCreate.py -O data/_bin/dbaseCreate/out -t previ19 -d previ2019_empty.mdb


:blue:`Créer une base de type previ19 dans le répertoire data/_bin/dbaseCreate/out, l'utilisateur ayant défini le nom de la base previ19_models.mdb. Celle-ci est complétée par les informations contenues dans le fichier data/_bin/dbaseCreate/in/previ19_models.txt.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseCreate.py -O data/_bin/dbaseCreate/out -t previ19 -d previ19_models.mdb -c data/_bin/dbaseCreate/in/previ19_models.txt


:blue:`Créer une base vierge de type sacha dans le répertoire data/_bin/dbaseCreate/out avec le nom par défaut (sacha.mdb)`

.. container:: cmdimg

   .. container:: cmdline

      dbaseCreate.py -O data/_bin/dbaseCreate/out -t sacha


:blue:`Créer une base vierge de type sacha dans le répertoire data/_bin/dbaseCreate/out, l'utilisateur ayant défini le nom de la base vierge mydbase_sacha.mdb`

.. container:: cmdimg

   .. container:: cmdline

      dbaseCreate.py -O data/_bin/dbaseCreate/out -d mydbase_sacha.mdb -t sacha



