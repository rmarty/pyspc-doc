
.. fichier rst créé automatiquement par bindoc

.. _csv2mf:

.. role:: blue

.. role:: boldblue

.. index:: csv2mf


csv2mf
======

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.csv2mf
    :func: set_parser
    :prog: csv2mf
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Conversion au format Publithèque ('data') des données de précipitations horaires ('PH') à la station 43091005 depuis le fichier csv de type GRP v2016 ('grp16') dans le répertoire data/model/grp16/cal vers le répertoire data/_bin/csv2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2mf.py -I data/model/grp16/cal -O data/_bin/csv2mf/out -n PH -C grp16 -t data -s 43091005


:blue:`Conversion au format Publithèque ('data') des données de températures horaires ('TH') à la station 43091005 depuis le fichier csv de type GRP v2016 ('grp16') dans le répertoire data/model/grp16/cal vers le répertoire data/_bin/csv2mf/out. Les données sont restreintes à la période 2017061314 à 2017061318.`

.. container:: cmdimg

   .. container:: cmdline

      csv2mf.py -I data/model/grp16/cal -O data/_bin/csv2mf/out -n TH -C grp16 -t data -s 43091005 -F 2017061314 -L 2017061318


:blue:`Conversion au format Publithèque ('data') des données de précipitations horaires ('PH') aux stations listées dans data/_bin/csv2mf/in/list_grp18.txt depuis le fichier csv de type GRP v2018 ('grp18') dans le répertoire data/model/grp18/cal vers le répertoire data/_bin/csv2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2mf.py -I data/model/grp18/cal -O data/_bin/csv2mf/out -n PH -C grp18 -t data -l data/_bin/csv2mf/in/list_grp18.txt -w


Fichier de stations : data/_bin/csv2mf/in/list_grp18.txt

.. code-block:: text

   90035001
   90065003



:blue:`Conversion au format Publithèque ('data') des données de précipitations 6-min ('P6m') de la station 43111002 depuis le fichier csv de type 'pyspc' dans le répertoire data/core/csv vers le répertoire data/_bin/csv2mf/out`

.. container:: cmdimg

   .. container:: cmdline

      csv2mf.py -I data/core/csv -O data/_bin/csv2mf/out -n P6m -C pyspc -t data -s 43111002 -w



