
.. fichier rst créé automatiquement par bindoc

.. _xmlSandre2csv:

.. role:: blue

.. role:: boldblue

.. index:: xmlSandre2csv


xmlSandre2csv
=============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.xmlSandre2csv
    :func: set_parser
    :prog: xmlSandre2csv
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Convertir le fichier datafcst_hydro.xml du format XML Sandre de type ('data_fcst_hydro') situé dans le répertoire data/data/sandre au format csv de type 'pyspc' dans le répertoire data/_bin/xmlSandre2csv/out. Les séries sont écrites dans un seul fichier.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d datafcst_hydro.xml -t data_fcst_hydro -O data/_bin/xmlSandre2csv/out -C pyspc -1


:blue:`Convertir le fichier dataobs_hydro_Q.xml du format XML Sandre de type ('data_obs_hydro') situé dans le répertoire data/data/sandre au format csv de type 'grp16' dans le répertoire data/_bin/xmlSandre2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d dataobs_hydro_Q.xml -t data_obs_hydro -O data/_bin/xmlSandre2csv/out -C grp16


:blue:`Convertir le fichier dataobs_hydro_H.xml du format XML Sandre de type ('data_obs_hydro') situé dans le répertoire data/data/sandre au format csv de type 'pyspc' dans le répertoire data/_bin/xmlSandre2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d dataobs_hydro_H.xml -t data_obs_hydro -O data/_bin/xmlSandre2csv/out -C pyspc


:blue:`Convertir le fichier dataobs_meteo.xml du format XML Sandre de type ('data_obs_meteo') situé dans le répertoire data/data/sandre au format csv de type 'grp16' dans le répertoire data/_bin/xmlSandre2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d dataobs_meteo.xml -t data_obs_meteo -O data/_bin/xmlSandre2csv/out -C grp16


:blue:`Convertir le fichier dataobs_meteo.xml du format XML Sandre de type ('data_obs_meteo') situé dans le répertoire data/data/sandre au format csv de type 'grp18' dans le répertoire data/_bin/xmlSandre2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d dataobs_meteo.xml -t data_obs_meteo -O data/_bin/xmlSandre2csv/out -C grp18


:blue:`Convertir le fichier levelcor.xml du format XML Sandre de type ('levelcor') situé dans le répertoire data/data/sandre au format csv de type 'pyspc' dans le répertoire data/_bin/xmlSandre2csv/out.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d levelcor.xml -t levelcor -O data/_bin/xmlSandre2csv/out -C pyspc


:blue:`Convertir le fichier spcmo.xml du format XML Sandre de type ('data_fcst_hydro') situé dans le répertoire data/data/sandre au format csv de type 'pyspc' dans le répertoire data/_bin/xmlSandre2csv/out. Les séries sont écrites dans un seul fichier. L'extraction concerne uniquement le site Y2100020 pour les modèles-scénarios suivants: [['model', '11gGRPd130'], ['scen', 'ctl']].`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandre2csv.py -I data/data/sandre -d spcmo.xml -t data_fcst_hydro -O data/_bin/xmlSandre2csv/out -s Y2100020 -U model 11gGRPd130 -U scen ctl -C pyspc -1



