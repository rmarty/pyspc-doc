
.. _listsitepackage:

Bibliothèques tierces
---------------------

Le paquet *pySPC* requiert les modules suivants pour profiter de l'ensemble de ces fonctionnalités:

-  `argparse <https://docs.python.org/3/library/argparse.html>`__: bibliothèque fournie avec la distribution basique.
-  `collections <https://docs.python.org/3/library/collections.html>`__: bibliothèque fournie avec la distribution basique.
-  `configparser <https://docs.python.org/3/library/configparser.html>`__: bibliothèque fournie avec la distribution basique.
-  `ctypes <https://docs.python.org/3/library/ctypes.html>`__: bibliothèque fournie avec la distribution basique.
-  `datetime <https://docs.python.org/3/library/datetime.html>`__: bibliothèque fournie avec la distribution basique.
-  `difflib <https://docs.python.org/3/library/difflib.html>`__: bibliothèque fournie avec la distribution basique.
-  `io <https://docs.python.org/3/library/io.html>`__: bibliothèque fournie avec la distribution basique.
-  `itertools <https://docs.python.org/3/library/itertools.html>`__: bibliothèque fournie avec la distribution basique.
-  `json <https://docs.python.org/3/library/json.html>`__: bibliothèque fournie avec la distribution basique.
-  `libbdimage <https://gitlab.com/vigicrues/lamedo/bdimage3>`__: bibliothèque développée par Vigicrues (mini: 1.4.1 ou 1.5.8 downgradé: à demander (voir section Contact)).
-  `libhydro <https://gitlab.com/vigicrues/hydro3/libhydro>`__: bibliothèque développée par Vigicrues (mini: 0.9.2).
-  `lxml <http://lxml.de/>`__: bibliothèque destinée à traiter les fichiers XML et HTML. **Requise par libhydro3**
-  `matplotlib <https://pypi.python.org/pypi/matplotlib/>`__: bibliothèque destinée à créer des figures
-  `numpy <https://pypi.python.org/pypi/numpy/>`__: bibliothèque destinée à réaliser des calculs numériques
-  `os <https://docs.python.org/3/library/os.html>`__: bibliothèque fournie avec la distribution basique.
-  `pandas <https://pypi.org/project/pandas/>`__: bibliothèque destinée à manipuler des séries temporelles
-  `pdfminer <https://pypi.python.org/pypi/pdfminer3k>`__: dans sa version **python3**
-  `pyodbc <https://pypi.python.org/pypi/pyodbc/>`__: bibliothèque destinée à manipuler les bases de données de type Access
-  `PyQt4 <https://pypi.python.org/pypi/PyQt4>`__ ou `PyQt5 <https://pypi.python.org/pypi/PyQt5>`__: bibliothèque destinée à créer des interfaces graphiques en python, PyQt4 (à partir de la version 2.0.0) ou PyQt5 (à partir de la version 2.1.2)
-  `requests <https://pypi.org/project/requests/>`__: bibliothèque destinée à gérer le téléchargement de données par le protocole HTTP
-  `scipy <http://www.scipy.org/>`__: bibliothèque destinée à réaliser des calculs numériques
-  `socket <https://docs.python.org/3/library/socket.html>`__: bibliothèque fournie avec la distribution basique.
-  `subprocess <https://docs.python.org/3/library/subprocess.html>`__: bibliothèque fournie avec la distribution basique.
-  `suds <https://pypi.org/project/suds-jurko/>`__: bibliothèque destinée à se connecter à la PHyC
-  `sys <https://docs.python.org/3/library/sys.html>`__: bibliothèque fournie avec la distribution basique.
-  `urllib.request <https://docs.python.org/3/library/urllib.request.html>`__: bibliothèque fournie avec la distribution basique. **urllib2 dans Python 2**
-  `urllib.error <https://docs.python.org/3/library/urllib.error.html>`__: bibliothèque fournie avec la distribution basique. **urllib2 dans Python 2**
-  `warnings <https://docs.python.org/3/warnings.html>`__: bibliothèque fournie avec la distribution basique.
-  `xml <https://docs.python.org/3/library/xml.etree.elementtree.html>`__: bibliothèque fournie avec la distribution basique.
-  `xlrd <https://pypi.org/project/xlrd/>`__: bibliothèque destinée à **lire** les fichiers XLS(X).
-  `xlwt <https://pypi.org/project/xlwt/>`__: bibliothèque destinée à **écrire** les fichiers XLS(X).

.. warning: Des tests réalisés en mai 2021 ont montré que la version 1.4.1 de libbdimage est incompatible avec les images antilope 15’ (antilope france-t[r,d]-[15,60]mn). Les modifications suivantes ont été apportées afin de permettre les requêtes de ces nouvelles images, ainsi que leur lecture. Un patch est disponible sur demande.

Vous pouvez tester si les paquets sont présents en ouvrant une console Python

.. container:: cmdimg

   .. container:: cmdline
   
      python

et en important les modules, un par un. Voici, par exemple, ce qu'il faut écrire pour importer le paquet *argparse*:

.. code-block:: python
   
   import argparse

Si un message d'erreur apparaît lors de l'import de l'un de ces modules, il faut l'installer manuellement.

.. seealso:: Les bibliothèques tierces non-installées par la distribution basique de Python, et nécessaires pour le bon fonctionnement de fonctions et de méthodes sont précisées dans :ref:`api`.

