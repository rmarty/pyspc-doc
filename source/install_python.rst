
.. _pyinstall:

Installation de python
----------------------

Il est recommandé d'utiliser `Miniconda3 <https://docs.conda.io/en/latest/miniconda.html>`__ pour installer Python et gérer les dépendances entre les bibliothèques complémentaires à installer.

.. _miniconda:

Miniconda
+++++++++

Les programmes du paquet *pySPC* requiert l'installation de Python 3 (ou supérieur). 

.. warning:: En raison de la rupture de comptabilité avec Python 2, il est impossible d'utiliser les Versions 2.x et antérieures.

La distribution `Miniconda3 <https://docs.conda.io/en/latest/miniconda.html>`__ permet d'installer python 3 et offre un moyen facile d'installer des bibliothèques supplémentaires. Elle permet également de disposer de plusieurs environnements *python* avec leur configuration propres (Versions majeures/mineures différentes, Versions de bibliothèques différentes), en plus de l'environnement par défaut, grâce à la bibliothèque `virtualenv <https://pypi.python.org/pypi/virtualenv/>`__

.. warning:: Il est conseillé d'installer la version 32 bits. La version 64 présente des instabilités lorsque la bibliothèque tierce `pyodbc <https://pypi.python.org/pypi/pyodbc/>`__ est utilisée

.. note:: *Miniconda3* installe la dernière Version de *Python 3* comme environnement par défaut. *Miniconda2* installe, quant à lui, la dernière Version de *Python 2*.

Pour installer *Miniconda3*, il faut:

1. Télécharger l'exécutable sur ce `site <https://docs.conda.io/en/latest/miniconda.html>`__, en veillant a bien sélectionner le bon fichier qui correspond à votre système d'exploitation et à la Version *Python* que vous souhaitez par défaut.

2. Exécuter l'exécutable téléchargé

3. Configurer les variables d'environnement *http\_proxy* et *https\_proxy* si vous êtes sur un réseau particulier, afin de permettre à *Miniconda* d'accéder aux serveurs contenant les installeurs de bibliothèques et de python.

.. warning:: Ceci peut impacter d'autres programmes. Si vous ne souhaitez pas perturber ces autres programmes, vous pouvez définir cette variable d'environnement lors de l'appel au programme *conda*

   .. container:: cmdimg

      .. container:: cmdline
      
         set http_proxy=http://(host):(port) && conda xxxx

.. _envs:
         
Environnements complémentaires
++++++++++++++++++++++++++++++

Si vous souhaitez créer un environnement spécifique pour pySPC, évitant ainsi toute interaction incorrecte avec d'autres projets, vous pouvez créer un environnement complémentaire, isolé des environnements existants, en lançant la commande suivante:

.. container:: cmdimg

   .. container:: cmdline

      conda create -n name python=X

où *name* est le nom de l'environnement ainsi défini, et *X* la Version de python voulue pour cet environnement. Vous pouvez aussi spécifier une Version particulière d'une bibliothèque *lib* lors de la construction de l'environnement

.. container:: cmdimg

   .. container:: cmdline
   
      conda create -n name python=X lib=Y

où *Y* est la Version de la biliothèque *lib* voulue. Pour connaître les informations et bibliothèques installées au sein d'un environnement, il suffit de lancer la commande

.. container:: cmdimg

   .. container:: cmdline
   
      conda list -n name

Ici, name peut contenir *root* pour accéder à l'information de l'environnement par défaut. Pour passer d'un environnement préalablement créé, il suffit de lancer la commande

.. container:: cmdimg

   .. container:: cmdline
   
      call activate name

où *name* est le nom de l'environnement. Pour retourner dans l'environnement par défaut, il faut lancer la commande

.. container:: cmdimg

   .. container:: cmdline
   
      call deactivate name

.. warning:: Si vous créez un environnement spécifique pour ce module, il est nécessaire de modifier le fichier *pySPC.bat* afin d'y ajouter une commande ``call activate name`` avant le lancement de python et une commande ``call deactivate`` en fin du bat. Ici, *name* est à remplacer par le nom de votre environnement dédié.
      
.. _sitepackage:

Bibliothèques tierces
+++++++++++++++++++++

pySPC requiert un certain nombre de :ref:`listsitepackage`. Les dépendances, nécessitant une compilation, sont à installer préalablement avec Miniconda, par exemple à l'aide du fichier de requirements: 

.. container:: cmdimg

   .. container:: cmdline
   
      conda install --file requirements.txt

.. note:: Le fichier requirements.txt est disponible dans le répertoire resources/install/ du projet pySPC.


Si vous souhaitez créer un environnement dédié et installer les bibliothèques tierces en même temps, vous pouvez suivre la commande suivante:

.. container:: cmdimg

   .. container:: cmdline
   
      conda env create -f environment.yml

.. note:: Le fichier environment.yml est disponible dans le répertoire resources/install/ du projet pySPC.


Si vous souhaitez compléter votre installationm en installant une bibliothèque *lib*, il faut simplement lancer la commande suivante:

.. container:: cmdimg

   .. container:: cmdline
   
      conda install lib

Si la bibliothèque *lib* requiert d'autres bibliothèques, *Miniconda* se charge de les installer en même temps.

.. warning:: Il est possible que certaines bibliothèques ne soient pas reconnues par *Miniconda*. C'est notamment le cas de pdfminer3k. Dans ce cas, il faut suivre l'une des procédures décrites dans :ref:`classicinstall`.
