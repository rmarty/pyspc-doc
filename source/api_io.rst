
.. currentmodule:: pyspc.io

Lecture et conversion des données
=================================


Bareme
------

.. autosummary::
    :toctree: api/

    bareme.read_Bareme


Cristal
-------

.. autosummary::
    :toctree: api/

    cristal.read_Cristal


CSV - XLS - PyspcFile
---------------------

.. autosummary::
    :toctree: api/

    csv.read_csv
    csv.read_xls
    csv.write_csv
    csv.write_xls
    pyspcfile.read_PyspcFile
    pyspcfile.write_PyspcFile



Data Base
---------------------

.. autosummary::
    :toctree: api/

    dbase.dbase
    dbase.mdb
    dbase.sdb
    

GRP
---

.. autosummary::
    :toctree: api/

    grp16.read_GRP16
    grp16.write_GRP16
    grp18.read_GRP18
    grp18.write_GRP18
    grp20.read_GRP20
    grp20.write_GRP20
    grp22.read_GRP22
    grp22.write_GRP22


Hydro2
------

.. autosummary::
    :toctree: api/

    hydro2.read_Hydro2


Lamedo
------

.. autosummary::
    :toctree: api/

    lamedo.read_BdApbp
    lamedo.write_BdApbp
    lamedo.read_BdImage


Météo-France
------------

.. autosummary::
    :toctree: api/

    meteofrance.read_MF_Data
    meteofrance.write_MF_Data
    meteofrance.read_MF_OpenAPI
    meteofrance.read_MF_OpenData
    meteofrance.read_BP
    meteofrance.read_Sympo


MORDOR
------

.. autosummary::
    :toctree: api/

    mordor.read_Mordor
    mordor.write_Mordor


PLATHYNES
---------

.. autosummary::
    :toctree: api/

    plathynes.read_Plathynes
    plathynes.write_Plathynes


PREMHYCE
--------

.. autosummary::
    :toctree: api/

    premhyce.read_Premhyce


Prévision du SPC Loire-Allier-Cher-Indre
----------------------------------------

.. autosummary::
    :toctree: api/

    prevision.read_Prevision14
    prevision.read_Prevision17
    prevision.read_Prevision19
    prevision.write_Prevision19



Format PRV (OTAMIN, SCORES)
---------------------------

.. autosummary::
    :toctree: api/

    prv.read_prv
    prv.write_prv


Référentiel du SPC Loire-Allier-Cher-Indre
------------------------------------------

.. autosummary::
    :toctree: api/

    refspc.read_RefSPC

Sacha
-----

.. autosummary::
    :toctree: api/

    sacha.read_Sacha


Sandre (XML)
------------

.. autosummary::
    :toctree: api/

    sandre.read_Sandre
    sandre.write_Sandre


Vigicrues
---------

.. autosummary::
    :toctree: api/

    vigicrues.read_Vigicrues

XML
---

.. autosummary::
    :toctree: api/

    xml.from_xml
