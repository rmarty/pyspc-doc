# -*- coding: utf-8 -*-
"""
Created on Mon Jan 14 19:47:34 2019

@author: rmarty

NE RIEN METTRE DIRECTEMENT DEDANS (i.e. ne pas faire comme dans un script)
"""
import glob
import os.path
import sys

from pyspc.binutils.gui.tree import TREE_CONTENT
MODE_DEBUG = False

BIN_DIRNAME = os.path.join('source', 'bin')
BIN_DIRNAME2 = 'bin'
INC_DIRNAME = os.path.join('source', 'inc')
INC_DIRNAME2 = os.path.join('..', 'inc')

TITLE_LINES = {
    0: '=',
    1: '-',
    2: '+',
    3: '*',
    4: '~'
}
SYMB_LIST = {
    1: '-',
    2: '   +',
    3: '      *',
    4: '         -',
    5: '            +',
    6: '               *'
}


def process_bin(binary=None):
    if binary is None:
        return None
    filename = os.path.join(BIN_DIRNAME, '{}.rst'.format(binary))
    txt = """
.. fichier rst créé automatiquement par bindoc

.. _{binary}:

.. role:: blue

.. role:: boldblue

.. index:: {binary}


{binary}
{line}

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.{binary}
    :func: set_parser
    :prog: {binary}
    :nodefaultconst:

""".format(binary=binary, line=TITLE_LINES[0]*len(binary))
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(txt)
        # EXAMPLE
        incbasename = '{}_example.rst'.format(binary)
        incfilename = os.path.join(INC_DIRNAME, incbasename)
        if os.path.exists(incfilename):
            f.write('\n')
            write_included_file(
                filename=incfilename,
                stdout=f
            )
            f.write('\n')
            f.write('\n')
        else:
            print('TODO TODO TODO => XXXXXX_example.rst')


def process_subtree(stdout=None, subtree=None, level=None):
    if stdout is None or subtree is None or level is None:
        return None
    for k in sorted(subtree):
        if k in ['title', 'icon']:
            continue
        print('   ' * (level - 1) + k)
        if 'title' in subtree[k] and k.endswith('.py'):
            title = subtree[k]['title']
            binary = k.replace('.py', '')
            stdout.write('   bin/{}\n'.format(binary))
            process_bin(binary=binary)
            if MODE_DEBUG:
                break
        elif 'title' in subtree[k]:
            title = subtree[k]['title']
            stdout.write('\n')
            stdout.write('\n')
            if level == 2:
                stdout.write('{}'.format(title))
                stdout.write('\n')
                stdout.write('{}'.format(
                    ''.join([TITLE_LINES[level] * len(title)])))
            elif level == 3:
                stdout.write(':boldblue:`{}`'.format(title))
            elif level == 4:
                stdout.write(':blue:`{} : {}`'.format(title, k))
            stdout.write('\n')
            stdout.write('\n')
            test_newtoc = any([x.endswith('.py') for x in subtree[k].keys()])
            if test_newtoc:
                stdout.write('\n')
                stdout.write('.. toctree::\n')
                stdout.write('   :maxdepth: 2\n')
                stdout.write('   :titlesonly:\n')
                stdout.write('\n')

            process_subtree(stdout=stdout, subtree=subtree[k], level=level+1)
            if MODE_DEBUG:
                break


def write_included_file(filename=None, stdout=None):
    if stdout is None or not os.path.exists(filename):
        return None
    with open(filename, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            stdout.write(line)


def write_bin_main(filename=None):
    if filename is None:
        return None
    with open(filename, 'w', encoding='utf-8') as f:
        # =====================================================================
        # TITRE 1
        # =====================================================================
        f.write('.. fichier rst créé automatiquement par bindoc\n\n')
        f.write('.. _usage:\n\n')
        f.write('Utilisation par ligne de commande\n')
        f.write('=================================\n')
        f.write('\n')
        f.write('.. role:: blue\n')
        f.write('\n')
        f.write('.. role:: boldblue\n')
        f.write('\n')
        f.write('.. index:: Ligne de commande\n')
        f.write('\n')
        if not sys.path[0].endswith('pyspc'):
            return None
        # =====================================================================
        # MESSAGE GENERIQUE
        # =====================================================================
        f.write('\n')
        f.write("Vous trouverez ci-dessous les utilitaires disponibles dans "
                "pySPC, organisés par grande thématique du panel d'activités "
                "du modélisateur \n")
        f.write('\n')
        f.write('\n')
        f.write(".. note:: Chaque utilitaire peut être exécuté via une "
                "interface graphique, voir :ref:`gui`. "
                "Les utilitaires y sont organisés de la même façon.\n")
        f.write('\n')
        # =====================================================================
        # RENVOI VERS PAGE CONVENTION POUR OPTIONS F/L
        # =====================================================================
        f.write('\n')
        f.write(".. seealso:: Si l'utilitaire fait référence à des dates, "
                "vous pouvez obtenir leur description en cherchant "
                "parmi les :ref:`descdate`.\n")
        f.write('\n')
        # =====================================================================
        # RENVOI VERS PAGE CONVENTION POUR OPTIONS C/D/c
        # =====================================================================
        f.write('\n')
        f.write(".. seealso:: Si l'utilitaire fait référence à des fichiers, "
                "vous pouvez obtenir leur description en cherchant "
                "parmi les :ref:`descfile`.\n")
        f.write('\n')
        # =====================================================================
        # RENVOI VERS PAGE CONVENTION POUR OPTIONS N
        # =====================================================================
        f.write('\n')
        f.write(".. seealso:: Si l'utilitaire fait référence à des grandeurs, "
                "vous pouvez obtenir leur description en cherchant "
                "parmi les :ref:`descvar`.\n")
        f.write('\n')
        # =====================================================================
        # RENVOI VERS PAGE CONVENTION POUR OPTIONS T
        # =====================================================================
        f.write('\n')
        f.write(".. seealso:: Si l'utilitaire fait référence à des types de "
                "données, vous pouvez obtenir leur description en cherchant, "
                "parmi les :ref:`apiconvention`, les variables souvent "
                "nommées DATATYPES, DATANAMES, ou assimilées.\n")
        f.write('\n')
#        f.write('\n')
#        f.write(".. note:: L'ajout de la description des types de données de "
#                "chaque utilitaire est prévue pour une version ultérieure.\n")
#        f.write('\n')
        # =====================================================================
        # TRAITEMENT DES SOUS MODULES
        # =====================================================================
        for k1 in sorted(TREE_CONTENT):
            title1 = TREE_CONTENT[k1]['title']
            print(title1)
            f.write('\n')
            f.write('\n')
            f.write('{}'.format(title1))
            f.write('\n')
            f.write('{}'.format(''.join(['-' * len(title1)])))
            f.write('\n')
            f.write('\n')
            process_subtree(stdout=f, subtree=TREE_CONTENT[k1], level=2)
            if MODE_DEBUG:
                break


def process_gui_subtree(stdout=None, subtree=None, level=None):
    if stdout is None or subtree is None or level is None:
        return None
    for k in sorted(subtree):
        if k in ['title', 'icon']:
            continue
        if 'title' in subtree[k] and k.endswith('.py'):
            title = subtree[k]['title']
            binary = k.replace('.py', '')
#            stdout.write('\n')
#            stdout.write('\n')
            stdout.write('{} :boldblue:`{}.py` (:blue:`{}`)'
                         ''.format(SYMB_LIST[level], binary, title))
#            stdout.write('\n')
            stdout.write('\n')
            if MODE_DEBUG:
                break
        elif 'title' in subtree[k]:
            title = subtree[k]['title']
            icon = subtree[k]['icon']
#            stdout.write('\n')
            stdout.write('\n')
            stdout.write('{}'.format(SYMB_LIST[level]))
            stdout.write(' .. image:: _static/{}\n'.format(icon))
            stdout.write(' ' * (4+len(SYMB_LIST[level])))
            stdout.write(':height: 20px\n')
            stdout.write(' ' * (4+len(SYMB_LIST[level])))
            stdout.write(':alt: alternate text\n')
            stdout.write(' ' * (4+len(SYMB_LIST[level])))
            stdout.write(':align: left\n')
            stdout.write('\n')
            stdout.write(' ' * (1+len(SYMB_LIST[level])))
            stdout.write(':boldblue:`{}` (:blue:`{}`)'
                         ''.format(k, title))
            stdout.write('\n')
            stdout.write('\n')
            process_gui_subtree(
                stdout=stdout, subtree=subtree[k], level=level+1)
            if MODE_DEBUG:
                break


def write_gui_tree(filename=None, level=None):
    if filename is None:
        return None
    if level is None:
        return None
    with open(filename, 'w', encoding='utf-8') as f:
        # =====================================================================
        # TITRE 1
        # =====================================================================
        f.write('.. fichier rst créé automatiquement par bindoc\n\n')
        f.write('\n')
        f.write('.. role:: blue\n')
        f.write('\n')
        f.write('.. role:: boldblue\n')
        f.write('\n')
        print(sys.path[0])
        if not sys.path[0].endswith('pyspc'):
            return None
        # =====================================================================
        # TRAITEMENT DES SOUS MODULES
        # =====================================================================
        for k1 in sorted(TREE_CONTENT):
            title1 = TREE_CONTENT[k1]['title']
            icon = TREE_CONTENT[k1]['icon']
            f.write('\n')
            f.write('\n')
            f.write('{}'.format(SYMB_LIST[level]))
            f.write(' .. image:: _static/{}\n'.format(icon))
            f.write('     :height: 20px\n')
            f.write('     :alt: alternate text\n')
            f.write('     :align: left\n')
            f.write('\n')
            f.write('  :boldblue:`{}` (:blue:`{}`)'
                    ''.format(k1, title1))
            f.write('\n')
            process_gui_subtree(
                stdout=f, subtree=TREE_CONTENT[k1], level=level+1)
            if MODE_DEBUG:
                break


def setup(app):
    # Nettoyage du dossier BIN_DIRNAME
    for filename in glob.glob(os.path.join(BIN_DIRNAME, '*.rst')):
        try:
            os.remove(filename)
        except OSError:
            pass
    # Ecriture de l'arborescence BIN
    filename = os.path.join(app.confdir, 'userguide_bin.rst')
    write_bin_main(filename)
    # Ecriture de l'arborescence GUI
    filename = os.path.join(app.confdir, 'userguide_guitree.rst')
    write_gui_tree(filename, level=1)

    return None
