
.. _classicinstall:

API de pyspc
------------

Il est possible de n'installer que l'API du projet pySPC. Voici deux procédures:

.. _classicpython:

Python
++++++

Il suffit de récupérer l'archive depuis le dépôt `Bitbucket <https://bitbucket.org/rmarty/pyspc/downloads/>`_ puis de faire

.. container:: cmdimg

   .. container:: cmdline
   
      python setup.py install


.. _classicpip:

Utilisation de pip
++++++++++++++++++

Les dépendances sont installées automatiquement par `pip <https://pypi.org/project/pip/>`__ (si vous l'avez déjà installé). Si vous souhaitez compléter votre installationm en installant une bibliothèque *lib*, il faut simplement lancer la commande suivante:

.. container:: cmdimg

   .. container:: cmdline
   
      pip install lib
      
.. note:: Pour connaître les dépendances installables par `pip <https://pypi.org/project/pip/>`__

`pip <https://pypi.org/project/pip/>`__ permet d'installer la librarie directement depuis Bitbucket en indiquant un numéro de révision ou tout autre identificateur standard utilisé par Git.

Exemple pour installer la révision la plus récente de la branche stable:

.. container:: cmdimg

   .. container:: cmdline
   
      pip install https://bitbucket.org/rmarty/pyspc/get/master.zip
      
Exemple pour installer la version (le tag) 2.2.0:

.. container:: cmdimg

   .. container:: cmdline
   
      pip install https://bitbucket.org/rmarty/pyspc/get/v2.2.0.zip
