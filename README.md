pySPC-doc ![icon](https://bitbucket.org/rmarty/pyspc-doc/raw/master/resources/icons/pyspc-doc.png)
===============================================================================

Description
-------------------------------------------------------------------------------

Ce projet contient la documentation du module [**pySPC**](https://bitbucket.org/rmarty/pyspc/src)

La documentation est accessible sur le site [**readthedocs.io**](https://pyspc-doc.readthedocs.io/fr/latest/)
[![Documentation](https://readthedocs.org/projects/pyspc-doc/badge/?version=latest)](https://pyspc-doc.readthedocs.io/fr/latest/?badge=latest)

License
-------------------------------------------------------------------------------

Se reporter au fichier COPYING.txt.

Contact
-------------------------------------------------------------------------------

Renaud Marty <renaud.marty@developpement-durable.gouv.fr>
