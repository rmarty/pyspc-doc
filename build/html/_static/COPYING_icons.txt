ICONS COPYING

api.png : from noun_modules_948753 (creatd by mikicon)
build.png : from noun_1468316_cc (creatd by Danil Polshin)
checklist.png (original filename : noun_166498_cc.png) : Created by Aha-Soft from Noun Project
cmd.png (original filename : noun_187083_cc.png) : Created by Dalpat Prajapati from Noun Project
cristal : from noun_765131_cc (creatd by ProSymbols) and noun_1203668_cc (creatd by Alexander Skowalsky)
data.png (original filename : noun_28306_cc.png) : Created by Ilsur Aptukov from Noun Project
database.png (original filename : noun_15201_cc.png) : Created by Cees dce Vries from Noun Project
description.png (original filename : noun_Tag_2041573.png) : Created by visual langage
evaluation.png (original filename : noun_4188.png) : Created by Noun Project
figure.png (original filename : noun_175719_cc.png) : Created by Creative Stall from Noun Project
gui.png : from noun_gamepad_138424 : Created by Arthur Shlain
help.png (original filename : noun_208829_cc.png) : Created by Gonzalo Bravo from Noun Project
help2.png (original filename : noun_38009_cc.png) : Created by dw from Noun Project
index.png (original filename : noun_215531_cc.png) : Created by Arthur Shlain from Noun Project
irstea_data : from noun_1184297_cc (creatd by Ralf Schmitzer) and irstea (see below)
irstea_otamin : from noun_30978_cc (creatd by Icon Solid) and noun_175736_cc (creatd by Creative Stall) and irstea (see below)
irstea_rtime : from noun_6732_cc (creatd by Richard de Vos) and irstea (see below)
irstea_rtime_cfg : from noun_6732_cc (creatd by Richard de Vos) and noun_181348_cc (creatd by Guilherme Simoes) and irstea (see below)
irstea_rtime_data : from noun_6732_cc (creatd by Richard de Vos) and noun_291667_cc (creatd by Prasad) and irstea (see below)
irstea_verification : from noun_733351_cc (creatd by AlfredoCreates.com) and irstea (see below)
modhydro.png (original filename : noun_15616_cc.png) : Created by Andrew J. Young from Noun Project
run.png : from noun_925427_cc (creatd by Ananth)
schapi_database : from noun_15201_cc (creatd by Cees de Vries) and schapi (see below)
schapi_raster : from Qgis_mActionAddRasterLayer and schapi (see below)
schapi_scores : from noun_733351_cc (creatd by AlfredoCreates.com) and schapi (see below)
schapi_vector : from Qgis_mActionAddOgrLayer and schapi (see below)
startingpoint.png (original filename : noun_204609_cc.png) : Created by Sofi Ovchinnikova from Noun Project
stop.png : from noun_425106_cc (creatd by Mateus Bolsoni)
tree.png (original filename : noun_222460_cc.png) : Created by icon 54 from Noun Project
tree2.png : from noun_Tree view_532875 (created by icon 54)

audio_on.png (original filename : noun_1390787_cc.png) : Created by Ben Avery from Noun Project
audio_off.png (original filename : noun_1390793_cc.png) : Created by Ben Avery from Noun Project

back.png (original filename : noun_1442976_cc.png) : Created by Deemak Daksina S from Noun Project
formard.png (original filename : noun_1442938_cc.png) : Created by Deemak Daksina S from Noun Project
initial.png (original filename : noun_1442973_cc.png) : Created by Deemak Daksina S from Noun Project
reload.png (original filename : noun_74444_cc.png) : Created by Rohith M S from Noun Project
webbrowser.png (original filename : noun_955626_cc.png) : Created by Adrien Coquet from Noun Project

irstea.png : copyright IRSTEA (France)
meteo_france.png : copyright Météo-France
python.png : copyright Python Software Foundation
schapi.png : copyright Ministère de l'Ecologie, du Développement Durable et de l'Energie (France)
spc_lci.png : copyright Dreal Centre-Val de Loire (France)
