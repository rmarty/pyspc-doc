
.. fichier rst créé automatiquement par bindoc

.. _xmlSandreInfo:

.. role:: blue

.. role:: boldblue

.. index:: xmlSandreInfo


xmlSandreInfo
=============

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.xmlSandreInfo
    :func: set_parser
    :prog: xmlSandreInfo
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les méta-données de type 'loc_hydro' issues de la PHyC contenues dans le fichier CapteurHydro.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/CapteurHydro.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d CapteurHydro.xml -t loc_hydro -c data/_bin/xmlSandreInfo/out/CapteurHydro.csv


:blue:`Extraire les méta-données de type 'loc_hydro' issues de la PHyC contenues dans le fichier SiteHydro.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/SiteHydro.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d SiteHydro.xml -t loc_hydro -c data/_bin/xmlSandreInfo/out/SiteHydro.csv


:blue:`Extraire les méta-données de type 'loc_hydro' issues de la PHyC contenues dans le fichier SiteHydro_child.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/SiteHydro_child.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d SiteHydro_child.xml -t loc_hydro -c data/_bin/xmlSandreInfo/out/SiteHydro_child.csv


:blue:`Extraire les méta-données de type 'loc_hydro' issues de la PHyC contenues dans le fichier StationHydro.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/StationHydro.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d StationHydro.xml -t loc_hydro -c data/_bin/xmlSandreInfo/out/StationHydro.csv


:blue:`Extraire les méta-données de type 'loc_hydro' issues de la PHyC contenues dans le fichier StationHydro_child.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/StationHydro_child.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d StationHydro_child.xml -t loc_hydro -c data/_bin/xmlSandreInfo/out/StationHydro_child.csv


:blue:`Extraire les méta-données de type 'loc_hydro' issues de la PHyC contenues dans le fichier ZoneHydro_child.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/ZoneHydro_child.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d ZoneHydro_child.xml -t loc_hydro -c data/_bin/xmlSandreInfo/out/ZoneHydro_child.csv


:blue:`Extraire les méta-données de type 'loc_meteo' issues de la PHyC contenues dans le fichier SiteMeteo.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/SiteMeteo.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d SiteMeteo.xml -t loc_meteo -c data/_bin/xmlSandreInfo/out/SiteMeteo.csv


:blue:`Extraire les méta-données de type 'user' issues de la PHyC contenues dans le fichier 404_user_admin.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/404_user_admin.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d 404_user_admin.xml -t user -c data/_bin/xmlSandreInfo/out/404_user_admin.csv


:blue:`Extraire les méta-données de type 'user' issues de la PHyC contenues dans le fichier 404_user_loc_meteo.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/404_user_loc_meteo.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d 404_user_loc_meteo.xml -t user -c data/_bin/xmlSandreInfo/out/404_user_loc_meteo.csv


:blue:`Extraire les méta-données de type 'user' issues de la PHyC contenues dans le fichier 404_user_site_hydro.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/404_user_site_hydro.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d 404_user_site_hydro.xml -t user -c data/_bin/xmlSandreInfo/out/404_user_site_hydro.csv


:blue:`Extraire les méta-données de type 'user' issues de la PHyC contenues dans le fichier 404_user_station_hydro.xml au format xml Sandre situé dans le répertoire data/metadata/sandre. Ces informations sont écrites dans le fichier data/_bin/xmlSandreInfo/out/404_user_station_hydro.csv.`

.. container:: cmdimg

   .. container:: cmdline

      xmlSandreInfo.py -I data/metadata/sandre -d 404_user_station_hydro.xml -t user -c data/_bin/xmlSandreInfo/out/404_user_station_hydro.csv



