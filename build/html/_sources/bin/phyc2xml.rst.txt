
.. fichier rst créé automatiquement par bindoc

.. _phyc2xml:

.. role:: blue

.. role:: boldblue

.. index:: phyc2xml


phyc2xml
========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.phyc2xml
    :func: set_parser
    :prog: phyc2xml
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Récupérer des prévisions hydrologiques (data_fcst_hydro) stockées en PHyC pour le lieu K4350010, la grandeur QH, la période du 20190101 au 20190103 et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s K4350010 -t data_fcst_hydro -F 20190101 -L 20190103 -n QH -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


:blue:`Récupérer des observations hydrologiques (data_obs_hydro) stockées en PHyC pour le lieu K0550010, la grandeur QH, la période du 20141101 au 20141110 et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s K0550010 -t data_obs_hydro -F 20141101 -L 20141110 -n QH -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


:blue:`Récupérer des observations météorologiques (data_obs_meteo) stockées en PHyC pour les lieux listés dans data/_bin/phyc2xml/in/list_pluvio.txt, la grandeur PH, la période du 20141101 au 20141110 et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -l data/_bin/phyc2xml/in/list_pluvio.txt -t data_obs_meteo -F 20141101 -L 20141110 -n PH -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


Fichier de stations : data/_bin/phyc2xml/in/list_pluvio.txt

.. code-block:: text

   43091005
   43130002



:blue:`Récupérer des courbes de correction (levelcor) stockées en PHyC pour le lieu K055001010, la grandeur None, la période du 20080101 au 20181231 et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s K055001010 -t levelcor -F 20080101 -L 20181231 -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


:blue:`Récupérer des informations sur les lieux hydrométriques (loc_hydro) stockées en PHyC pour le lieu K0550010, et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s K0550010 -t loc_hydro -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


:blue:`Récupérer des informations sur les lieux hydrométriques (loc_hydro_child) stockées en PHyC pour le lieu K025, et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s K025 -t loc_hydro_child -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


:blue:`Récupérer des informations sur les lieux météorologiques (loc_meteo) stockées en PHyC pour les lieux listés dans data/_bin/phyc2xml/in/list_pluvio.txt et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -l data/_bin/phyc2xml/in/list_pluvio.txt -t loc_meteo -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


Fichier de stations : data/_bin/phyc2xml/in/list_pluvio.txt

.. code-block:: text

   43091005
   43130002



:blue:`Récupérer des courbes de tarage (ratingcurve) stockées en PHyC pour le lieu K055001010, la grandeur None, la période du 20080101 au 20131231 et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s K055001010 -t ratingcurve -F 20080101 -L 20131231 -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out


:blue:`Récupérer des informations utilisateur (user) stockées en PHyC pour l'utilisateur 398 et enregistrer le fichier xml Sandre dans le répertoire data/_bin/phyc2xml/out. L'adresse de la PHyC et les identifiants de la connexion sont renseignés dans le fichier ../bin/phyc2xml_RM.txt`

.. container:: cmdimg

   .. container:: cmdline

      phyc2xml.py -s 398 -t user -c ../bin/phyc2xml_RM.txt -O data/_bin/phyc2xml/out



