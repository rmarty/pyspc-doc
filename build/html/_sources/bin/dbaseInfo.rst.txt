
.. fichier rst créé automatiquement par bindoc

.. _dbaseInfo:

.. role:: blue

.. role:: boldblue

.. index:: dbaseInfo


dbaseInfo
=========

Usage
-----

.. argparse::
    :module: pyspc.binutils.args.dbaseInfo
    :func: set_parser
    :prog: dbaseInfo
    :nodefaultconst:



Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Extraire les méta-données de type 'loc_hydro' des éléments listés dans data/_bin/dbaseInfo/in/liste_hydro2.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/loc_hydro2.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/loc_hydro2.csv -l data/_bin/dbaseInfo/in/liste_hydro2.txt -t loc_hydro -2


Fichier de stations : data/_bin/dbaseInfo/in/liste_hydro2.txt

.. code-block:: text

   K0100020
   K0100030
   K0260020
   K0260030



:blue:`Extraire les méta-données de type 'loc_hydro' des éléments listés dans data/_bin/dbaseInfo/in/liste_capteur.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/loc_hydro_capteur.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/loc_hydro_capteur.csv -l data/_bin/dbaseInfo/in/liste_capteur.txt -t loc_hydro -3


Fichier de stations : data/_bin/dbaseInfo/in/liste_capteur.txt

.. code-block:: text

   K01000201001
   K01000201001
   K05500101001
   K05500101002



:blue:`Extraire les méta-données de type 'loc_hydro' des éléments listés dans data/_bin/dbaseInfo/in/liste_site.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/loc_hydro_site.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/loc_hydro_site.csv -l data/_bin/dbaseInfo/in/liste_site.txt -t loc_hydro -3


Fichier de stations : data/_bin/dbaseInfo/in/liste_site.txt

.. code-block:: text

   K0010020
   K0114020
   K0550010



:blue:`Extraire les méta-données de type 'loc_hydro' des éléments listés dans data/_bin/dbaseInfo/in/liste_station.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/loc_hydro_station.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/loc_hydro_station.csv -l data/_bin/dbaseInfo/in/liste_station.txt -t loc_hydro -3


Fichier de stations : data/_bin/dbaseInfo/in/liste_station.txt

.. code-block:: text

   K010002010
   K026001002
   K055001010



:blue:`Extraire les méta-données de type 'loc_meteo' des éléments listés dans data/_bin/dbaseInfo/in/liste_meteo.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/loc_meteo.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/loc_meteo.csv -l data/_bin/dbaseInfo/in/liste_meteo.txt -t loc_meteo -3


Fichier de stations : data/_bin/dbaseInfo/in/liste_meteo.txt

.. code-block:: text

   07235005
   43042002
   43091005



:blue:`Extraire les méta-données de type 'reach' des éléments listés dans data/_bin/dbaseInfo/in/liste_reach.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/reach.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/reach.csv -l data/_bin/dbaseInfo/in/liste_reach.txt -t reach -3


Fichier de stations : data/_bin/dbaseInfo/in/liste_reach.txt

.. code-block:: text

   LC105



:blue:`Extraire les méta-données de type 'reach' des éléments listés dans data/_bin/dbaseInfo/in/liste_reach.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/reach2.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/reach2.csv -l data/_bin/dbaseInfo/in/liste_reach.txt -t reach -2


Fichier de stations : data/_bin/dbaseInfo/in/liste_reach.txt

.. code-block:: text

   LC105



:blue:`Extraire les méta-données de type 'stat_hydro' de l'élément K0260010 issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/stat_hydro.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/stat_hydro.csv -s K0260010 -t stat_hydro -3


:blue:`Extraire les méta-données de type 'stat_meteo' des éléments listés dans data/_bin/dbaseInfo/in/liste_meteo.txt issues du référentiel SPC contenues dans le fichier BDD_light.sqlite situé dans le répertoire data/metadata/refspc. Ces informations sont écrites dans le fichier data/_bin/dbaseInfo/out/stat_meteo.csv.`

.. container:: cmdimg

   .. container:: cmdline

      dbaseInfo.py -I data/metadata/refspc -d BDD_light.sqlite -c data/_bin/dbaseInfo/out/stat_meteo.csv -l data/_bin/dbaseInfo/in/liste_meteo.txt -t stat_meteo -3


Fichier de stations : data/_bin/dbaseInfo/in/liste_meteo.txt

.. code-block:: text

   07235005
   43042002
   43091005




