.. fichier rst créé automatiquement par scriptdoc

.. _script_index:

Index des scripts
-----------------

.. role:: blue

.. role:: boldblue

.. index:: Ligne de commande

.. index:: Script



- :blue:`Télécharger les données BDImage` : `_bdimage2grp_download.py <script/_bdimage2grp_download.html>`_



- :blue:`Extraire les données BDImage et les stocker au format csv de GRP` : `_bdimage2grp_extract.py <script/_bdimage2grp_extract.html>`_



- :blue:`Calcul des ETP à partir de séries de température horaires, les latitudes des stations et selon la pondération du bassin` : `_grpCal_computeETP.py <script/_grpCal_computeETP.html>`_



- :blue:`Extension des données ETP des bassins GRP` : `_grpCal_extendETP.py <script/_grpCal_extendETP.html>`_



- :blue:`Extraction de données de GRP TEMPS REEL et conversion au format GRP Calage` : `_grpCal_fromRT.py <script/_grpCal_fromRT.html>`_



- :blue:`Calage automatique de GRP v3.1` : `_grpCal_run31.py <script/_grpCal_run31.html>`_



- :blue:`Calage automatique de GRP-16r1571` : `_grpCal_run33.py <script/_grpCal_run33.html>`_



- :blue:`Création de la base de données GRP à partir des données SACHA` : `_grpCal_sacha2grp.py <script/_grpCal_sacha2grp.html>`_



- :blue:`Calcul des lames d'eau des bassins GRP` : `_grpCal_wav.py <script/_grpCal_wav.html>`_



- :blue:`Exécution de GRP Temps-Réel, application d'OTAMIN et mise en base de prévision` : `_grpRT_fcst31.py <script/_grpRT_fcst31.html>`_



- :blue:`Script gérant le lancement en temps différé de GRP Temps Réel (16r1571)` : `_grpRT_fcst33.py <script/_grpRT_fcst33.html>`_



- :blue:`Télécharger les rapports/bulletins/fiches MF` : `_mfReports.py <script/_mfReports.html>`_



- :blue:`Création d'un pdf multi-page et de fichiers png des courbes hypso` : `_plotHypso.py <script/_plotHypso.html>`_



- :blue:`Script post-événement : récupération des lieux hydro` : `_postEvent_000_locations.py <script/_postEvent_000_locations.html>`_



- :blue:`Script post-événement : complément des sous-groupes par le fichier loc_hydro` : `_postEvent_001_subreach_bylochydro.py <script/_postEvent_001_subreach_bylochydro.html>`_



- :blue:`Script post-événement : complément des sous-groupes par GRP` : `_postEvent_002_subreach_bygrp.py <script/_postEvent_002_subreach_bygrp.html>`_



- :blue:`Script post-événement : export Hydro2` : `_postEvent_100_hydro2_export.py <script/_postEvent_100_hydro2_export.html>`_



- :blue:`Script post-événement : téléchargement des données depuis la PHyC` : `_postEvent_110_phyc_download.py <script/_postEvent_110_phyc_download.html>`_



- :blue:`Script post-événement : conversion des données issues la PHyC` : `_postEvent_111_phyc_convert.py <script/_postEvent_111_phyc_convert.html>`_



- :blue:`Script post-événement : conversion au format MF des pluvios hors-MF de la PHyC` : `_postEvent_112_phyc_cristaledf2mfdata.py <script/_postEvent_112_phyc_cristaledf2mfdata.html>`_



- :blue:`Script post-événement : téléchargement des données depuis LAMEDO` : `_postEvent_120_lamedo_download.py <script/_postEvent_120_lamedo_download.html>`_



- :blue:`Script post-événement : conversion des données issues de LAMEDO` : `_postEvent_121_lamedo_convert.py <script/_postEvent_121_lamedo_convert.html>`_



- :blue:`Script post-événement : conversion des données issues de LAMEDO` : `_postEvent_130_mf_convert.py <script/_postEvent_130_mf_convert.html>`_



- :blue:`Script post-événement : changement de pas de temps` : `_postEvent_140_regularscale.py <script/_postEvent_140_regularscale.html>`_



- :blue:`Script post-événement : déterminer les maxis (H et Q) et les tps de retour (Q)` : `_postEvent_150_subreach_stats.py <script/_postEvent_150_subreach_stats.html>`_



- :blue:`Script post-événement : déterminer les maxis (H et Q) et les tps de retour (Q)` : `_postEvent_151_subreach_plot.py <script/_postEvent_151_subreach_plot.html>`_



- :blue:`Script post-événement : tracer les prévisions BP` : `_postEvent_200_bp_plot.py <script/_postEvent_200_bp_plot.html>`_



- :blue:`Script post-événement : conversion des données obs en fichier d'entrée GRP` : `_postEvent_210_grp_inputs.py <script/_postEvent_210_grp_inputs.html>`_



- :blue:`Script post-événement : Lancement de GRP en Temps Diff et conversion en csv` : `_postEvent_211_grp_run.py <script/_postEvent_211_grp_run.html>`_



- :blue:`Script post-événement : Tracer la figure de l'événement vu par GRP` : `_postEvent_212_grp_plotevent.py <script/_postEvent_212_grp_plotevent.html>`_



- :blue:`Script post-événement : Lancement de MOHYS en SIMULATION et conversion en csv` : `_postEvent_220_mohys_sim.py <script/_postEvent_220_mohys_sim.html>`_



- :blue:`Script post-événement : Lancement de MOHYS en PREVISION et conversion en csv` : `_postEvent_221_mohys_run.py <script/_postEvent_221_mohys_run.html>`_



- :blue:`Script post-événement : tracer les prévisions opérationnelles par lieu/runtime` : `_postEvent_230_rawfcst_plot.py <script/_postEvent_230_rawfcst_plot.html>`_



- :blue:`Script post-événement : tracer les prévisions oper. validées par lieu/runtime` : `_postEvent_231_validfcst_plot.py <script/_postEvent_231_validfcst_plot.html>`_



- :blue:`Script post-événement : tracer les prévisions oper. validées par lieu` : `_postEvent_232_validfcst_plotevent.py <script/_postEvent_232_validfcst_plotevent.html>`_



- :blue:`Script post-événement : synthèse des modèles et outils de validation` : `_postEvent_233_validfcst_describe.py <script/_postEvent_233_validfcst_describe.html>`_



- :blue:`Script post-événement : calculer les critères de vérification des prv brutes` : `_postEvent_240_rawfcst_verification.py <script/_postEvent_240_rawfcst_verification.html>`_



- :blue:`Script post-événement : calculer les critères de vérification des prv validées` : `_postEvent_241_validfcst_verification.py <script/_postEvent_241_validfcst_verification.html>`_



- :blue:`Script post-événement : calculer les critères de vérification des prv rejouées` : `_postEvent_242_refcst_verification.py <script/_postEvent_242_refcst_verification.html>`_



- :blue:`Script post-événement : tracer les critères de vérification` : `_postEvent_243_verif_plot.py <script/_postEvent_243_verif_plot.html>`_



- :blue:`Script post-événement : tracer les critères de vérification` : `_postEvent_244_verif_plotuncertainty.py <script/_postEvent_244_verif_plotuncertainty.html>`_

