.. fichier rst créé automatiquement par scriptdoc

:orphan:


_grpRT_fcst33.py
================


.. role:: blue

.. role:: boldblue

.. index:: _grpRT_fcst33.py

Script gérant le lancement en temps différé de GRP Temps Réel (16r1571)

``_grpRT_fcst33.py``



.. rubric:: Configuration des periodes de calcul


:boldblue:`FIRST_DT` : :blue:`Premier pas de temps à considérer`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps à considérer`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 pas de temps`

:boldblue:`LTIME_DT` : :blue:`Horizon maximal des prévisions`


.. rubric:: Arborescence


:boldblue:`HOME` : :blue:`Répertoire de travail`


.. rubric:: Arborescence grp temps reel


:boldblue:`GRP_HOME` : :blue:`Répertoire principal`

:boldblue:`GRP_CFG` : :blue:`Répertoire de la configuration`

:boldblue:`GRP_CFG_FILENAME` : :blue:`Fichier de la configuration`

:boldblue:`GRP_CFG_BACKUP` : :blue:`Archive du fichier de la configuration`

:boldblue:`GRP_EXE_FILENAME` : :blue:`Exécutable GRP`

:boldblue:`GRP_IN` : :blue:`Répertoire des Entrées`

:boldblue:`GRP_OUT` : :blue:`Répertoire des Sorties`

:boldblue:`GRP_SCEN_NORAIN` : :blue:`Identifiant du scénario Pluie Nulle`

:boldblue:`GRP_MODEL_POM` : :blue:`Code modèle POM`


.. rubric:: Arborescence otamin


:boldblue:`OTAMIN_HOME` : :blue:`Répertoire principal`

:boldblue:`OTAMIN_EXE` : :blue:`Exécutable TR OTAMIN`

:boldblue:`OTAMIN_IN` : :blue:`Répertoire des entrées TR OTAMIN`

:boldblue:`OTAMIN_OUT` : :blue:`Répertoire des sorties TR OTAMIN`


.. rubric:: Arborescence scenarios meteo


:boldblue:`MET_HOME` : :blue:`Répertoire de travail`


.. rubric:: Archivage prv, csg et png


:boldblue:`PRV_DIRNAME` : :blue:`Répertoire des données au format prv`

:boldblue:`PRV_BAT_FILENAME` : :blue:`Exécutable de conversion grp2prv`

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`PNG_DIRNAME` : :blue:`Répertoire des données au format png`

:boldblue:`OBS_DIRNAME` : :blue:`Répertoire des données observées`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`COLORMAP` : :blue:`Palette de couleur`

:boldblue:`DPI` : :blue:`Résolution des images`


.. rubric:: Utilitaires exécutés par le script


- :blue:`Dupliquer la configuration GRP Temps Reel et modifier son contenu` : `duplicateGrpRTCfg.py <../bin/duplicateGrpRTCfg.html>`_

- :blue:`Tracer les données observées et/ou prévues sous forme graphique` : `plotGrpData.py <../bin/plotGrpData.html>`_

- :blue:`Convertir les données prv Scores/OTAMIN au format csv GRP` : `prv2grp.py <../bin/prv2grp.html>`_
