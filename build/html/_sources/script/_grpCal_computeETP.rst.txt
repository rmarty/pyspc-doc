
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grpCal_computeETP:

.. role:: blue

.. role:: boldblue

Calcul d'ETP (_grpCal_computeETP.py)
-------------------------------------------------------

Description
+++++++++++

Calcul des ETP à partir de séries de température horaires, les latitudes des stations et selon la pondération du bassin

Paramètres
++++++++++

.. rubric:: Configuration des donnees

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`COORD_FILENAME` : :blue:`Fichier contenant les latitudes des stations`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`

:boldblue:`TIMESTEP` : :blue:`Pas de temps`