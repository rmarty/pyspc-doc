
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_postEvent_302_validfcst_plotevent:

.. role:: blue

.. role:: boldblue

Tracer les prévisions opérationnelles validées par lieu (_postEvent_302_validfcst_plotevent.py)
-----------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les prévisions opérationnelles validées par lieu

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`RAW_TREND` : :blue:`Nom de la prévision brute à conserver`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`COLORS` : :blue:`Couleurs par prévision/tendance`

:boldblue:`LINESTYLES` : :blue:`Styles de ligne, tendance [0] et déterministe [1]`

:boldblue:`FILLED` : :blue:`Afficher les incert sous forme d'enveloppe (True/False)`