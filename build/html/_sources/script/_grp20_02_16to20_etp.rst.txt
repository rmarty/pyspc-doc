
.. fichier rst créé automatiquement par scriptdoc

:orphan:

.. _script_grp20_02_16to20_etp:

.. role:: blue

.. role:: boldblue

Conversion des fichiers bassins de GRPv2016 à GRPv2020 (_grp20_02_16to20_etp.py)
--------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers d'ETP de GRP, de la version 2016 à la version 2020

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16

:boldblue:`BDD_16` : :blue:`Répertoire de la base de données`

:boldblue:`ETP_16` : :blue:`Répertoire des fichiers ETP`

.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de la base de données`

:boldblue:`ETP_20` : :blue:`Répertoire des fichiers ETP`