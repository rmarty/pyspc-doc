.. fichier rst créé automatiquement par scriptdoc

:orphan:


_postEvent_241_validfcst_verification.py
========================================


.. role:: blue

.. role:: boldblue

.. index:: _postEvent_241_validfcst_verification.py

Script post-événement : calculer les critères de vérification des prv validées

``_postEvent_241_validfcst_verification.py``



.. rubric:: Configuration de l'evenement


:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`


.. rubric:: Configuration des series


:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`


.. rubric:: Configuration des statistiques


:boldblue:`IDSERIES` : :blue:`Table de regroupement des séries par tendance`

:boldblue:`SCORES_ABS` : :blue:`Calcul en valeur absolue ? (T/F) sous forme de liste`

:boldblue:`SCORES_REL` : :blue:`Calcul en valeur relative ? (T/F) sous forme de liste`

:boldblue:`SCORES_DELTA` : :blue:`Pas, en fréquence, des statistiques d'erreur`

:boldblue:`SCORES_FREQS` : :blue:`Fréquences des statistiques d'erreur`
