pyspc.verification.scores.scores.Results
========================================

.. currentmodule:: pyspc.verification.scores.scores

.. autoclass:: Results

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Results.__init__
      ~Results.read
      ~Results.to_csv
      ~Results.to_png
   
   

   
   
   