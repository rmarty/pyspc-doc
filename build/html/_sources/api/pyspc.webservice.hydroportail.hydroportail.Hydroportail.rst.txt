pyspc.webservice.hydroportail.hydroportail.Hydroportail
=======================================================

.. currentmodule:: pyspc.webservice.hydroportail.hydroportail

.. autoclass:: Hydroportail

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Hydroportail.__init__
      ~Hydroportail.check_datatype
      ~Hydroportail.get
      ~Hydroportail.get_datatypes
      ~Hydroportail.login
      ~Hydroportail.logout
      ~Hydroportail.retrieve
      ~Hydroportail.seasons2ouahs
   
   

   
   
   