pyspc.data.meteofrance.Sympo\_Data
==================================

.. currentmodule:: pyspc.data.meteofrance

.. autoclass:: Sympo_Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Sympo_Data.__init__
      ~Sympo_Data.check_version
      ~Sympo_Data.get_versions
      ~Sympo_Data.read
      ~Sympo_Data.split_basename
      ~Sympo_Data.write
   
   

   
   
   