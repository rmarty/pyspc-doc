pyspc.model.plathynes.unit\_hydrograph
======================================

.. automodule:: pyspc.model.plathynes.unit_hydrograph

   
   
   .. rubric:: Functions

   .. autosummary::
   
      FBN
      FPU
      RE1
      TG1
      build
      plot
   
   

   
   
   

   
   
   