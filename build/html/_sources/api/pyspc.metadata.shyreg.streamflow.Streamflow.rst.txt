pyspc.metadata.shyreg.streamflow.Streamflow
===========================================

.. currentmodule:: pyspc.metadata.shyreg.streamflow

.. autoclass:: Streamflow

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Streamflow.__init__
      ~Streamflow.read
      ~Streamflow.to_csv
   
   

   
   
   